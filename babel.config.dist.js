module.exports = {
  presets: ['@babel/preset-typescript'],
  plugins: [
    ['@babel/plugin-syntax-decorators', { decoratorsBeforeExport: false }],
    '@babel/plugin-syntax-optional-chaining',
  ],
  ignore: [
    './src-ts/declarations.d.ts',
    './src-ts/interfaces.ts',
    './src-ts/service-types.ts',
  ],
};
