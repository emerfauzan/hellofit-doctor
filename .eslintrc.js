module.exports = {
  root: true,
  extends: ['@react-native-community', 'prettier'],
  rules: {
    'prettier/prettier': 'error',
    'import/newline-after-import': 'error',
  },
  plugins: ['prettier', 'import'],
};
