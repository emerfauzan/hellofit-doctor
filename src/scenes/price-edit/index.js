import React, { createRef, useState } from 'react';
import { ScrollView, StyleSheet, Text } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { useObserver } from 'mobx-react-lite';
import { profileStore } from '../../stores';
import { Header, Page, TextInputField } from '../../components';

export const PriceEdit = (props) => {
  const [loading, setLoading] = useState(false);
  const [price, setPrice] = useState(profileStore.doctor.price.toString());
  const ref = createRef();

  async function _submit() {
    setLoading(true);

    try {
      const price = ref.current.getRawValue();
      await profileStore.updatePrice(price);
      Actions.pop();
    } finally {
      setLoading(false);
    }
  }

  return useObserver(() => (
    <Page loading={loading}>
      <Header
        title="Atur Biaya Konsultasi"
        buttonRight="Simpan"
        onBack={() => Actions.pop()}
        onRight={() => _submit()}
      />
      <ScrollView contentContainerStyle={styles.container}>
        <TextInputField
          ref={ref}
          label="Biaya Konsultasi"
          keyboardType="number-pad"
          placeholder="Rp"
          options={{
            precision: 0,
            unit: 'Rp ',
          }}
          type="money"
          value={price}
          onChangeText={(text) => setPrice(text)}
        />
        <Text style={styles.minPrice}>Minimum Rp 5.000</Text>
        <Text style={styles.note}>*Biaya ini hanya berlaku untuk 30 menit waktu chatting</Text>
      </ScrollView>
    </Page>
  ));
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    padding: 16,
  },
  minPrice: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
    fontSize: 14,
    lineHeight: 16,
  },
  note: {
    color: '#FFCB05',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
    lineHeight: 15,
  },
});
