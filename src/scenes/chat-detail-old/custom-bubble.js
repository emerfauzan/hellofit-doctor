import React from 'react';
import { Bubble, Time } from 'react-native-gifted-chat';
import { DoctorNoteBubble } from './doctor-note-bubble';
import { SymptomBubble } from './symptom-bubble';

export const CustomBubble = (props) => {
  let customProps = Object.assign({}, props, {
    wrapperStyle: {
      left: {
        backgroundColor: '#FFEFB1',
      },
      right: {
        backgroundColor: '#FFF',
      },
    },
    textStyle: {
      left: {
        color: '#373F50',
        fontFamily: 'Quicksand-Regular',
        fontSize: 16,
      },
      right: {
        color: '#373F50',
        fontFamily: 'Quicksand-Regular',
        fontSize: 16,
      },
    },

    renderTime() {
      if (props.currentMessage && props.currentMessage.createdAt) {
        const { containerStyle, wrapperStyle, textStyle, ...timeProps } = props;

        if (props.renderTime) {
          return props.renderTime(timeProps);
        }

        const timeTextStyle = {
          left: {
            color: '#9CA2B4',
          },
          right: {
            color: '#9CA2B4',
          },
        };
        return <Time {...timeProps} timeTextStyle={timeTextStyle} />;
      }

      return null;
    },
  });

  if (props.currentMessage?.text.slice(0, 3) === '***') {
    const data = JSON.parse(props.currentMessage.text.slice(3));
    customProps = Object.assign({}, customProps, {
      renderMessageText() {
        return (
          <SymptomBubble
            symptom={data.symptom}
            duration={data.duration}
            medicine={data.medicine}
          />
        );
      },
    });
  } else if (props.currentMessage?.text.slice(0, 3) === '###') {
    const data = JSON.parse(props.currentMessage.text.slice(3));
    customProps = Object.assign({}, customProps, {
      renderMessageText() {
        return (
          <DoctorNoteBubble
            diagnose={data.diagnose}
            suggestion={data.suggestion}
          />
        );
      },
    });
  }

  return <Bubble {...customProps} />;
};
