import React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { Send } from 'react-native-gifted-chat';
import PlusIcon from './plus.svg';
import SendIcon from './send.svg';

export const CustomSend = (props) => (
  <View style={styles.container}>
    <TouchableOpacity onPress={props.onAdd}>
      <View style={styles.plusButton}>
        <PlusIcon />
      </View>
    </TouchableOpacity>
    <Send {...props}>
      <View
        style={{
          alignItems: 'center',
          backgroundColor: '#FFCB05',
          borderRadius: 4,
          height: 36,
          justifyContent: 'center',
          marginBottom: 8,
          marginRight: 8,
          width: 36,
        }}
      >
        <SendIcon />
      </View>
    </Send>
  </View>
);
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  plusButton: {
    alignItems: 'center',
    height: 30,
    justifyContent: 'center',
    marginRight: 8,
    marginTop: 4,
    width: 30,
  },
});
