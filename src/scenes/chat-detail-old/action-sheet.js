import React from 'react';
import {
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';

export const ActionSheet = (props) => (
  <Modal {...props} transparent>
    <View style={styles.container}>
      <View style={[styles.menu, styles.shadow]}>
        <View style={styles.hole} />
        <TouchableWithoutFeedback onPress={props.onRequestClose}>
          <View style={styles.menuItem}>
            <Text style={styles.title}>Lampiran</Text>
          </View>
        </TouchableWithoutFeedback>
        <View style={styles.separator} />
        <TouchableOpacity onPress={props.onNote}>
          <View style={styles.menuItem}>
            <Text style={styles.menuLabel}>Catatan Dokter</Text>
          </View>
        </TouchableOpacity>
        {/* <View style={styles.separator} />
        <TouchableOpacity onPress={props.onMedicine}>
         <View style={styles.menuItem}>
           <Text style={styles.menuLabel}>Rekomendasi Obat</Text>
         </View>
        </TouchableOpacity> */}
      </View>
      <View style={styles.blank} />
    </View>
  </Modal>
);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  menu: {
    backgroundColor: '#FFF',
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
  },
  hole: {
    alignSelf: 'center',
    backgroundColor: '#EBECF0',
    borderRadius: 3,
    height: 5,
    marginTop: 8,
    width: 48,
  },
  title: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
    fontSize: 16,
  },
  menuItem: {
    height: 50,
    justifyContent: 'center',
    paddingLeft: 16,
  },
  menuLabel: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
  },
  separator: {
    borderBottomWidth: 1,
    borderColor: '#EBECF0',
  },
  blank: {
    height: 60,
  },
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
});
