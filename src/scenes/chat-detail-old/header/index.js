import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { HeaderLayout } from '../../../components';
import { chatSeconds } from '../../../utils';
import { TimerBox } from './timer-box';
import BackIcon from './back.svg';
import MoreIcon from './more.svg';

export const Header = (props) => (
  <HeaderLayout>
    <View style={styles.container}>
      <TouchableOpacity onPress={props.onBack}>
        <View style={styles.left}>
          <BackIcon />
        </View>
      </TouchableOpacity>
      <View style={styles.middle}>
        <Text style={styles.title}>{props.chat.patient.name}</Text>
      </View>
      <View style={styles.right}>
        {props.chat.acceptedAt && (
          <TimerBox
            initial={chatSeconds(props.chat.acceptedAt)}
            onEnd={props.onEnd}
            onPress={props.onDone}
          />
        )}
        <TouchableOpacity style={styles.moreButton}>
          <MoreIcon />
        </TouchableOpacity>
      </View>
    </View>
  </HeaderLayout>
);
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFCB05',
    flexDirection: 'row',
    height: 56,
  },
  left: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 56,
    width: 56,
  },
  middle: {
    flex: 1,
    justifyContent: 'center',
  },
  title: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
    fontSize: 16,
    lineHeight: 19,
  },
  right: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  moreButton: {
    marginHorizontal: 8,
  },
});
