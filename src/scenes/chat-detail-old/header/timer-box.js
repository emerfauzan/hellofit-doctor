import React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { Timer } from '../../../components';
import CloseIcon from './close.svg';

export const TimerBox = (props) => (
  <TouchableOpacity onPress={props.onPress}>
    <View style={styles.container}>
      <Timer initial={props.initial} max={1800} onEnd={props.onEnd} />
      <View style={styles.closeButton}>
        <CloseIcon />
      </View>
    </View>
  </TouchableOpacity>
);
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    borderRadius: 4,
    flexDirection: 'row',
    height: 32,
    paddingLeft: 8,
  },
  closeButton: {
    alignItems: 'center',
    height: 32,
    justifyContent: 'center',
    width: 32,
  },
});
