import React, { useEffect, useState } from 'react';
import { View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { GiftedChat } from 'react-native-gifted-chat';
import { useObserver } from 'mobx-react-lite';
import { InfoDialog, Page } from '../../components';
import { useForceUpdate } from '../../hooks';
import { feathersClient } from '../../utils';
import {
  chatMessageStore,
  chatProgressStore,
  profileStore,
} from '../../stores';
import { ActionSheet } from './action-sheet';
import { CustomBubble } from './custom-bubble';
import { CustomSend } from './custom-send';
import { Header } from './header';

export const ChatDetail = (props) => {
  const [showActionSheet, setShowActionSheet] = useState(false);
  const [showAskDialog, setShowAskDialog] = useState(false);
  const [showEndDialog, setShowEndDialog] = useState(false);
  useForceUpdate();
  useEffect(() => {
    const listener = (chat) => {
      if (chat.id === props.chat.id && chat.status === 'completed') {
        setShowEndDialog(true);
      }
    };

    feathersClient.service('chats').once('patched', listener);
    return function cleanup() {
      feathersClient.service('chats').removeListener('patched', listener);
    };
  }, [props.chat.id]);
  const query = {
    chatId: props.chat.id,
    $sort: {
      createdAt: -1,
    },
  };
  useEffect(() => {
    chatMessageStore.find({
      query,
    });
  }, [query]);

  async function _handleSend(newMessages) {
    for (const message of newMessages) {
      await chatMessageStore.create({
        chatId: props.chat.id,
        type: 'doctor',
        text: message.text,
      });
    }

    await chatMessageStore.find({
      query,
    });
  } // async function _handleDone() {
  //   const lastMessage = chatMessageStore.data[0];
  //   await chatProgressStore.patch(props.chat.id, {
  //     suggestion: lastMessage.text,
  //     status: 'completed',
  //   });
  //   await chatProgressStore.find({ query: { status: 'accepted' } });
  //   Actions.pop();
  // }

  async function _handleDone() {
    await chatProgressStore.patch(props.chat.id, {
      status: 'completed',
    });
    await chatProgressStore.find({
      query: {
        status: 'accepted',
      },
    });
    Actions.pop();
  }

  return useObserver(() => {
    const { symptom, duration, medicine } = props.chat;
    const doctorId = `doctor-${profileStore.doctor.id}`;
    const patientId = `patient-${props.chat.patientId}`;
    const welcomeMessage = {
      _id: -2,
      text: `Selamat siang. Saya ${props.chat.doctor.name}. Ada yang bisa saya bantu? Silahkan sampaikan keluhan Anda.`,
      createdAt: props.chat.createdAt,
      user: {
        _id: doctorId,
      },
    };
    const symptomMessage = {
      _id: -1,
      text:
        '***' +
        JSON.stringify({
          symptom,
          duration,
          medicine,
        }),
      createdAt: props.chat.createdAt,
      user: {
        _id: patientId,
      },
    };
    const messages = chatMessageStore.data.map((chatMessage) => ({
      _id: chatMessage.id,
      text: chatMessage.text,
      createdAt: chatMessage.createdAt,
      user: {
        _id: chatMessage.type === 'doctor' ? doctorId : patientId,
      },
    }));
    return (
      <Page>
        <Header
          chat={props.chat}
          onBack={() => Actions.pop()}
          onDone={() => setShowAskDialog(true)}
          onEnd={() => setShowAskDialog(true)}
        />
        <GiftedChat
          alwaysShowSend
          placeholder="Ketik pesan anda di sini"
          messages={[...messages, symptomMessage, welcomeMessage]}
          keyboardShouldPersistTaps={false}
          renderAvatar={null}
          renderBubble={(props) => <CustomBubble {...props} />}
          renderChatFooter={() => (
            <View
              style={{
                height: 28,
              }}
            />
          )}
          renderSend={(props) => (
            <CustomSend {...props} onAdd={() => setShowActionSheet(true)} />
          )}
          textInputStyle={{
            color: '#373F50',
            borderColor: '#EFF0F3',
            borderRadius: 4,
            borderWidth: 1,
            fontFamily: 'Quicksand-Regular',
            fontSize: 16,
            lineHeight: 22,
            marginLeft: 8,
            marginRight: 8,
            marginTop: 8,
            marginBottom: 8,
            paddingHorizontal: 16,
          }}
          textInputProps={{
            autoCorrect: false,
          }}
          user={{
            _id: doctorId,
          }}
          onSend={(newMessages) => _handleSend(newMessages)}
        />
        <ActionSheet
          visible={showActionSheet}
          onRequestClose={() => setShowActionSheet(false)}
          onDismiss={() => setShowActionSheet(false)}
          onNote={() => {
            setShowActionSheet(false);
            Actions.push('doctor-note', {
              chat: props.chat,
            });
          }}
          onMedicine={() => {
            setShowActionSheet(false);
            Actions.push('doctor-prescription');
          }}
        />
        <InfoDialog
          visible={showAskDialog}
          title="Akhiri Sesi"
          description="Apakah Anda yakin ingin mengakhiri sesi?"
          onCancel={() => setShowAskDialog(false)}
          onConfirm={() => _handleDone()}
        />
        <InfoDialog
          visible={showEndDialog}
          title="Sesi Berakhir"
          description="Sesi chat anda sudah selesai"
          confirmLabel="OK"
          onConfirm={() => {
            setShowEndDialog(false)
            Actions.pop()
          }}
        />
      </Page>
    );
  });
};
