import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export const DoctorNoteBubble = (props) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Catatan Dokter</Text>
      <View style={styles.row}>
        <Text style={styles.key}>Diagnosa</Text>
        <Text style={styles.value}>{props.diagnose}</Text>
      </View>
      <View style={styles.row}>
        <Text style={styles.key}>Saran</Text>
        <Text style={styles.value}>{props.suggestion}</Text>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    padding: 8,
  },
  title: {
    color: '#373F50',
    fontFamily: 'Quicksand-SemiBold',
    fontSize: 16,
  },
  row: {
    borderColor: '#FFF',
    borderTopWidth: 1,
    marginTop: 8,
    paddingTop: 8,
  },
  key: {
    color: '#373F50',
    fontFamily: 'Quicksand-SemiBold',
    fontSize: 12,
  },
  value: {
    color: '#373F50',
    fontFamily: 'Quicksand-Regular',
    fontSize: 16,
  },
});
