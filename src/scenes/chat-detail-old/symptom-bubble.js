import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export const SymptomBubble = (props) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Keluhan Pasien</Text>
      <View style={styles.row}>
        <Text style={styles.key}>Keluhan</Text>
        <Text style={styles.value}>{props.symptom}</Text>
      </View>
      <View style={styles.row}>
        <Text style={styles.key}>Lama Keluhan Berlangsung</Text>
        <Text style={styles.value}>{props.duration} hari</Text>
      </View>
      <View style={styles.row}>
        <Text style={styles.key}>Obat yang Pernah Dikonsumsi / Digunakan</Text>
        <Text style={styles.value}>{props.medicine}</Text>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    padding: 8,
  },
  title: {
    color: '#373F50',
    fontFamily: 'Quicksand-SemiBold',
    fontSize: 16,
  },
  row: {
    borderColor: '#FFF',
    borderTopWidth: 1,
    marginTop: 8,
    paddingTop: 8,
  },
  key: {
    color: '#373F50',
    fontFamily: 'Quicksand-SemiBold',
    fontSize: 12,
  },
  value: {
    color: '#373F50',
    fontFamily: 'Quicksand-Regular',
    fontSize: 16,
  },
});
