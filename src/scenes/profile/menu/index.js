import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import ExitIcon from './exit.svg';
import GearIcon from './gear.svg';
import HelpIcon from './help.svg';
import StarIcon from './star.svg';
import { Separator } from './separator';

export const Menu = (props) => (
  <View style={styles.container}>
    <TouchableOpacity onPress={props.onRating}>
      <View style={styles.row}>
        <View style={styles.left}>
          <StarIcon />
        </View>
        <View style={styles.right}>
          <Text style={styles.label}>Rating & Feedback</Text>
        </View>
      </View>
    </TouchableOpacity>
    <Separator />
    <TouchableOpacity onPress={props.onSettings}>
      <View style={styles.row}>
        <View style={styles.left}>
          <GearIcon />
        </View>
        <View style={styles.right}>
          <Text style={styles.label}>Pengaturan</Text>
        </View>
      </View>
    </TouchableOpacity>
    <Separator />
    <TouchableOpacity onPress={props.onHelp}>
      <View style={styles.row}>
        <View style={styles.left}>
          <HelpIcon />
        </View>
        <View style={styles.right}>
          <Text style={styles.label}>Bantuan</Text>
        </View>
      </View>
    </TouchableOpacity>
    <Separator />
    <TouchableOpacity onPress={props.onLogout}>
      <View style={styles.row}>
        <View style={styles.left}>
          <ExitIcon />
        </View>
        <View
          style={[
            styles.right,
            {
              borderBottomWidth: 0,
            },
          ]}
        >
          <Text style={styles.label}>Keluar</Text>
        </View>
      </View>
    </TouchableOpacity>
  </View>
);
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    borderRadius: 4,
  },
  row: {
    flexDirection: 'row',
    height: 50,
  },
  left: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 50,
  },
  right: {
    flex: 1,
    justifyContent: 'center',
  },
  label: {
    color: '#373F50',
    fontFamily: 'Quicksand-Regular',
    fontSize: 16,
    lineHeight: 19,
  },
});
