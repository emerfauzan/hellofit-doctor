import React from 'react';
import { StyleSheet, View } from 'react-native';
import BellIcon from './bell.svg';

export const NotificationIcon = (props) => (
  <View>
    <BellIcon />
    {props.mark && <View style={styles.dot} />}
  </View>
);
const styles = StyleSheet.create({
  dot: {
    backgroundColor: '#E6495A',
    borderColor: '#FFCB05',
    borderRadius: 4,
    borderWidth: 1,
    height: 8,
    position: 'absolute',
    right: 2,
    top: 4,
    width: 8,
  },
});
