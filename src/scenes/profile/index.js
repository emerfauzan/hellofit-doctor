import React, { useEffect, useState } from 'react';
import { AppState, ScrollView, StyleSheet, DeviceEventEmitter, Platform } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { useObserver } from 'mobx-react-lite';
import PushNotification from 'react-native-push-notification';
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import { Page, InfoDialog } from '../../components';
import { Header } from './header';
import { Menu } from './menu';
import { OnlineStatus } from './online-status';
import { Summary } from './summary';
import { dashboardStore, profileStore, chatProgressStore, hospitalScheduleStore } from '../../stores';
import { feathersClient } from '../../utils';

export const Profile = (props) => {
  const [refresh, setRefresh] = useState(true);
  const [showAlert, setShowAlert] = useState(false);

  PushNotification.configure({
    // (optional) Called when Token is generated (iOS and Android)
    onRegister: function (token) {
      console.log("TOKEN:", token);
    },
  
    // (required) Called when a remote is received or opened, or local notification is opened
    onNotification: function (notification) {
      console.log("NOTIFICATION:", notification);

      notification.finish(PushNotificationIOS.FetchResult.NoData);
    },
  
    // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
    onAction: function (notification) {
      console.log("ACTION:", notification.action);
      console.log("NOTIFICATION:", notification);

    },
  
    // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
    onRegistrationError: function(err) {
      console.error(err.message, err);
    },
  
    // IOS ONLY (optional): default: all - Permissions to register.
    permissions: {
      alert: true,
      badge: true,
      sound: true,
    },
    requestPermissions: true,
  });
  async function setData() {
    const result = await feathersClient.service('hospital-schedules').find({
      query: {
        doctorId: profileStore.doctor.id,
      },
    });
    hospitalScheduleStore.setData(result.data)
  }

  useEffect(() => {
    function listener(nextState) {
      if (nextState === 'active') {
        setRefresh(true);
      } else {
        setRefresh(false);
      }
    }

    AppState.addEventListener('change', listener);
    return function cleanup() {
      AppState.removeEventListener('change', listener);
    };
  }, []);
  useEffect(() => {
    async function run() {
      if (refresh) {
        const { user } = await feathersClient.get('authentication');
        await dashboardStore.get(user.id);
        await profileStore.fetch();

      }
    }

    run();
  }, [refresh]);

  async function _logout() {
    try {
      await feathersClient.logout();
      Actions.reset('login');
    } catch (error) { }
  }

  async function _setStatus(value) {
    if(profileStore.doctor.online && chatProgressStore.total !== 0){
      setShowAlert(true)
    }else{
      await profileStore.setStatus(value);
  
      if (value) {
        PushNotification.localNotification({
          id: '1',
          message: 'Status is Online',
          ongoing: true,
          soundName: 'notif_hellofit.mp3',
        });
        DeviceEventEmitter.addListener('notificationActionReceived', function(action){
          const info = JSON.parse(action.dataJSON);
          if (info.action == 'Accept') {
            // Do work pertaining to Accept action here
          } else if (info.action == 'Reject') {
            // Do work pertaining to Reject action here
          }
          // Add all the required actions handlers
        });
      } else {
        PushNotification.clearLocalNotification(1);
      }
    }
  }
  
  useEffect(() => {
    chatProgressStore.find({
      query: {
        status: 'accepted',
      },
    });
  }, []);

  return useObserver(() => (
    <Page>
      <Header />
        <InfoDialog
          visible={showAlert}
          description="Anda masih memiliki chat yang berlangsung, mohon selesaikan chat Anda terlebih dahulu"
          confirmLabel="OK"
          onConfirm={() => setShowAlert(false)}
        />
      {profileStore.doctor && (
        <ScrollView contentContainerStyle={styles.container}>
          <Summary
            doctor={profileStore.doctor}
            rating={dashboardStore.current && dashboardStore.current.rating}
            weeklyOnline={
              dashboardStore.current ? dashboardStore.current.weeklyOnline : 0
            }
            onEdit={() => {
              setData()
              Actions.push('profile-edit')
            }}
          />
          <OnlineStatus
            value={profileStore.doctor.online}
            onChangeValue={(value) => _setStatus(value)}
          />
          <Menu
            onRating={() => Actions.push('rating-list')}
            onSettings={() => Actions.push('settings')}
            onHelp={() => Actions.push('help-list')}
            onLogout={() => _logout()}
          />
        </ScrollView>
      )}
    </Page>
  ));
};
const styles = StyleSheet.create({
  container: {
    padding: 16,
  },
});
