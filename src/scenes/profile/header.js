import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { HeaderLayout } from '../../components';
import { NotificationIcon } from './notification-icon';

export const Header = (props) => (
  <HeaderLayout>
    <View style={styles.container}>
      <Text style={styles.title}>Profil</Text>
      <NotificationIcon />
    </View>
  </HeaderLayout>
);
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#FFCB05',
    flexDirection: 'row',
    height: 56,
    justifyContent: 'space-between',
    paddingHorizontal: 16,
  },
  title: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
    fontSize: 16,
    lineHeight: 19,
  },
});
