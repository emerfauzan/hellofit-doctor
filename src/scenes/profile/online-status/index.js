import React, { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import moment from 'moment'
import { SwitchInput } from './switch-input';

export const OnlineStatus = (props) => {
  let ledColor = '#E6495A';
  let status = 'Offline';

  if (props.value) {
    ledColor = '#52DE86';
    status = 'Online';
  }

  return (
    <View style={styles.container}>
      <View
        style={[
          styles.led,
          {
            backgroundColor: ledColor,
          },
        ]}
      />
      <Text style={styles.label}>{status}</Text>
      <View style={styles.right}>
        <SwitchInput value={props.value} onChangeValue={props.onChangeValue} />
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#FFF',
    borderRadius: 4,
    flexDirection: 'row',
    height: 50,
    marginBottom: 12,
    paddingLeft: 16,
  },
  led: {
    borderRadius: 8,
    height: 16,
    width: 16,
  },
  label: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    lineHeight: 17,
    marginLeft: 16,
  },
  right: {
    alignItems: 'flex-end',
    flex: 1,
  },
});
