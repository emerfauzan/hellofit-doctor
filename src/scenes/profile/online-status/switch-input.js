import React, { useState, useEffect } from 'react';
import {
  Animated,
  StyleSheet,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import moment from 'moment'

const useMount = func => useEffect(() => func(), []);

const useCheckTime = () => {
  const [disabled, setDisabled] = useState(false)

  useMount(() => {
    const getDisabled = async () => {
      let format = 'HH:mm:ss';

      const timeStart = moment("22:00:00", format);
      const timeEnd = moment("24:00:00", format);
      const timeStart2 = moment("00:00:00", format);
      const timeEnd2 = moment("07:00:00", format);
      const timeNow = moment().format(format)

      const isOff = moment(timeNow, format).isBetween(timeStart, timeEnd) || moment(timeNow, format).isBetween(timeStart2, timeEnd2)
      if (isOff) {
        setDisabled(true)
      } else {
        setDisabled(false)
      }
    };

    getDisabled();
  });

  return { disabled };
};

export const SwitchInput = (props) => {
  const [left] = useState(new Animated.Value(2));
  const { disabled } = useCheckTime()

  let backgroundColor = '#CACDD5';
  let top = 2;

  if (props.value) {
    backgroundColor = '#FFCB05';
    Animated.timing(left, {
      toValue: 18,
      duration: 100,
    }).start();
  }

  function _animate() {
    if (props.value) {
      Animated.timing(left, {
        toValue: 2,
        duration: 100,
      }).start();
    } else {
      Animated.timing(left, {
        toValue: 18,
        duration: 100,
      }).start();
    }

    props.onChangeValue(!props.value);
  }

  return (
    <TouchableWithoutFeedback onPress={() => _animate()} disabled={disabled}>
      <View style={styles.container}>
        <View
          style={[
            styles.inner,
            {
              backgroundColor,
            },
          ]}
        >
          <Animated.View
            style={[
              styles.circle,
              {
                left,
                top,
              },
            ]}
          />
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    height: 32,
    justifyContent: 'center',
    width: 64,
  },
  inner: {
    borderRadius: 8,
    height: 16,
    width: 32,
  },
  circle: {
    backgroundColor: '#FFF',
    borderRadius: 6,
    height: 12,
    width: 12,
  },
});
