import React, { useEffect, useState } from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { numeral } from '../../../utils';
import avatar from './avatar.png';
import ThumbIcon from './thumb.svg';
import TimerIcon from './timer.svg';

export const Summary = (props) => {
  const [weeklyOnline, setWeeklyOnline] = useState(props.weeklyOnline);
  useEffect(() => {
    let counter = props.weeklyOnline;
    const interval = setInterval(() => {
      if (props.doctor.online) {
        counter = counter + 1;
        setWeeklyOnline(counter);
      }
    }, 1000);
    return () => {
      clearInterval(interval);
    };
  }, [props.doctor, props.weeklyOnline]);
  return (
    <View style={styles.container}>
      <View style={styles.top}>
        <Image
          source={
            props.doctor.photo
              ? {
                uri: props.doctor.photo,
              }
              : avatar
          }
          style={styles.avatar}
        />
        <View style={styles.middle}>
          <View>
            <Text style={styles.name}>{props.doctor.name}</Text>
            <Text style={styles.description}>
              {props.doctor.specialty.name}
            </Text>
          </View>
          <TouchableOpacity style={styles.textButton} onPress={props.onEdit}>
            <Text style={styles.textButtonLabel}>Edit Profil</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.bottom}>
        <View style={styles.bottomLeft}>
          <Text style={styles.bottomLabel}>Rating</Text>
          <View style={styles.bottomValue}>
            <ThumbIcon />
            <Text style={styles.bottomValueText}> {props.rating}%</Text>
          </View>
        </View>
        <View style={styles.bottomRight}>
          <Text style={styles.bottomLabel}>Online Minggu Ini</Text>
          <View style={styles.bottomValue}>
            <TimerIcon />
            <Text style={styles.weeklyOnline}>
              {numeral(weeklyOnline).format('00:00:00')}
            </Text>
          </View>
        </View>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    marginBottom: 12,
  },
  top: {
    backgroundColor: '#FFF',
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    flexDirection: 'row',
  },
  avatar: {
    borderRadius: 28,
    height: 56,
    margin: 16,
    width: 56,
  },
  middle: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 20,
    paddingRight: 16,
  },
  name: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
    fontSize: 16,
  },
  description: {
    color: '#9CA2B4',
    fontFamily: 'Quicksand-Regular',
  },
  bottom: {
    flexDirection: 'row',
    marginTop: 1,
  },
  bottomLeft: {
    alignItems: 'center',
    backgroundColor: '#FFF',
    borderBottomLeftRadius: 4,
    flex: 1,
    paddingBottom: 16,
    paddingTop: 12,
  },
  bottomRight: {
    alignItems: 'center',
    backgroundColor: '#FFF',
    borderBottomRightRadius: 4,
    flex: 1,
    marginLeft: 1,
    paddingBottom: 16,
    paddingTop: 12,
  },
  bottomLabel: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
    marginBottom: 4,
  },
  bottomValue: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  bottomValueText: {
    color: '#FFCB05',
    fontFamily: 'Quicksand-Bold',
    lineHeight: 17,
  },
  weeklyOnline: {
    color: '#FFCB05',
    fontFamily: 'Quicksand-Bold',
    lineHeight: 17,
    paddingLeft: 4,
    width: 72,
  },
  textButton: {
    height: 48,
    justifyContent: 'center',
  },
  textButtonLabel: {
    color: '#FFCB05',
    fontFamily: 'Quicksand-Medium',
    fontSize: 16,
    lineHeight: 19,
  },
});
