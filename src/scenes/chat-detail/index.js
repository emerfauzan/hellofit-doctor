import React, { useEffect, useRef, useState } from 'react';
import { ScrollView, StyleSheet, Text, View, KeyboardAvoidingView } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { useObserver } from 'mobx-react-lite';
import {toJS} from 'mobx'
import Toast from 'react-native-simple-toast';
import ImageViewing from 'react-native-image-viewing';
import { InfoDialog, Page } from '../../components';
import { useForceUpdate } from '../../hooks';
import { chatProgressStore, chatMessageStore } from '../../stores';
import { chatSeconds, feathersClient, moment } from '../../utils';
import { ChatMessageItem } from './chat-message-item';
import { Footer } from './footer';
import { Header } from './header';
import { PatientSymptom } from './patient-symptom';
import { TouchableOpacity } from 'react-native-gesture-handler';

export const ChatDetail = (props) => {
  const [loading, setLoading] = useState(false);
  const [showPatientInformation, setShowPatientInformation] = useState(false);
  const [end, setEnd] = useState(false);
  const [showAskDialog, setShowAskDialog] = useState(false);
  const [showEndDialog, setShowEndDialog] = useState(false);
  const [text, setText] = useState('');
  const [images, setImages] = useState([]);
  const [showImages, setShowImages] = useState(false);
  const [noteExist, setNoteExist] = useState(false)
  const [chat, setChat] = useState({})
  const [newMsg, setNewMsg] = useState(0);
  const [actionItem, setActionItem] = useState([
    {
      id: 1,
      name: 'Foto',
    },
    {
      id: 2,
      name: 'Reply Template',
    },
    {
      id: 3,
      name: 'Catatan Dokter',
    },
    {
      id: 4,
      name: 'Rekomendasi Obat'
    },
    {
      id: 5,
      name: 'Riwayat Pasien'
    },
  ])
  const scrollViewRef = useRef(null);
  useForceUpdate();
  useEffect(() => {
    chatMessageStore.setId(props.chat.id)
  }, []);
  useEffect(() => {
    chatMessageStore.find({
      query,
    });
  }, [query]);

  useEffect(() => {
    _scrollToEnd();
  }, []);

  useEffect(() => {
    const listener = (chat) => {
      if (
        chat.id === props.chat.id &&
        chat.status === 'completed' &&
        end === false
      ) {
        setShowEndDialog(true);
      }

      _setActionItem();
    };

    feathersClient.service('chats').on('patched', listener);
    return function cleanup() {
      feathersClient.service('chats').removeListener('patched', listener);
    };
  }, [end, props.chat.id]);

  async function _handleDone() {
    setShowAskDialog(false)
    try {
      setEnd(true);
      setLoading(true);
      await chatProgressStore.patch(props.chat.id, {
        status: 'completed',
      });

      await feathersClient.service('chats').find({
        query: {
          id: props.chat.id,
          isNoteFilled: false,
        }
      }).then((result) => {
        if (result.data.length > 0) {
          Actions.push('doctor-note', {
            isNew: true,
            isEnd: true,
            fromPatient: false,
            chat: result.data[0],
          })
        } else {
          Actions.pop()
        }
      })

      setLoading(false);
    } catch (error) {
      setLoading(false);
      setTimeout(() => {
        Toast.show(error.message);
      }, 100);
    }
  }

  async function _handleSend(val) {
    setText('');
    await chatMessageStore.create({
      chatId: props.chat.id,
      text: val ? val : text,
    });
    _scrollToEnd()
  }

  async function _handleSendPhoto(url) {
    const text = `[photo] ${url}`;
    await chatMessageStore.create({
      chatId: props.chat.id,
      text,
    });
    _scrollToEnd()
  }

  useEffect(() => {
    _setActionItem()
  }, [])

  async function _setActionItem() {
    await feathersClient.service('chats').find({
      query: {
        id: props.chat.id,
      }
    }).then((result) => {
      setChat(result.data[0])
    })

  }

  function _scrollToEnd() {
    setTimeout(() => {
      if (scrollViewRef.current) {
        scrollViewRef.current.scrollToEnd();
      }
      setLoading(false);
    }, 100);
  }

  function _newMsg(val) {
    if(val > newMsg){
      _scrollToEnd()
      setNewMsg(val)
    }
  }

  async function _handleDeleteNote(chatMessageId) {
    await chatProgressStore.patch(props.chat.id, {
      isNoteFilled: false
    });
    await chatMessageStore.remove(chatMessageId);
    await chatMessageStore.find({
      query: {
        chatId: props.chat.id,
        $sort: {
          createdAt: -1,
        },
      },
    });
  }

  const query = {
    chatId: props.chat.id,
    $sort: {
      createdAt: 1,
    },
    $limit: 50,
  };
  return useObserver(() => (
    <Page loading={loading}>
      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : null}
        style={{ flex: 1 }}
        keyboardVerticalOffset={Platform.select({ios: 20, android: 500})}
      >
      <Header
        title={props.chat.patient.name}
        timerInitial={chatSeconds(props.chat.acceptedAt)}
        timerMax={1800}
        onBack={() => Actions.pop()}
        onDone={() => setShowAskDialog(true)}
        onEnd={() => setShowEndDialog(true)}
        chat={props.chat}
        showPatientInformation={showPatientInformation}
        setShowPatientInformation={()=> setShowPatientInformation(!showPatientInformation)}
      />
      <ScrollView ref={scrollViewRef} contentContainerStyle={styles.container}>
        <View style={styles.top}>
          <Text style={styles.dateTime}>
            {props.chat.acceptedAt &&
              moment(props.chat.acceptedAt).calendar().split('pukul')[0]}
          </Text>
        </View>
        <PatientSymptom chat={props.chat} />
        {
        _newMsg(chatMessageStore.data.length),
        chatMessageStore.data.map((message) => {
          console.log(toJS(message))
          return (
            <ChatMessageItem
              key={message.id}
              message={message}
              onDeleteNote={() => _handleDeleteNote(message.id)}
              // onNoteExist={() => _setActionItem()}
              onEditNote={() => {
                Actions.push('doctor-note', {
                  chat: chat,
                  isEnd: false
                })
              }}
              // onReplyTeplate={(msg) =>_handleSend(msg)}
              onPhoto={(url) => {
                setImages([
                  {
                    uri: url,
                  },
                ]);
                setShowImages(true);
              }}
            />
          )
        }
        )}
      </ScrollView>
      <Footer
        chat={chat}
        sendDisabled={text.length === 0}
        text={text}
        onChangeText={(text) => setText(text)}
        onFocus={() => _scrollToEnd()}
        onSend={() => _handleSend()}
        // setShowAlert={false}
        setPhotoData={() => { }}
        onSendPhoto={(url) => {
          setLoading(true)
          _handleSendPhoto(url)
        }}
        // onReplyTeplate={(msg) => _handleSend(msg)}
        onReplyTeplate={(text) => setText(text)}
        actionItem={actionItem}
        setLoading={()=> setLoading(true)}
      />
      <ImageViewing
        visible={showImages}
        images={images}
        imageIndex={0}
        onRequestClose={() => setShowImages(false)}
      />
      <InfoDialog
        visible={showAskDialog}
        title="Akhiri Sesi"
        description="Apakah Anda yakin ingin mengakhiri sesi?"
        onCancel={() => setShowAskDialog(false)}
        onConfirm={() => _handleDone()}
      />
      <InfoDialog
        visible={showEndDialog}
        title="Sesi Berakhir"
        description="Sesi chat anda sudah selesai"
        confirmLabel="OK"
        onConfirm={() => {
          if (chat.isNoteFilled) {
            Actions.pop()
          } else {
            setShowEndDialog(false)
            Actions.replace('doctor-note', {
              isNew: true,
              isEnd: true,
              fromPatient: true,
              chat: chat,
            })
          }
        }}
      />
      </KeyboardAvoidingView>
    </Page>
  ));
};
const styles = StyleSheet.create({
  container: {
    padding: 16,
  },
  top: {
    alignItems: 'center',
    paddingBottom: 16,
  },
  dateTime: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
  },
  card: {
    backgroundColor: 'white',
    zIndex: 3,
  },
  card1: {
    flexDirection: 'row',
    marginVertical: 6,
    backgroundColor: 'white'
  },
  card2: {
    width: '25%',
    borderRightColor: '#EBECF0',
    borderRightWidth: 1,
    paddingVertical: 10,
  },
  textTitle: {
    textAlign: 'center',
    marginBottom: 10,
    color: '#999DAD',
    fontSize: 12,
    fontFamily: 'Quicksand-Medium',
    lineHeight: 15,
  },
  textContent: {
    textAlign: 'center',
    color: '#373F50',
    fontSize: 14,
    fontFamily: 'Quicksand-Bold',
    lineHeight: 18,
  },
  card3: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 4,
  },
  textTop: {
    textAlign: 'center',
    marginRight: 8,
    color: '#373F50',
    fontSize: 14,
    fontFamily: 'Quicksand-Medium',
    lineHeight: 18,
  },
});
