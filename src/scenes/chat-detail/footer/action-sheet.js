import React from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';

export const ActionSheet = (props) => {
  function _handleSelect(item) {
    props.onSelect(item);

    if (props.onRequestClose) {
      props.onRequestClose();
    }
  }

  return (
    <View style={styles.container}>
      <View style={[styles.menu]}>
        <View style={styles.hole} />
        <TouchableWithoutFeedback onPress={props.onRequestClose}>
          <View style={styles.header}>
            <Text style={styles.title}>{props.title}</Text>
          </View>
        </TouchableWithoutFeedback>
        {props.items.map((item) => (
          <TouchableOpacity key={item.id} onPress={() => _handleSelect(item)}>
            <View style={styles.menuItem}>
              <Text style={styles.menuLabel}>{item.name}</Text>
            </View>
          </TouchableOpacity>
        ))}
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-end',
  },
  menu: {
    backgroundColor: '#FFF',
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
  },
  hole: {
    alignSelf: 'center',
    backgroundColor: '#EBECF0',
    borderRadius: 3,
    height: 5,
    marginTop: 8,
    width: 48,
  },
  header: {
    borderBottomWidth: 1,
    borderColor: '#EBECF0',
    height: 50,
    justifyContent: 'center',
    paddingLeft: 16,
  },
  title: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
    fontSize: 16,
  },
  menuItem: {
    borderColor: '#EBECF0',
    borderBottomWidth: 1,
    height: 50,
    justifyContent: 'center',
    paddingLeft: 16,
  },
  menuLabel: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
  },
  separator: {
    borderBottomWidth: 1,
    borderColor: '#EBECF0',
  },
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
});
