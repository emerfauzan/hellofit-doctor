import React, { useState, useEffect } from 'react';
import { StyleSheet, TextInput, TouchableOpacity, View, ScrollView, Animated } from 'react-native';
import ImagePicker from 'react-native-image-picker';
import { Actions } from 'react-native-router-flux';
import { uploadFile, feathersClient } from '../../../utils';
import { ActionSheet } from './action-sheet';
import ClipIcon from './clip.svg';
import SendIcon from './send.svg';
import { InfoDialog } from '../../../components';
import { ReplyTemplate } from './reply-template';
import { profileStore } from '../../../stores';

export const Footer = (props) => {
  const [showActionSheet, setShowActionSheet] = useState(false);
  const [showReplyTemplate, setShowReplyTemplate] = useState(false);
  const [showAlert, setShowAlert] = useState(false);
  const [photoData, setPhotoData] = useState('');
  const [templateItem, setTemplateItem] = useState([])
  const [searchValue, setSearchValue] = useState('');

  useEffect(() => {
    getData();
  }, [])
  
  async function getData() {
    const result = await feathersClient.service('reply-templates').find({
      query:{
        doctorId: profileStore.doctor.id,
      }
    })
    setTemplateItem(result.data)
    setLoading(false)
  }

  async function getMore() {
    const result = await feathersClient.service('reply-templates')
      .find({ 
        query: { 
          doctorId: profileStore.doctor.id,
          $skip: templateItem.length,
        } 
      });
      setTemplateItem(templateItem.concat(result.data))
      setLoading(false)
  }

  function isScrollToBottom ({ layoutMeasurement, contentOffset, contentSize }) {
    const paddingToBottom = 20;
    return (
       layoutMeasurement.height + contentOffset.y >=
       contentSize.height - paddingToBottom
    );
  };

  function searchTemplate(value){
    let data = value.filter((x)=>{
      if(searchValue){
        return x.text.toLowerCase().search(searchValue.toLowerCase()) !== -1
      }else{
        return x
      }
    })
    return data
  }

  return (
    <View>
      {showActionSheet && (
        <ActionSheet
          visible={true}
          title="Lampiran"
          items={props.actionItem}
          onRequestClose={() => setShowActionSheet(false)}
          onSelect={(item) => {
            let options = {
              quality: 0.5,
            };
            if (item.id === 1) {
              ImagePicker.showImagePicker(options, async (response) => {
                if (!response.didCancel) {
                  const url = await uploadFile(
                    response.uri,
                    'image/*',
                    'regCard'
                  );
                  setShowAlert(true)
                  setPhotoData(url)
                }
              });
            } else if (item.id === 2) {
              setShowReplyTemplate(true)
            } else if (item.id === 3) {
              if (props.chat.isNoteFilled) {
                Actions.push('doctor-note', {
                  chat: props.chat,
                });
              } else {
                Actions.push('doctor-note', {
                  isNew: true,
                  chat: props.chat,
                });
              }
            }
            setShowActionSheet(false);
          }}
        />
      )}
      <ScrollView
        onScroll={Animated.event([
          { nativeEvent: { contentOffset: { y: new Animated.Value(0) } } }
        ])}
        onMomentumScrollEnd={async ({ nativeEvent }) => {
          if (isScrollToBottom(nativeEvent)) {
            getMore()
          }
        }}
      >
      {showReplyTemplate && (
        <ReplyTemplate
          visible={true}
          title="Pilih Template"
          items={searchTemplate(templateItem)}
          searchValue={searchValue}
          onChangeText={(text)=> setSearchValue(text)}
          onRequestClose={() => {
            setShowReplyTemplate(false);
            setShowActionSheet(true);
            setSearchValue('');
          }}
          onSelect={(item) => {
            props.onReplyTeplate(item.text);
            setShowReplyTemplate(false);
            setShowActionSheet(false);
            setSearchValue('');
          }}
          getMore={()=> getMore()}
        />
      )}
      </ScrollView>
      <View style={styles.container}>
        <InfoDialog
          visible={showAlert}
          title="Send Picture"
          description="Kirim gambar yang telah anda pilih?"
          cancelLabel="Batal"
          confirmLabel="Kirim"
          onCancel={() => setShowAlert(false)}
          onConfirm={() => {
            setShowAlert(false)
            props.onSendPhoto(photoData);
          }}
        />
        <View style={styles.textInputBox}>
          <TextInput
            autoCompleteType="off"
            autoCorrect={false}
            multiline={true}
            placeholder="Ketik di sini"
            placeholderTextColor="#C9CDD6"
            selectionColor="#FFCB05"
            style={styles.textInput}
            value={props.text}
            onChangeText={props.onChangeText}
            onFocus={props.onFocus}
          />
          <TouchableOpacity
            style={styles.clipButton}
            onPress={() => {
              setShowActionSheet(!showActionSheet)
              setShowReplyTemplate(false)
            }}
          >
            <ClipIcon />
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          disabled={props.sendDisabled}
          style={styles.sendButton}
          onPress={props.onSend}
        >
          <SendIcon />
        </TouchableOpacity>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#FFF',
    flexDirection: 'row',
    paddingLeft: 16,
    paddingRight: 8,
    paddingVertical: 8,
  },
  textInputBox: {
    alignItems: 'center',
    borderColor: '#EBECF0',
    borderRadius: 4,
    borderWidth: 1,
    flex: 1,
    flexDirection: 'row',
  },
  textInput: {
    color: '#373F50',
    height: 80,
    flex: 1,
    fontFamily: 'Quicksand-Medium',
    paddingLeft: 16,
  },
  clipButton: {
    padding: 8,
  },
  sendButton: {
    backgroundColor: '#FFCB05',
    borderRadius: 4,
    marginHorizontal: 8,
    padding: 8,
  },
});
