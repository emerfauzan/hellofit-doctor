import React from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
} from 'react-native';
import SearchIcon from './search.svg';

export const ReplyTemplate = (props) => {
  function _handleSelect(item) {
    props.onSelect(item);
  }

  return (
    <View style={styles.container}>
      <View style={[styles.menu]}>
        <View style={styles.shadow}>
          <TouchableOpacity activeOpacity={1} onPress={props.onRequestClose}>
            <View style={styles.hole} />
            <Text style={styles.title}>{props.title}</Text>
          </TouchableOpacity>
          <View style={styles.header}>
            <View style={styles.viewTop}>
              <View style={styles.searchBox}>
                <TextInput
                  style={[styles.textArea, {height: 40}]}
                  underlineColorAndroid="transparent"
                  autoCapitalize='none'
                  placeholder="Cari Template"
                  placeholderTextColor='#C9CDD6'
                  value={props.searchValue}
                  onChangeText={props.onChangeText}
                />
                <SearchIcon/>
              </View>
            </View>
          </View>
        </View>
        {props.items.map((item) => (
          <TouchableOpacity key={item.id} onPress={() => _handleSelect(item)}>
            <View style={styles.menuItem}>
              <Text style={styles.menuLabel1}>{item.title ? item.title : 'Judul'}</Text>
              <Text style={styles.menuLabel}>{item.text}</Text>
            </View>
          </TouchableOpacity>
        ))}
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-end',
  },
  menu: {
    backgroundColor: '#FFFFFF',
    paddingBottom: 20,
  },
  hole: {
    alignSelf: 'center',
    backgroundColor: '#EBECF0',
    borderRadius: 3,
    height: 5,
    marginTop: 8,
    width: 48,
  },
  header: {
    height: 50,
  },
  viewTop: {
    backgroundColor: '#FFFFFF',
  },
  searchBox: {
    height: 40,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    marginHorizontal: 16,
    marginVertical: 6,
    borderColor: '#EBECF0',
    borderRadius: 4,
    borderWidth: 1,
    alignItems: 'center'
  },
  title: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
    fontSize: 14,
    textAlign: 'center',
    marginVertical: 12,
  },
  menuItem: {
    borderColor: '#EBECF0',
    backgroundColor: '#F9F9FB',
    marginBottom: 12,
    marginHorizontal: 16,
    borderRadius: 5,
    justifyContent: 'center',
    paddingHorizontal: 10,
    paddingVertical: 10
  },
  menuLabel: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    fontSize: 14,
    paddingTop: 5,
  },
  menuLabel1: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    fontSize: 14,
    borderBottomColor: '#C9CDD6',
    borderBottomWidth: 1,
    paddingBottom: 10,
  },
  separator: {
    borderBottomWidth: 1,
    borderColor: '#EBECF0',
  },
  shadow: {
    backgroundColor: '#FFFFFF',
    marginTop: 5,
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
    marginBottom: 16,

    //shadow
    shadowColor: '#F9F9FB',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,
    elevation: 4,
  },
  itemList: {
    backgroundColor: '#FFF',
    flex: 1,
    paddingBottom: 20,
    // paddingHorizontal: 16,
    // paddingVertical: 20,
  },
  textArea: {
    width: '90%',
    justifyContent: "flex-start",
    fontFamily: 'Quicksand-Medium',
    fontSize: 14,
    paddingHorizontal: 16,
    paddingVertical: 9,
  },
});
