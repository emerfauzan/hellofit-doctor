import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { TextField } from '../../components';
import { moment } from '../../utils';

export const PatientSymptom = (props) => (
  <View style={styles.container}>
    <View style={styles.content}>
      <View style={styles.top}>
        <View style={styles.circle} />
        <Text style={styles.title}>Keluhan Pasien</Text>
      </View>
      <View style={styles.bottom}>
        <TextField name="Keluhan" value={props.chat.symptom} />
        <TextField
          name="Lama Keluhan Berlangsung"
          value={`${props.chat.duration} hari`}
        />
        <TextField
          name="Obat yang Pernah Dikonsumsi / Digunakan"
          value={props.chat.medicine}
        />
      </View>
    </View>
    <Text style={styles.dateTime}>
      {props.chat.acceptedAt && moment(props.chat.acceptedAt).format('hh.mm')}
    </Text>
  </View>
);
const styles = StyleSheet.create({
  container: {
    marginBottom: 16,
  },
  content: {
    backgroundColor: '#FFF9E0',
    borderRadius: 4,
    marginBottom: 8,
    marginRight: 16,
  },
  top: {
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#FDF2C3',
    flexDirection: 'row',
    padding: 8,
  },
  bottom: {
    padding: 8,
  },
  circle: {
    backgroundColor: '#FFF',
    borderRadius: 9,
    height: 18,
    width: 18,
  },
  title: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
    lineHeight: 17,
    marginLeft: 8,
  },
  dateTime: {
    alignSelf: 'flex-end',
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
  },
});
