import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { TextField } from '../../components';
import PencilIcon from './pencil.svg';
import TrashIcon from './trash.svg';

export const DoctorSuggestion = (props) => (
  <View style={styles.container}>
    <View style={styles.content}>
      <View style={styles.top}>
        <View style={styles.circle} />
        <Text style={styles.title}>Catatan Dokter</Text>
      </View>
      <View style={styles.bottom}>
        {props.chat.diagnose && (
          <TextField name="Diagnosa" value={props.chat.diagnose} />
        )}
        {props.chat.suggestion && (
          <TextField name="Saran" value={props.chat.suggestion} />
        )}
      </View>
      <View style={styles.iconButtonRow}>
        <TouchableOpacity onPress={props.onEditNote}>
          <View style={styles.iconButton}>
            <PencilIcon />
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={props.onDeleteNote}>
          <View style={styles.iconButton}>
            <TrashIcon />
          </View>
        </TouchableOpacity>
      </View>
    </View>
    {/* <View style={[styles.content, { marginTop: 8 }]}>
     <View style={styles.top}>
       <View style={styles.circle} />
       <Text style={styles.title}>Rekomendasi Obat</Text>
     </View>
     <View style={styles.prescriptionList}>
       <PrescriptionItem />
       <PrescriptionItem />
     </View>
     <View style={styles.iconButtonRow}>
       <TouchableOpacity>
         <View style={styles.iconButton}>
           <PencilIcon />
         </View>
       </TouchableOpacity>
       <TouchableOpacity>
         <View style={styles.iconButton}>
           <TrashIcon />
         </View>
       </TouchableOpacity>
     </View>
    </View> */}
  </View>
);
const styles = StyleSheet.create({
  container: {
    marginBottom: 8,
  },
  content: {
    backgroundColor: '#FFF',
    borderRadius: 4,
    marginLeft: 16,
  },
  top: {
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#EBECF0',
    flexDirection: 'row',
    padding: 8,
  },
  bottom: {
    borderBottomWidth: 1,
    borderColor: '#EBECF0',
    padding: 8,
  },
  circle: {
    backgroundColor: '#FFF9E0',
    borderRadius: 9,
    height: 18,
    width: 18,
  },
  title: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
    lineHeight: 17,
    marginLeft: 8,
  },
  prescriptionList: {
    backgroundColor: '#EBECF0',
  },
  buyButton: {
    alignItems: 'center',
    backgroundColor: '#FFCB05',
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    height: 40,
    justifyContent: 'center',
  },
  buyButtonLabel: {
    color: '#373F50',
    fontFamily: 'Quicksand-SemiBold',
    lineHeight: 17,
  },
  iconButtonRow: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    padding: 8,
  },
  iconButton: {
    alignItems: 'center',
    backgroundColor: '#EBECF0',
    borderRadius: 12,
    height: 24,
    justifyContent: 'center',
    marginLeft: 8,
    width: 24,
  },
});
