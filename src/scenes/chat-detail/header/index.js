import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { HeaderLayout, Timer } from '../../../components';
import BackIcon from './back.svg';
import UpIcon from './up.svg';
import DownIcon from './down.svg';
import CloseIcon from './close.svg';
import moment from 'moment';

export const Header = (props) => (
  <HeaderLayout>
    <View style={styles.container}>
      <TouchableOpacity onPress={props.onBack}>
        <View style={styles.left}>
          <BackIcon />
        </View>
      </TouchableOpacity>
      <View style={styles.middle}>
        <Text style={styles.title}>{props.title}</Text>
      </View>
      <View style={styles.right}>
        <TouchableOpacity style={styles.timer} onPress={props.onDone}>
          <Timer
            initial={props.timerInitial}
            max={props.timerMax}
            onEnd={props.onEnd}
          />
          <CloseIcon />
        </TouchableOpacity>
      </View>
    </View>
    <View style={styles.card}>
      {
        props.showPatientInformation &&
        <View style={styles.card1}>
          <View style={styles.card2}>
            <Text style={styles.textTitle}>Umur</Text>
            <Text style={styles.textContent}>{moment().diff(moment(props.chat.patient.birthdate), 'years')} Tahun</Text>
          </View>
          <View style={styles.card2}>
            <Text style={styles.textTitle}>Gol. Darah</Text>
            <Text style={styles.textContent}>{props.chat.patient.bloodType}</Text>
          </View>
          <View style={styles.card2}>
            <Text style={styles.textTitle}>Berat Badan</Text>
            <Text style={styles.textContent}>{props.chat.patient.weight} Kg</Text>
          </View>
          <View style={styles.card2}>
            <Text style={styles.textTitle}>Tinggi Badan</Text>
            <Text style={styles.textContent}>{props.chat.patient.height} Cm</Text>
          </View>
        </View>
      }
      <TouchableOpacity onPress={props.setShowPatientInformation} style={styles.card3}>
        <Text style={styles.textTop}>Informasi Pasien</Text>
        {
          props.showPatientInformation ?
          <UpIcon />:<DownIcon />
        }
      </TouchableOpacity>
    </View>
  </HeaderLayout>
);
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFCB05',
    flexDirection: 'row',
    height: 56,
  },
  left: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 56,
    width: 56,
  },
  middle: {
    flex: 1,
    justifyContent: 'center',
  },
  right: {
    justifyContent: 'center',
    paddingRight: 16,
  },
  title: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
    fontSize: 16,
    lineHeight: 19,
  },
  timer: {
    backgroundColor: '#FFF',
    borderRadius: 4,
    flexDirection: 'row',
    padding: 8,
  },
  card: {
    backgroundColor: 'white',
    zIndex: 3,
  },
  card1: {
    flexDirection: 'row',
    marginVertical: 6,
    backgroundColor: 'white'
  },
  card2: {
    width: '25%',
    borderRightColor: '#EBECF0',
    borderRightWidth: 1,
    paddingVertical: 10,
  },
  textTitle: {
    textAlign: 'center',
    marginBottom: 10,
    color: '#999DAD',
    fontSize: 12,
    fontFamily: 'Quicksand-Medium',
    lineHeight: 15,
  },
  textContent: {
    textAlign: 'center',
    color: '#373F50',
    fontSize: 14,
    fontFamily: 'Quicksand-Bold',
    lineHeight: 18,
  },
  card3: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 4,
  },
  textTop: {
    textAlign: 'center',
    marginRight: 8,
    color: '#373F50',
    fontSize: 14,
    fontFamily: 'Quicksand-Medium',
    lineHeight: 18,
  },
});
