import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { moment } from '../../utils';

export const DoctorMessage = (props) => (
  <View style={styles.container}>
    <View style={styles.content}>
      <Text style={styles.text}>
        Selamat siang. Saya {props.chat.doctor.name}. Ada yang bisa saya bantu?
        Silahkan sampaikan keluhan Anda.
      </Text>
    </View>
    <Text style={styles.dateTime}>
      {props.chat.acceptedAt && moment(props.chat.acceptedAt).format('hh.mm')}
    </Text>
  </View>
);
const styles = StyleSheet.create({
  container: {
    marginBottom: 16,
  },
  content: {
    backgroundColor: '#FFF',
    borderRadius: 4,
    padding: 8,
    paddingBottom: 12,
    marginBottom: 8,
    marginLeft: 16,
  },
  text: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    lineHeight: 18,
  },
  dateTime: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
  },
});
