import React from 'react';
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { moment } from '../../utils';
import { DoctorSuggestion } from './doctor-suggestion';

export const ChatMessageItem = (props) => {
  const contentStyle = [styles.content];
  const dateTimeStyle = [styles.dateTime];

  if (props.message.type === 'patient') {
    contentStyle.push({
      alignSelf: 'flex-start',
      backgroundColor: '#FFF9E0',
      marginLeft: 0,
      marginRight: 16,
    });
    dateTimeStyle.push({
      textAlign: 'left',
    });
  }

  function _renderContent() {
    const words = props.message.text.split(' ');

    if (words[0] === '[photo]') {
      const imageUrl = words[1];
      return (
        <View style={contentStyle}>
          <TouchableOpacity
            style={{
              alignSelf: 'center',
            }}
            onPress={() => props.onPhoto(imageUrl)}
          >
            <Image
              style={{
                height: 100,
                width: 100,
              }}
              source={{
                uri: imageUrl,
              }}
            />
          </TouchableOpacity>
        </View>
      );
    } else if (words[0] === '[doctor-note]') {
      // props.onNoteExist()
      return (
        <DoctorSuggestion
          chat={props.message.chat}
          onDeleteNote={props.onDeleteNote}
          onEditNote={props.onEditNote}
        />
      );
    }

    return (
      <View style={contentStyle}>
        <Text style={styles.text}>{props.message.text}</Text>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      {_renderContent()}
      <Text style={dateTimeStyle}>
        {moment(props.message.createdAt).format('hh.mm')}
      </Text>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    marginBottom: 16,
  },
  content: {
    alignSelf: 'flex-end',
    backgroundColor: '#FFF',
    borderRadius: 4,
    padding: 8,
    paddingBottom: 12,
    marginBottom: 8,
    marginLeft: 16,
    minWidth: Dimensions.get('window').width / 2,
  },
  text: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    lineHeight: 18,
  },
  dateTime: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
    textAlign: 'right',
  },
});
