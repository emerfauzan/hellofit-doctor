import React from 'react';
import { ScrollView, StyleSheet, Text } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Header, Page } from '../../components';

export const HelpDetail = (props) => (
  <Page style={styles.container}>
    <Header title="FAQ" onBack={() => Actions.pop()} />
    <ScrollView contentContainerStyle={styles.content}>
      <Text style={styles.title}>{props.help.question}</Text>
      <Text style={styles.description}>{props.help.answer}</Text>
    </ScrollView>
  </Page>
);
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
  },
  content: {
    padding: 16,
  },
  title: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
    fontSize: 16,
  },
  description: {
    color: '#373F50',
    fontFamily: 'Quicksand-Regular',
    lineHeight: 18,
    marginTop: 16,
  },
});
