import React, { useEffect } from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { useObserver } from 'mobx-react-lite';
import moment from 'moment';
import { Header, Page } from '../../components';
import { chatMedicineStore } from '../../stores';
import { formatPrice } from '../../utils';

export const ChatHistoryDetail = (props) => {
  useEffect(() => {
    chatMedicineStore.find({
      query: {
        chatId: props.chat.id,
      },
    });
  }, [props.chat.id]);
  const age = moment().diff(moment(props.chat.patient.birthdate), 'years');
  const { height, weight } = props.chat.patient;
  const { acceptedAt, closedAt } = props.chat;
  let chatDate = '';

  if (closedAt) {
    chatDate = moment(closedAt).format('DD MMM YYYY');
  }

  let duration = '';
  let timeRange = '';

  if (acceptedAt && closedAt) {
    let minutes = Math.floor(
      moment.duration(moment(closedAt).diff(acceptedAt)).asMinutes()
    );
    if (minutes === 0) minutes = 1;
    duration = `${minutes} menit`;
    const from = moment(acceptedAt).format('HH:mm');
    const to = moment(closedAt).format('HH:mm');
    timeRange = `(${from} - ${to})`;
  }

  let price = props.chat.doctor.price;

  if (props.chat.doctor.promo) {
    price = 0;
  }

  return useObserver(() => (
    <Page>
      <Header title={props.chat.patient.name} onBack={() => Actions.pop()} />
      <ScrollView>
        <View style={styles.sectionHeader}>
          <Text style={styles.title}>Pasien</Text>
        </View>
        <View style={styles.sectionBody}>
          <Text style={styles.name}>{props.chat.patient.name}</Text>
          <Text style={styles.description}>
            {age} Tahun ･ {height} cm ･ {weight} kg
          </Text>
        </View>
        <View style={styles.sectionHeader}>
          <Text style={styles.title}>Waktu Sesi</Text>
        </View>
        <View style={styles.sectionBody}>
          <View style={styles.hField}>
            <Text style={styles.hFieldName}>Tanggal</Text>
            <Text style={styles.hFieldValue}>{chatDate}</Text>
          </View>
          <View
            style={{
              height: 12,
            }}
          />
          <View style={styles.hField}>
            <Text style={styles.hFieldName}>Durasi</Text>
            <Text style={styles.hFieldValue}>
              {duration} {timeRange}
            </Text>
          </View>
        </View>
        <View style={styles.sectionHeader}>
          <Text style={styles.title}>Biaya Sesi</Text>
        </View>
        <View style={styles.sectionBody}>
          <View style={styles.hField}>
            <Text style={styles.hFieldName}>Sesi 30 menit</Text>
            <Text style={styles.hFieldValue}>{formatPrice(price)}</Text>
          </View>
        </View>
        <View style={styles.sectionHeader}>
          <Text style={styles.title}>Catatan Dokter</Text>
        </View>
        <View style={styles.sectionBody}>
          <View>
            <Text style={styles.vFieldName}>Diagnosa</Text>
            <Text style={styles.vFieldValue}>{props.chat.diagnose}</Text>
          </View>
          <View
            style={{
              height: 12,
            }}
          />
          <View>
            <Text style={styles.vFieldName}>Saran</Text>
            <Text style={styles.vFieldValue}>{props.chat.suggestion}</Text>
          </View>
        </View>
        {/* <View style={styles.sectionHeader}>
         <Text style={styles.title}>Rekomendasi Obat</Text>
        </View>
        {chatMedicineStore.data.map(chatMedicine => (
         <MedicineItem key={chatMedicine.id} chatMedicine={chatMedicine} />
        ))} */}
      </ScrollView>
    </Page>
  ));
};
const styles = StyleSheet.create({
  container: {
    borderColor: 'red',
    borderWidth: 2,
  },
  sectionHeader: {
    backgroundColor: '#EBECF0',
    height: 32,
    justifyContent: 'center',
    paddingHorizontal: 16,
  },
  title: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
    fontSize: 12,
  },
  sectionBody: {
    backgroundColor: '#FFF',
    padding: 16,
  },
  name: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
    fontSize: 16,
  },
  description: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
  },
  hField: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  hFieldName: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
  },
  hFieldValue: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
  },
  vFieldName: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
  },
  vFieldValue: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
  },
});
