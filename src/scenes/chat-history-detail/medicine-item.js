import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { formatPrice } from '../../utils';
import medicineImage from './medicine.png';

export const MedicineItem = (props) => (
  <View style={styles.container}>
    <View style={styles.left}>
      <Image source={medicineImage} style={styles.image} resizeMode="contain" />
    </View>
    <View style={styles.right}>
      <Text style={styles.name}>{props.chatMedicine.medicine.name}</Text>
      <Text>
        <Text style={styles.price}>
          {formatPrice(props.chatMedicine.medicine.price)}
        </Text>
        <Text style={styles.quantity}> x{props.chatMedicine.quantity}</Text>
      </Text>
      <Text style={styles.description}>{props.chatMedicine.dosage}</Text>
    </View>
  </View>
);
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    borderColor: '#EBECF0',
    borderBottomWidth: 1,
    flexDirection: 'row',
    height: 84,
  },
  left: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 68,
  },
  right: {
    justifyContent: 'center',
  },
  image: {
    height: 40,
    width: 40,
  },
  name: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
  },
  price: {
    color: '#FFCB05',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
  },
  quantity: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
  },
  description: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
  },
});
