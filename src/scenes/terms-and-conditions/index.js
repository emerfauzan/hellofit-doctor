import React from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Button, Page } from '../../components';

export const TermsAndConditions = (props) => (
  <Page>
    <ScrollView contentContainerStyle={styles.container}>
      <Text style={styles.title}>Syarat & Ketentuan Pengguna</Text>
      <Text style={styles.subtitle}>Aplikasi Hellofit</Text>
      <Text style={styles.paragraph}>
        Hellofit merupakan pihak ketiga yang mempertemukan Anda (tenaga medis)
        dengan pihak pengguna aplikasi.Pihak Hellofit tidak bertanggung jawab
        atas segala kerugian ataupun kerusakan pada pihak tenaga medis maupun
        pengguna aplikasi yang terjadi akibat hasil konsultasi.
      </Text>
      <Text style={styles.paragraph}>
        Pihak tenaga medis mengetahui, menyadari, dan menyetujui bahwa
        konsultasi dengan aplikasi e- healthcare bukanlah pengganti
        pertemuansecara langsung dan pemeriksaan fisik, dan oleh karena itu
        hasil konsultasi dengan aplikasi ini tidaklah menggantikan hasil
        konsultasi/diagnosis/saran pemeriksaan/ pengobatan tenaga medis, serta
        bukan merupakan second opinion yang sah.
      </Text>
      <Text style={styles.paragraph}>
        Pihak tenaga medis menyadari bahwa komunikasi melalui media internet
        tidaklah memungkiri adanya kemungkinan kesalahan persepsi,interpretasi,
        maupun kendala komunikasi lain yang berpotensi merugikan baik pihak
        tenaga medis maupun pengguna aplikasi. Mengingat perihal tersebut, pihak
        tenaga medis menyetujui bilamana terjadi kesalahpahaman yang
        mencapai/tidak terbatas pada ranah hukum, maka kendala tersebut akan
        diselesaikan antara pihak tenaga medis dan pengguna aplikasi.
      </Text>
      <Text style={styles.paragraph}>
        Segala informasi yang diberikan oleh tenaga medis melalui aplikasi, baik
        tertulis maupun lisan, termasuk didalamnya gambar, video, ataupun bentuk
        digital lain merupakan perwakilan dari pribadi tenaga medis yang
        bersangkutan, dan tidak merupakan perwakilan dari pihak Hellofit.
        Hellofit tidak bertanggung jawab atas segala bentuk kerugian, baik
        materiil maupun immaterial yang diakibatkan oleh misinformasi dalam
        bentuk apapun dari pihak tenaga medis.
      </Text>
      <View style={styles.button}>
        <Button
          label="Kembali ke Halaman Registrasi"
          onPress={() => Actions.pop()}
        />
      </View>
    </ScrollView>
  </Page>
);
const styles = StyleSheet.create({
  container: {
    padding: 16,
    paddingTop: 64,
  },
  title: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
    fontSize: 16,
  },
  subtitle: {
    color: '#FFCB05',
    fontFamily: 'Quicksand-Bold',
    fontSize: 16,
  },
  paragraph: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    lineHeight: 24,
    marginTop: 16,
  },
  item: {
    flexDirection: 'row',
    marginTop: 4,
  },
  bullet: {
    backgroundColor: '#373F50',
    borderRadius: 2,
    height: 4,
    marginRight: 12,
    marginTop: 12,
    width: 4,
  },
  itemText: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    lineHeight: 24,
  },
  button: {
    padding: 16,
  },
});
