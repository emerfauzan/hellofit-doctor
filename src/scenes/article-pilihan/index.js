import React, { useEffect } from 'react';
import { ScrollView } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { useObserver } from 'mobx-react-lite';
import { Header, Page } from '../../components';
import { articleStore } from '../../stores';
import { ArticleGrid } from './article-grid';
import { ArticleTrending } from './article-trending';
import { Category } from './category';

export const ArticlePilihan = (props) => {
  useEffect(() => {
    articleStore.find();
  }, []);

  function _handleBack() {
    articleStore.find({
      query: {
        featured: true,
      },
    });
    Actions.pop();
  }

  return useObserver(() => (
    <Page>
      <Header title="Artikel Pilihan" onBack={() => _handleBack()} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <ArticleTrending
          articles={articleStore.data}
          onPressArticle={(article) =>
            Actions.push('article-detail', {
              article,
            })
          }
        />
        <Category
          articles={articleStore.data}
          selected={3}
          onPressCategory={(article) =>
            {}
          }
        />
        <ArticleGrid
          articles={articleStore.data}
          onPressArticle={(article) =>
            Actions.push('article-detail', {
              article,
            })
          }
        />
      </ScrollView>
    </Page>
  ));
};
