import React from 'react';
import {
  Dimensions,
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
} from 'react-native';

export const ArticleTrending = (props) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Trending</Text>
      <ScrollView
        horizontal
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={styles.scrollView}
      >
        {props.articles.map((article) => (
          <TouchableOpacity
            key={article.id}
            onPress={() => props.onPressArticle(article)}
          >
            <ImageBackground
              source={{
                uri: article.photo,
              }}
              style={styles.article}
            >
              <View style={styles.articleFooter}>
                <Text style={styles.articleTitle}>
                  {article.title.split(' ').slice(0, 5).join(' ') + '...'}
                </Text>
              </View>
            </ImageBackground>
          </TouchableOpacity>
        ))}
      </ScrollView>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
  },
  scrollView: {
    backgroundColor: '#FFF',
    paddingHorizontal: 6,
    paddingRight: 16,
  },
  articles: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
  article: {
    // backgroundColor: 'rgba(55, 63, 80, 0.8)',
    height: 80,
    justifyContent: 'flex-end',
    overflow: "hidden",
    width: 140,
    marginLeft: 10,
    borderRadius: 5
  },
  articleFooter: {
    height: 30,
    paddingHorizontal: 6,
    marginBottom: 4,
  },
  articleTitle: {
    color: '#FFFFFF',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
    lineHeight: 15,
  },
  title: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    fontSize: 16,
    lineHeight: 20,
    paddingHorizontal: 16,
    paddingBottom: 10,
    paddingTop: 14,
  },
});
