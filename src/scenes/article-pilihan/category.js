import React from 'react';
import {
  Dimensions,
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
} from 'react-native';

export const Category = (props) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Kategori</Text>
      <ScrollView
        horizontal
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={styles.scrollView}
      >
        {props.articles.map((article) => (
          <TouchableOpacity
            key={article.id}
            onPress={() => props.onPressCategory(article)}
          >
            <View style={props.selected === article.id ? styles.articleSelected : styles.article}>
                <Text style={props.selected === article.id ? styles.articleTitleSelected : styles.articleTitle}>
                  {article.title.split(' ')[0]}
                </Text>
            </View>
          </TouchableOpacity>
        ))}
      </ScrollView>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
  },
  scrollView: {
    backgroundColor: '#FFF',
    paddingHorizontal: 11,
    paddingRight: 16,
  },
  article: {
    backgroundColor: '#F5F4F4',
    justifyContent: 'center',
    alignContent: 'center',
    paddingHorizontal: 20,
    paddingVertical: 7,
    marginLeft: 5,
    borderRadius: 5
  },
  articleSelected: {
    backgroundColor: '#FFCB05',
    justifyContent: 'center',
    alignContent: 'center',
    paddingHorizontal: 20,
    paddingVertical: 7,
    marginLeft: 5,
    borderRadius: 5
  },
  articleTitle: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    fontSize: 14,
    lineHeight: 15,
    opacity: 0.5
  },
  articleTitleSelected: {
    color: '#FFFFFF',
    fontFamily: 'Quicksand-Medium',
    fontSize: 14,
    lineHeight: 15,
  },
  title: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    fontSize: 16,
    lineHeight: 20,
    paddingHorizontal: 16,
    paddingBottom: 10,
    paddingTop: 14,
  },
});
