import React from 'react';
import {
  Dimensions,
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

const articleWidth = (Dimensions.get('window').width - 136);
const articleHeight = 1.2 * articleWidth;
export const ArticleGrid = (props) => {
  return (
    <View style={styles.container}>
        {props.articles.map((article) => (
          <TouchableOpacity
            key={article.id}
            style={styles.articles}
            onPress={() => props.onPressArticle(article)}
          >
            <ImageBackground
              source={{
                uri: article.photo,
              }}
              style={styles.article}
            >
            </ImageBackground>
            <View style={styles.articleFooter}>
              <Text style={styles.articleTitle}>
                {article.title.split(' ').slice(0, 5).join(' ') + '...'}
              </Text>
              <Text style={styles.articleTitle2}>
                hellofitNews | 5 menit yang lulu
              </Text>
              <Text style={styles.articleTitle2}>
                12 views
              </Text>
            </View>
          </TouchableOpacity>
        ))}
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    padding: 16,
  },
  articles: {
    flexDirection: 'row'
  },
  article: {
    height: 120,
    marginVertical: 4,
    width: 120,
    overflow: 'hidden',
    borderRadius: 5
  },
  articleFooter: {
    width: articleWidth,
    paddingHorizontal: 16,
    justifyContent: 'center'
  },
  articleTitle: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    fontSize: 16,
    lineHeight: 20,
  },
  articleTitle2: {
    color: '#373F50',
    fontFamily: 'Quicksand-Regular',
    fontSize: 10,
    lineHeight: 13,
    marginTop: 6,
  },
});
