import React, { forwardRef } from 'react';
import { StyleSheet, Text, TextInput, View } from 'react-native';
import { TextInputMask } from 'react-native-masked-text';

export const TextInputField = forwardRef((props, ref) => {
  const { label, options, type, style, ...others } = props;
  return (
    <View style={styles.container}>
      <Text style={styles.label}>{props.label}</Text>
      {props.disable && <Text style={styles.label2}>*tidak dapat diubah</Text>}
      {type && options ? (
        <TextInputMask
          ref={ref}
          selectionColor="#FFCB05"
          placeholderTextColor="#C9CDD6"
          style={[styles.input, style]}
          {...others}
          type={type}
          options={options}
        />
      ) : (
        <TextInput
          editable={!props.disable}
          selectionColor="#FFCB05"
          placeholderTextColor="#C9CDD6"
          style={[styles.input, style, props.disable && {backgroundColor: '#EBECF0'}]}
          {...others}
        />
      )}
    </View>
  );
});
const styles = StyleSheet.create({
  container: {
    marginBottom: 16,
  },
  label: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
  },
  label2: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
    position: 'absolute',
    right: 0
  },
  input: {
    borderColor: '#373F50',
    borderRadius: 4,
    borderWidth: 1,
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    fontSize: 14,
    height: 40,
    marginTop: 8,
    paddingHorizontal: 16,
  },
});
