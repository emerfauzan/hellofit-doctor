import React, { useState, useEffect } from 'react';
import { ScrollView, StyleSheet, View, BackHandler, Text } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Toast from 'react-native-simple-toast';
import { Button, Header, Page, TextInputField } from '../../components';
import { chatMessageStore, chatProgressStore } from '../../stores';
import { feathersClient } from '../../utils';
import moment from 'moment';
import "moment/locale/id"

export const DoctorNote = (props) => {
  const [loading, setLoading] = useState(false);
  const [diagnose, setDiagnose] = useState(
    props.isNew ? '' : props.chat.diagnose || ''
  );
  const [suggestion, setSuggestion] = useState(
    props.isNew ? '' : props.chat.suggestion || ''
  );
  const isValid = diagnose.length > 0 && suggestion.length > 0;

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackButton);

    return function cleanup() {
      BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
    }
  }, [])

  async function _submit() {
    setLoading(true);

    try {
      await feathersClient.service('chats').patch(props.chat.id, {
        diagnose,
        suggestion,
      });

      if (props.isNew) {
        await chatMessageStore.create({
          chatId: props.chat.id,
          text: '[doctor-note]',
        });
      }

      if (!props.isEnd) {
        await chatMessageStore.find({
          query: {
            chatId: props.chat.id,
            $sort: {
              createdAt: 1,
            },
          },
        });
      }

      await chatProgressStore.patch(props.chat.id, {
        isNoteFilled: true
      });

      if (props.isEnd) {
        Actions.reset('main')
      } else {
        Actions.pop();
      }
      setLoading(false);
    } catch (error) {
      setLoading(false);
      setTimeout(() => {
        Toast.show(error.message);
      }, 100);
    }
    chatMessageStore.markIds.delete(props.chat.id);
    chatMessageStore.deleteBadge(props.chat.id)
  }

  function handleBackButton() {
    if (props.isEnd) {
      return true;
    }
  }

  return (
    <Page loading={loading} style={styles.container}>
      {
        props.isEnd ?
          <View style={{paddingLeft: 16, backgroundColor: '#FFCB05'}}>
            <Header title="Catatan Dokter" />
          </View>
          : <Header title="Catatan Dokter" onClose={() => { Actions.pop() }} />
      }
      <ScrollView>
        <Text style={styles.patientName}>{props.chat.patient.name}</Text>
        <View style={styles.card1}>
          <View style={styles.card2}>
            <Text style={styles.textTitle}>Umur</Text>
            <Text style={styles.textContent}>{moment().diff(moment(props.chat.patient.birthdate), 'years')} Tahun</Text>
          </View>
          <View style={styles.card2}>
            <Text style={styles.textTitle}>Gol. Darah</Text>
            <Text style={styles.textContent}>{props.chat.patient.bloodType}</Text>
          </View>
          <View style={styles.card2}>
            <Text style={styles.textTitle}>Berat Badan</Text>
            <Text style={styles.textContent}>{props.chat.patient.weight} Kg</Text>
          </View>
          <View style={styles.card2}>
            <Text style={styles.textTitle}>Tinggi Badan</Text>
            <Text style={styles.textContent}>{props.chat.patient.height} Cm</Text>
          </View>
        </View>
        <View style={styles.card3}>
          <Text style={styles.dateText}>{moment(props.chat.createdAt).format('DD MMMM YYYY')}</Text>
          <Text style={styles.dateText}>{moment(props.chat.acceptedAt).format('HH:mm')} - {props.chat.closedAt ? moment(props.chat.closedAt).format('HH:mm') : 'Sekarang'}</Text>
        </View>
        <View style={styles.content}>
          <TextInputField
            autoCorrect={false}
            autoCompleteType="off"
            label="Diagnosa"
            multiline
            numberOfLines={3}
            message='*maksimal 500 karakter'
            style={{
              textAlignVertical: 'top',
              height: 96,
            }}
            value={diagnose}
            onChangeText={(text) => {
              if(text.length < 500){
                setDiagnose(text)
              }
            }}
          />
          <TextInputField
            autoCorrect={false}
            autoCompleteType="off"
            label="Saran"
            multiline
            numberOfLines={3}
            message='*maksimal 500 karakter'
            style={{
              textAlignVertical: 'top',
              height: 96,
            }}
            value={suggestion}
            onChangeText={(text) => {
              if(text.length < 500){
                setSuggestion(text)
              }
            }}
          />
        </View>
      </ScrollView>
      <View style={styles.footer}>
        <Button
          disabled={!isValid}
          label="Bagikan Catatan"
          onPress={() => _submit()}
        />
      </View>
    </Page>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
  },
  content: {
    padding: 16,
  },
  footer: {
    borderColor: '#EBECF0',
    borderTopWidth: 1,
  },
  patientName: {
    color: '#373F50',
    fontSize: 16,
    lineHeight: 20,
    textAlign: 'center',
    fontFamily: 'Quicksand-Bold',
    marginVertical: 5,
  },
  card1: {
    flexDirection: 'row',
    marginVertical: 6,
  },
  card2: {
    width: '25%',
    borderRightColor: '#EBECF0',
    borderRightWidth: 1,
    paddingVertical: 10,
  },
  textTitle: {
    textAlign: 'center',
    marginBottom: 10,
    color: '#999DAD',
    fontSize: 12,
    fontFamily: 'Quicksand-Medium',
    lineHeight: 15,
  },
  textContent: {
    textAlign: 'center',
    color: '#373F50',
    fontSize: 14,
    fontFamily: 'Quicksand-Bold',
    lineHeight: 18,
  },
  card3: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 10,
    paddingHorizontal: 16,
    backgroundColor: '#EBECF0',
  },
  dateText: {
    color: '#373F50',
    fontSize: 14,
    fontFamily: 'Quicksand-Bold',
  },
});
