import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { ScheduleTimeItem } from './schedule-time-item';
import { SwitchInput } from './switch-input';

const dayNames = {
  sunday: 'Minggu',
  monday: 'Senin',
  tuesday: 'Selasa',
  wednesday: 'Rabu',
  thursday: 'Kamis',
  friday: 'Jumat',
  saturday: 'Sabtu',
};
export const ScheduleItem = (props) => {
  return (
    <View style={styles.container}>
      <View style={styles.top}>
        <Text style={styles.day}>{dayNames[props.schedule.day]}</Text>
        <SwitchInput
          value={props.schedule.enabled}
          onChangeValue={() =>
            props.onChangeSchedule({
              ...props.schedule,
              enabled: !props.schedule.enabled,
            })
          }
        />
      </View>
      <View style={styles.bottom}>
        {props.schedule.enabled ? (
          <View>
            {props.schedule.times.map((scheduleTime, index) => (
              <ScheduleTimeItem
                key={scheduleTime.id}
                scheduleTime={scheduleTime}
                onChangeScheduleTime={(scheduleTime) => {
                  const { times } = props.schedule;
                  times[index] = scheduleTime;
                  const schedule = { ...props.schedule, times };
                  props.onChangeSchedule(schedule);
                }}
              />
            ))}
            <TouchableOpacity
              style={styles.textButton}
              onPress={props.onAddSchedule}
            >
              <Text style={styles.textButtonLabel}>+ Tambah Waktu</Text>
            </TouchableOpacity>
          </View>
        ) : (
          <Text style={styles.offline}>Offline</Text>
        )}
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    marginBottom: 1,
  },
  top: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 16,
    paddingTop: 8,
  },
  bottom: {
    paddingLeft: 16,
    paddingBottom: 16,
  },
  day: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
  },
  offline: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
  },
  textButton: {
    alignSelf: 'flex-start',
    flexDirection: 'row',
    height: 32,
    paddingTop: 16,
  },
  textButtonLabel: {
    color: '#FFCB05',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
    lineHeight: 14,
  },
});
