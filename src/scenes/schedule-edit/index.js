import React, { useEffect, useState } from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { useObserver } from 'mobx-react-lite';
import Toast from 'react-native-simple-toast';
import { Header, Page } from '../../components';
import { scheduleEditStore } from '../../stores';
import { ScheduleItem } from './schedule-item';

export const ScheduleEdit = (props) => {
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    scheduleEditStore.reset();
  }, []);

  async function _submit() {
    setLoading(true);

    try {
      await scheduleEditStore.submit();
      setLoading(false);
      Actions.pop();
    } catch (error) {
      setLoading(false);
      setTimeout(() => {
        Toast.show(error.message);
      }, 100);
    }
  }

  let targetColor = '#E6495A';

  if (scheduleEditStore.hours >= 20) {
    targetColor = '#3BD15E';
  }

  return useObserver(() => (
    <Page loading={loading}>
      <Header
        title="Atur Jadwal Online"
        buttonRight="Simpan"
        onBack={() => Actions.pop()}
        onRight={() => _submit()}
      />
      <ScrollView>
        <View style={styles.target}>
          <Text style={styles.targetLabel}>Minimal Online Seminggu</Text>
          <Text style={styles.targetValue}>
            <Text
              style={{
                color: targetColor,
              }}
            >
              {scheduleEditStore.hours}
            </Text>
            <Text> / 20 jam</Text>
          </Text>
        </View>
        {scheduleEditStore.data.map((schedule) => (
          <ScheduleItem
            key={schedule.id}
            schedule={schedule}
            onAddSchedule={() => scheduleEditStore.addTime(schedule.id)}
            onChangeSchedule={(schedule) =>
              scheduleEditStore.records.set(schedule.id, schedule)
            }
          />
        ))}
      </ScrollView>
    </Page>
  ));
};
const styles = StyleSheet.create({
  container: {
    borderColor: 'red',
    borderWidth: 2,
  },
  target: {
    backgroundColor: '#FFF',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 8,
    padding: 16,
  },
  targetLabel: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
  },
  targetValue: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
  },
});
