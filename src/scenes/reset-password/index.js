import React, { useState } from 'react';
import { StyleSheet, ScrollView, Text } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { useObserver } from 'mobx-react-lite';
import Toast from 'react-native-simple-toast';
import {
  ButtonFooter,
  Form,
  Page,
  TextInputField,
} from '../../components';
import { forgotPasswordStore } from '../../stores';
import { Title } from './title';

export const ResetPassword = (props) => {
  const [sent, setSent] = useState(false);
  const [loading, setLoading] = useState(false);
  const [id, setId] = useState(props.id);

  async function _submit() {
    setLoading(true);
    forgotPasswordStore.resetPasswordForm.id = id

    try {
      await forgotPasswordStore.update();
      setSent(true);
      setLoading(false);
      setTimeout(() => {
        Toast.show("Ubah password berhasil. Silahkan login kembali.");
      }, 100);
      Actions.reset('login');
    } catch (error) {
      setLoading(false);
      setTimeout(() => {
        Toast.show(error.message);
      }, 100);
    }
  }

  return useObserver(() => (
    <Page style={styles.container} loading={loading}>
      <ScrollView>
        <Title />
        <Form>
          <Text style={styles.notes}>Masukkan kata sandi anda dengan benar.</Text>
          <TextInputField
            autoCapitalize="none"
            label="Masukkan Kata Sandi Baru"
            placeholder="Minimal 6 karakter"
            secureTextEntry
            value={forgotPasswordStore.resetPasswordForm.password}
            onChangeText={(text) => {
              forgotPasswordStore.resetPasswordForm.password = text;
            }}
          />

          <TextInputField
            autoCapitalize="none"
            label="Ulangi Kata Sandi Baru"
            placeholder="Minimal 6 karakter"
            secureTextEntry
            value={forgotPasswordStore.resetPasswordForm.cpassword}
            onChangeText={(text) => {
              forgotPasswordStore.resetPasswordForm.cpassword = text;
            }}
          />
        </Form>
        <ButtonFooter
          disabled={!forgotPasswordStore.resetPasswordFormValid}
          label="Ubah Kata Sandi"
          onPress={() => _submit()}
        />
      </ScrollView>
    </Page>
  ));
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
  },
  footer: {
    paddingBottom: 32,
  },
  notes: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
    marginBottom: 10
  }
});
