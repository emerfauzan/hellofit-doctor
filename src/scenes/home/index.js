import React, { useEffect } from 'react';
import { ScrollView, DeviceEventEmitter, Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { useObserver } from 'mobx-react-lite';
import messaging from '@react-native-firebase/messaging';
import { ArticleGrid, Page } from '../../components';
import { articleStore, bannerStore, profileStore } from '../../stores';
import { feathersClient } from '../../utils';
import { ChatAcceptedDialog } from './chat-accepted-dialog';
import { ChatCompletedDialog } from './chat-completed-dialog';
import { ChatRejectedDialog } from './chat-rejected-dialog';
import { Header } from './header';
import PushNotification from 'react-native-push-notification';
import { result } from 'lodash';

import { Banners } from './banners';
import { SectionTitle } from './section-title';
import { Percentage } from './percentage';
import { ReportChat } from './report-chat';
import { Graphic } from './graphic';

export const Home = (props) => {
  useEffect(() => {
    async function run() {
      const result = await feathersClient.get('authentication');
      if(result.user.type === "patient"){
        _logout()
      }
    }

    async function _logout() {
      try {
        await feathersClient.logout();
        Actions.reset('login');
      } catch (error) { }
    }

    run()
  }, [])

  useEffect(() => {
    // if(props.notifData !== ""){
    //   Actions.push('chat-detail', {
    //     chat: props.notifData,
    //   });
    // }
    
    if (profileStore.doctor?.online) {
      PushNotification.getDeliveredNotifications((notifs) => {
        const ids = notifs.map((notif) => notif.identifier);

        if (!ids.includes('1')) {
          PushNotification.localNotification({
            id: '1',
            message: 'Status is Online',
            ongoing: true,
            soundName: 'notif_hellofit.mp3',
          });
          DeviceEventEmitter.addListener('notificationActionReceived', function(action){
            console.log ('Notification action received: ' + action);
            const info = JSON.parse(action.dataJSON);
            if (info.action == 'Accept') {
              console.log("ACC")
            } else if (info.action == 'Reject') {
              // Do work pertaining to Reject action here
            }
            // Add all the required actions handlers
          });
        }
      });
    } else {
      PushNotification.clearLocalNotification(1);
    }

  }, []);

  
  
  useEffect(() => {
    async function run() {
      const { user } = await feathersClient.get('authentication');
      if (!messaging().isDeviceRegisteredForRemoteMessages) {
        await messaging().registerDeviceForRemoteMessages();
      }
      const fcmToken = await messaging().getToken();
      await feathersClient.service('users').patch(user.id, {
        fcmToken,
      });

  
    }

    run();
  }, []);
  useEffect(async () => {
    await profileStore.fetch();
    articleStore.find({
      query: {
        featured: true,
      },
    });
    bannerStore.find();
    // profileStore.resume();
  }, []);

  useEffect(() => {
    getChatData();
  }, [])

  async function getChatData() {
    const { doctor } = profileStore
    const chatData = feathersClient.service('chats').find({
      query: {
        doctorId: doctor.id,
        isNoteFilled: false,
        status: 'completed',
        $sort: {
          createdAt: 1,
        },
      }
    }).then((result) => {
      if (result.data.length > 0) {
        Actions.push('doctor-note', {
          isNew: true,
          isEnd: true,
          fromPatient: false,
          chat: result.data[0],
        })
      }
    })
  }

  return useObserver(() => (
    <Page>
        <Header
          name={profileStore.doctor ? profileStore.doctor.name : ''}
        // onChat={() => Actions.push('covid-menu')}
        // onMedicine={() => Actions.push('medicine')}
        // onNotification={() => Actions.push('notification-list')}
        // onSchedule={() => Actions.push('hospital-schedule')}
        />
      <ScrollView style={{backgroundColor: '#FFFFFF'}}>
        <ReportChat />
        <Percentage />
        <Graphic />
        {/* <Banners banners={bannerStore.data} />
        <SectionTitle onPress={() => Actions.push('article-pilihan')}/>
        <ArticleGrid
          articles={articleStore.data}
          onPressArticle={(article) =>
            Actions.push('article-detail', {
              article,
            })
          }
        /> */}
      </ScrollView>
      <ChatAcceptedDialog />
      <ChatCompletedDialog />
      <ChatRejectedDialog />
    </Page>
  ));
};
