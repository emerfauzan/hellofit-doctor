import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { HeaderLayout } from '../../../components';
import { NotificationIcon } from '../notification-icon';
import { MenuButton } from './menu-button';
import NewChat from './new-chat.svg';
import OngoingChat from './ongoing-chat.svg';
import Forum from './forum.svg';

export const Header = (props) => (
  <HeaderLayout>
    <View style={styles.container}>
      <View style={styles.top}>
        <View>
          <Text style={styles.title}>Hai, {props.name}</Text>
          <Text style={styles.description}>Bagaimana kesehatan Anda?</Text>
        </View>
        {/* <NotificationIcon onPress={props.onNotification} /> */}
      </View>
      {/* <View style={styles.bottom}>
        <MenuButton
          icon={<NewChat />}
          label1="Chat Baru"
          onPress={props.onChat}
        />
        <View style={styles.space} />
        <MenuButton
          // comingSoon
          // disabled
          icon={<OngoingChat />}
          label1="Chat Sedang"
          label2="Berjalan"
          onPress={props.onSchedule}
        />
        <View style={styles.space} />
        <MenuButton
          // comingSoon
          // disabled
          icon={<Forum />}
          label1="Forum"
          onPress={props.onMedicine}
        />
      </View> */}
    </View>
  </HeaderLayout>
);
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFCB05',
    marginBottom: 4,
    padding: 16,
  },
  top: {
    backgroundColor: '#FFCB05',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  bottom: {
    flexDirection: 'row',
    marginTop: 16,
  },
  title: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
    fontSize: 24,
  },
  description: {
    color: '#373F50',
    fontFamily: 'Quicksand-Regular',
  },
  space: {
    width: 8,
  },
});
