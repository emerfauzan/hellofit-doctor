import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

export const MenuButton = (props) => (
  <TouchableOpacity style={styles.container} {...props}>
    {props.icon}
    <View style={styles.description}>
      <Text style={styles.label}>{props.label1}</Text>
      <Text style={styles.label}>{props.label2}</Text>
    </View>
    {props.comingSoon && (
      <View style={styles.comingSoon}>
        <Text style={styles.comingSoonLabel}>Segera Hadir</Text>
      </View>
    )}
  </TouchableOpacity>
);
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#FFF',
    borderRadius: 4,
    flex: 1,
    paddingVertical: 12,
    justifyContent: 'center',
  },
  description: {
    paddingTop: 10,
  },
  label: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
    textAlign: 'center',
  },
  comingSoon: {
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
    borderRadius: 4,
    bottom: 0,
    justifyContent: 'center',
    left: 0,
    position: 'absolute',
    right: 0,
    top: 0,
  },
  comingSoonLabel: {
    color: '#FFCB05',
    fontFamily: 'Quicksand-Medium',
    textAlign: 'center',
  },
});
