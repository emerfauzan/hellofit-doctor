import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

export const SectionTitle = (props) => (
  <View style={styles.container}>
    <View>
      <Text style={styles.title1}>
        Artikel <Text style={styles.title2}>Pilihan</Text>
      </Text>
    </View>
    <TouchableOpacity onPress={props.onPress}>
      <Text style={styles.textButtonLabel}>Lihat Semua</Text>
    </TouchableOpacity>
  </View>
);
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#FFF',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 8,
    paddingHorizontal: 16,
    paddingTop: 16,
  },
  title1: {
    color: '#373F50',
    fontFamily: 'Quicksand-Regular',
    fontSize: 16,
  },
  title2: {
    color: '#FFCB05',
    fontFamily: 'Quicksand-Bold',
    fontSize: 16,
  },
  textButtonLabel: {
    padding: 4,
    color: '#9CA2B4',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
  },
});
