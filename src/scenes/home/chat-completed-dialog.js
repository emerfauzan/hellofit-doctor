import React, { useEffect, useState } from 'react';
import { Actions } from 'react-native-router-flux';
import { InfoDialog } from '../../components';
import { feathersClient } from '../../utils';

export const ChatCompletedDialog = (props) => {
  const [chat, setChat] = useState();
  const [showDialog, setShowDialog] = useState(false);
  const [description, setDescription] = useState('');
  useEffect(() => {
    const listener = (chat) => {
      if (
        chat.comment == null &&
        chat.status === 'completed' &&
        Actions.currentScene !== 'chat-detail'
      ) {
        setChat(chat);
        setDescription(`${chat.doctor.name} telah mengakhiri sesi`);
        // setShowDialog(true);
      }
    };

    feathersClient.service('chats').on('patched', listener);
    return function cleanup() {
      feathersClient.service('chats').removeListener('patched', listener);
    };
  }, []);

  function _handleConfirm() {
    setShowDialog(false);
    // Actions.push('rating', {
    //   chat,
    // });
  }



  return (
    <InfoDialog
      visible={showDialog}
      title="Sesi Telah Berakhir"
      description={description}
      confirmLabel="Beri Rating"
      onConfirm={() => _handleConfirm()}
    />
  );
};
