import React, { useEffect, useState } from 'react';
import { Actions } from 'react-native-router-flux';
import { InfoDialog } from '../../components';
import { feathersClient } from '../../utils';

export const ChatRejectedDialog = (props) => {
  const [showDialog, setShowDialog] = useState(false);
  const [description, setDescription] = useState('');
  useEffect(() => {
    const listener = (chat) => {
      if (chat.status === 'rejected') {
        setDescription(`${chat.doctor.name} telah menolak permintaan Anda`);
        // setShowDialog(true);
      }
    };

    feathersClient.service('chats').on('patched', listener);
    return function cleanup() {
      feathersClient.service('chats').removeListener('patched', listener);
    };
  }, []);
  return (
    <InfoDialog
      visible={showDialog}
      title="Permintaan Ditolak"
      description={description} // cancelLabel="Refund"
      cancelLabel="Kembali"
      confirmLabel="Ganti Dokter"
      onCancel={() => {
        setShowDialog(false);
        Actions.pop(); // TODO: Actions.replace('refund');
      }}
      onConfirm={() => {
        setShowDialog(false);

        if (Actions.currentScene === 'waiting') {
          const { fromScene } = Actions.currentParams;

          if (fromScene === 'symptom') {
            Actions.pop();
          } else if (fromScene === 'chat-list') {
            Actions.replace('doctor-list', {
              specialty: {
                id: 1,
                name: 'Dokter Umum',
              },
            });
          } else {
            Actions.push('doctor-list', {
              specialty: {
                id: 1,
                name: 'Dokter Umum',
              },
            });
          }
        } else {
          Actions.push('doctor-list', {
            specialty: {
              id: 1,
              name: 'Dokter Umum',
            },
          });
        }
      }}
    />
  );
};
