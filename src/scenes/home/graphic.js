import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Dimensions } from 'react-native';
import { LineChart } from "react-native-chart-kit";

const screenWidth = Dimensions.get("window").width;

export const Graphic = (props) => (
  <View style={styles.container}>
    <Text style={styles.text}>Grafik Perolehan Pasien :</Text>
    <LineChart
      data={{
        labels: ["Sep", "Oct", "Nov", "Dec", "Jan", "Feb"],
        datasets: [
          {
            data: [20, 45, 28, 80, 99, 43],
            // color: (opacity = 1) => `rgba(134, 65, 244, ${opacity})`, // optional
            // strokeWidth: 2 // optional
          }
        ],
        // legend: ["Rainy Days"] // optional
      }}
      width={screenWidth-50}
      height={173}
      verticalLabelRotation={20}
      chartConfig={{
        backgroundGradientFrom: "#FFFFFF",
        // backgroundGradientFromOpacity: 1,
        backgroundGradientTo: "#FFFFFF",
        // backgroundGradientToOpacity: 1,
        color: () => '#FFCB05',
        strokeWidth: 2, // optional, default 3
        // barPercentage: 0.5,
        // useShadowColorFromDataset: false // optional
      }}
      bezier
    />
  </View>
);
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    // flexDirection: 'row',
    marginTop: 36,
    margin: 20,
    paddingVertical: 12,
    // height: 173,
    borderRadius: 5,
    elevation: 10,
  },
  text: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
    fontSize: 14,
    marginLeft: 20,
    marginBottom: 20,
  },
});
