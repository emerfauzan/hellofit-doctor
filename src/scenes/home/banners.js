import React from 'react';
import { Dimensions, Image, StyleSheet, View } from 'react-native';
import Swiper from 'react-native-swiper';

const bannerWidth = Dimensions.get('window').width - 32;

const ActiveDot = () => <View style={styles.activeDot} />;

const Dot = () => <View style={styles.dot} />;

export const Banners = (props) => {
  const slides = [];
  props.banners.forEach((banner) =>
    slides.push(
      <View key={banner.id} style={styles.slide}>
        <Image
          source={{
            uri: banner.photo,
          }}
          resizeMode="contain"
          style={styles.slideImage}
        />
      </View>
    )
  );
  return (
    <View style={styles.container}>
      {props.banners.length > 0 && (
        <Swiper
          activeDot={<ActiveDot />} // autoplay
          dot={<Dot />}
          paginationStyle={{
            bottom: 0,
          }}
        >
          {slides}
        </Swiper>
      )}
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    height: 148,
    marginTop: 8,
    padding: 16,
  },
  slide: {
    alignItems: 'center',
  },
  slideImage: {
    height: 104,
    width: bannerWidth,
  },
  activeDot: {
    backgroundColor: '#FFCB05',
    borderRadius: 2,
    height: 4,
    marginHorizontal: 2,
    width: 12,
  },
  dot: {
    backgroundColor: '#D0D3DB',
    borderRadius: 2,
    height: 4,
    marginHorizontal: 2,
    width: 4,
  },
});
