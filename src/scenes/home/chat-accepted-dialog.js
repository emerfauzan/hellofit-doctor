import React, { useEffect, useState } from 'react';
import { Actions } from 'react-native-router-flux';
import { InfoDialog } from '../../components';
import { feathersClient } from '../../utils';

export const ChatAcceptedDialog = (props) => {
  const [chat, setChat] = useState();
  const [showDialog, setShowDialog] = useState(false);
  const [description, setDescription] = useState('');
  useEffect(() => {
    const listener = (chat) => {
      if (chat.status === 'accepted' && chat.diagnose == null) {
        setChat(chat);
        setDescription(`${chat.doctor.name} telah menerima permintaan Anda`);
        // setShowDialog(true);
      }
    };

    feathersClient.service('chats').on('patched', listener);
    return function cleanup() {
      feathersClient.service('chats').removeListener('patched', listener);
    };
  }, []);

  function _handleConfirm() {
    setShowDialog(false);

    if (Actions.currentScene === 'waiting') {
      Actions.replace('chat-detail', {
        chat,
      });
    } else {
      Actions.push('chat-detail', {
        chat,
      });
    }
  }

  return (
    <InfoDialog
      visible={showDialog}
      title="Permintaan Diterima"
      description={description}
      confirmLabel="Mulai Chat"
      onConfirm={() => _handleConfirm()}
      onRequestClose={() => setShowDialog(false)}
    />
  );
};
