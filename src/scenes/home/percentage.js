import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Dimensions } from 'react-native';
import {PieChart} from "react-native-chart-kit";

export const Percentage = (props) => (
  <View style={styles.container}>
    <View style={styles.percentageView}>
      <View>
        <View style={styles.pieDescView}>
          <View style={styles.pieDescView2}>
            <Text style={styles.pieDescText}>72%</Text>
          </View>
        </View>
        <PieChart
          data={[
            {
              population: 39,
              color: "#FFCB05",
            },
            {
              population: 8,
              color: "#EDF0F4",
            },
            {
              population: 7,
              color: "#EDF0F4",
            },
          ]}
          width={100}
          height={100}
          chartConfig={{
            backgroundGradientFrom: "#FFFFFF",
            // backgroundGradientFromOpacity: 1,
            backgroundGradientTo: "#FFFFFF",
            // backgroundGradientToOpacity: 1,
            color: () => '#FFCB05',
            strokeWidth: 2, // optional, default 3
            // barPercentage: 0.5,
            // useShadowColorFromDataset: false // optional
          }}
          accessor="population"
          backgroundColor="transparent"
          paddingLeft={25}
          hasLegend={false}
        />
      </View>
      <Text style={styles.text}>39 Pasien</Text>
    </View>
    <View style={styles.percentageView}>
      <View>
        <View style={styles.pieDescView}>
          <View style={styles.pieDescView2}>
            <Text style={styles.pieDescText}>15%</Text>
          </View>
        </View>
        <PieChart
          data={[
            {
              population: 8,
              color: "#2D99FF",
            },
            {
              population: 39,
              color: "#EDF0F4",
            },
            {
              population: 7,
              color: "#EDF0F4",
            },
          ]}
          width={100}
          height={100}
          chartConfig={{
            backgroundGradientFrom: "#FFFFFF",
            // backgroundGradientFromOpacity: 1,
            backgroundGradientTo: "#FFFFFF",
            // backgroundGradientToOpacity: 1,
            color: () => '#FFCB05',
            strokeWidth: 2, // optional, default 3
            // barPercentage: 0.5,
            // useShadowColorFromDataset: false // optional
          }}
          accessor="population"
          backgroundColor="transparent"
          paddingLeft={25}
          hasLegend={false}
        />
      </View>
      <Text style={styles.text}>8 Pasien</Text>
    </View>
    <View style={styles.percentageView}>
      <View>
        <View style={styles.pieDescView}>
          <View style={styles.pieDescView2}>
            <Text style={styles.pieDescText}>13%</Text>
          </View>
        </View>
        <PieChart
          data={[
            {
              population: 7,
              color: "#FF7233",
            },
            {
              population: 8,
              color: "#EDF0F4",
            },
            {
              population: 39,
              color: "#EDF0F4",
            },
          ]}
          width={100}
          height={100}
          chartConfig={{
            backgroundGradientFrom: "#FFFFFF",
            // backgroundGradientFromOpacity: 1,
            backgroundGradientTo: "#FFFFFF",
            // backgroundGradientToOpacity: 1,
            color: () => '#FFCB05',
            strokeWidth: 2, // optional, default 3
            // barPercentage: 0.5,
            // useShadowColorFromDataset: false // optional
          }}
          accessor="population"
          backgroundColor="transparent"
          paddingLeft={25}
          hasLegend={false}
        />
      </View>
      <Text style={styles.text}>7 Pasien</Text>
    </View>
  </View>
);
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 16,
    paddingHorizontal: 20,
    // paddingTop: 20,
  },
  percentageView: {
    marginHorizontal: '2%',
  },
  percentage: {
    backgroundColor: 'red',
    width: 84,
    height: 84,
    borderRadius: 84/2,
  },
  text: {
    color: '#505D6F',
    fontFamily: 'Quicksand-Medium',
    textAlign: 'center',
    fontSize: 12,
    lineHeight: 20,
  },
  pieDescView: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 1,
  },
  pieDescView2: {
    width: 65,
    height: 65,
    borderRadius: 65/2,
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 1,
    backgroundColor: 'white',
  },
  pieDescText: {
    color: '#505D6F',
    fontFamily: 'Quicksand-Bold',
    fontSize: 16,
    lineHeight: 24,
  },
});

