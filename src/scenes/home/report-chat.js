import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Dimensions, Platform } from 'react-native';
import {PieChart} from "react-native-chart-kit";

const screenWidth = Dimensions.get("window").width;

export const ReportChat = (props) => (
  <View style={styles.container}>
    <View style={styles.left}>
      <Text style={styles.text1}>Report Chat</Text>
      <Text style={styles.text2}>Hasil dari system</Text>
      <View style={styles.dotView}>
        <View style={[styles.dot, {backgroundColor: '#FFCB05'}]} />
        <Text style={styles.text3}>Acceptance</Text>
      </View>
      <View style={styles.dotView}>
        <View style={[styles.dot, {backgroundColor: '#2D99FF'}]} />
        <Text style={styles.text3}>Reject By Me</Text>
      </View>
      <View style={styles.dotView}>
        <View style={[styles.dot, {backgroundColor: '#FF7233'}]} />
        <Text style={styles.text3}>Reject By System</Text>
      </View>
    </View>
    <View>
      <View style={styles.pieDescView}>
        <View style={styles.pieDescView2}>
          <Text style={styles.pieDescText1}>Total</Text>
          <Text style={styles.pieDescText2}>56</Text>
        </View>
      </View>
      <PieChart
        data={[
          {
            name: "Acceptance",
            population: 39,
            color: "#FFCB05",
          },
          {
            name: "Reject By Me",
            population: 8,
            color: "#2D99FF",
          },
          {
            name: "Reject By System",
            population: 7,
            color: "#FF7233",
          }
        ]}
        width={200}
        height={200}
        chartConfig={{
          backgroundGradientFrom: "#FFFFFF",
          // backgroundGradientFromOpacity: 1,
          backgroundGradientTo: "#FFFFFF",
          // backgroundGradientToOpacity: 1,
          color: () => '#FFCB05',
          strokeWidth: 2, // optional, default 3
          // barPercentage: 0.5,
          // useShadowColorFromDataset: false // optional
        }}
        accessor="population"
        backgroundColor="transparent"
        hasLegend={false}
        paddingLeft={50}
      />
    </View>
  </View>
);
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    // justifyContent: 'center',
    // alignItems: 'center',
    paddingHorizontal: 20,
    paddingTop: 10,
  },
  left: {
    width: '40%',
  },
  dotView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 12,
  },
  dot: {
    height: 10,
    width: 10,
    borderRadius: 10/2,
    marginRight: 8,
  },
  right: {
    width: '55%',
  },
  text1: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
    fontSize: 16,
    lineHeight: 20,
    marginBottom: 3,
  },
  text2: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
    lineHeight: 20,
    marginBottom: 20,
  },
  text3: {
    color: '#505D6F',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
    lineHeight: 20,
  },
  pieDescView: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 1,
  },
  pieDescView2: {
    width: 120,
    height: 120,
    borderRadius: 120/2,
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 1,
    backgroundColor: 'white',
  },
  pieDescText1: {
    color: '#9AA1A9',
    fontFamily: 'Quicksand-Medium',
    fontSize: 14,
    lineHeight: 24,
    marginBottom: 2,
  },
  pieDescText2: {
    color: '#505D6F',
    fontFamily: 'Quicksand-Bold',
    fontSize: 24,
    lineHeight: 30,
  },
});