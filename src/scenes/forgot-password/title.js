import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { HeaderLayout } from '../../components';

export const Title = (props) => (
  <HeaderLayout transparent>
    <View style={styles.container}>
      <Text style={styles.label}>Lupa Kata Sandi</Text>
    </View>
  </HeaderLayout>
);
const styles = StyleSheet.create({
  container: {
    height: 44,
    margin: 16,
  },
  label: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
    fontSize: 24,
  },
  line: {
    backgroundColor: '#FFCB05',
    borderRadius: 2,
    height: 4,
    marginHorizontal: 1,
    marginVertical: 12,
    width: 36,
  },
});
