import React from 'react';
import { StyleSheet, Text, View, Modal } from 'react-native';
import { ButtonFooter } from "../../components"

export const SentInformation = (props) => (
    <Modal
        visible={props.visible}
        transparent
    >
        <View style={styles.container}>
            <View style={styles.box}>
                <Text style={styles.title}>Verifikasi Terkirim</Text>
                <Text style={styles.message}>Segera cek email untuk melanjutkan ke step berikutnya.</Text>
                <View style={styles.button}>
                    <ButtonFooter
                        label="Tutup"
                        onPress={() => props.onKunjungi()}
                    />
                </View>

            </View>
        </View>
    </Modal>
);
const styles = StyleSheet.create({
    container: {
        height: '100%',
        width: '100%',
        backgroundColor: 'rgba(55, 63, 80, 0.6)',
        paddingHorizontal: 10,
        justifyContent: 'center'
    },
    box: {
        backgroundColor: "#FFFFFF",
        width: "100%",
        paddingVertical: 20,
        alignItems: 'center'
    },
    title: {
        color: '#373F50',
        fontFamily: 'Quicksand-Bold',
        marginBottom: 10,
        fontSize: 18
    },
    message: {
        color: '#373F50',
        fontFamily: 'Quicksand-Bold',
        marginBottom: 10,
        paddingVertical: 15,
        textAlign: 'center'
    },
    button: {
        width: 170
    }
});
