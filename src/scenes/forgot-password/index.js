import React, { useState } from 'react';
import { StyleSheet, ScrollView, Text, Linking, NativeModules } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { useObserver } from 'mobx-react-lite';
import Toast from 'react-native-simple-toast';
import { openInbox } from 'react-native-email-link'
import {
    ButtonFooter,
    Footer,
    Form,
    Page,
    TextButton,
    TextInputField,
} from '../../components';
import { forgotPasswordStore } from '../../stores';
import { Title } from './title';
import { SentInformation } from './sent-information'

export const ForgotPassword = (props) => {
    const [sent, setSent] = useState(false);
    const [loading, setLoading] = useState(false);

    async function _submit() {
        if(!forgotPasswordStore.emailValid){
          return Toast.show('Format E-mail salah')
        }
        setLoading(true);

        try {
            await forgotPasswordStore.submit();
            setSent(true);
            setLoading(false);
        } catch (error) {
            setLoading(false);
            setTimeout(() => {
                Toast.show(error.message);
            }, 100);
        }
    }

    function _openMailApp() {
        setSent(false)
        Actions.pop()
        return;
    }

    return useObserver(() => (
        <Page style={styles.container} loading={loading}>
            <ScrollView>
                <Title />
                <SentInformation visible={sent} onKunjungi={() => _openMailApp()} />
                <Form>
                    <Text style={styles.notes}>Verifikasi akan dikirim ke email dibawah ini.</Text>
                    <TextInputField
                        autoCapitalize="none"
                        autoCompleteType="off"
                        autoCorrect={false}
                        keyboardType="email-address"
                        label="Email"
                        placeholder="Masukkan email anda"
                        value={forgotPasswordStore.accountForgotForm.email}
                        onChangeText={(text) => {
                            forgotPasswordStore.accountForgotForm.email = text;
                        }}
                    />
                </Form>
                <ButtonFooter
                    disabled={!forgotPasswordStore.accountForgotFormValid}
                    label="Kirim"
                    onPress={() => _submit()}
                />
            </ScrollView>
            <Footer style={styles.footer}>
                <TextButton
                    label1="Ingat kata sandi ? "
                    label2="Masuk"
                    onPress={() => Actions.pop()}
                />
            </Footer>
        </Page>
    ));
};
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFF',
    },
    footer: {
        paddingBottom: 32,
    },
    notes: {
        color: '#373F50',
        fontFamily: 'Quicksand-Bold',
        marginBottom: 10
    }
});
