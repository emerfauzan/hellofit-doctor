import React, { useEffect } from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { useObserver } from 'mobx-react-lite';
import { Header, Page } from '../../components';
import { feathersClient } from '../../utils';
import { dashboardStore, ratingStore } from '../../stores';
import { RatingItem } from './rating-item';
import { Summary } from './summary';

export const RatingList = (props) => {
  useEffect(() => {
    async function run() {
      const { user } = await feathersClient.get('authentication');
      await dashboardStore.get(user.id);
      await ratingStore.find({
        query: {
          $select: ['comment'],
          $sort: {
            id: -1,
          },
          $and: [
            {
              comment: {
                $ne: null,
              },
            },
            {
              comment: {
                $ne: '',
              },
            },
          ],
        },
      });
    }

    run();
  }, []);
  return useObserver(() => (
    <Page>
      <Header title="Rating & Feedback" onBack={() => Actions.pop()} />
      {dashboardStore.current && (
        <Summary
          rating={dashboardStore.current.rating}
          feedbacks={ratingStore.total}
          likes={dashboardStore.current.likes}
          dislikes={dashboardStore.current.dislikes}
        />
      )}
      <ScrollView contentContainerStyle={styles.container}>
        {ratingStore.data.map((chat) => (
          <RatingItem key={chat.id} comment={chat.comment} />
        ))}
      </ScrollView>
    </Page>
  ));
};
const styles = StyleSheet.create({
  container: {
    padding: 16,
  },
});
