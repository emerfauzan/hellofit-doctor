import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export const RatingItem = (props) => (
  <View style={styles.container}>
    <Text style={styles.text}>{props.comment}</Text>
  </View>
);
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    borderRadius: 4,
    height: 48,
    justifyContent: 'center',
    marginBottom: 12,
    padding: 8,
  },
  text: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
  },
});
