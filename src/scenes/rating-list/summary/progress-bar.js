import React, { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';

export const ProgressBar = (props) => {
  const [innerWidth, setInnerWidth] = useState(0);

  function _handleLayout(event) {
    let width = 0;

    if (props.max > 0) {
      width = (props.value / props.max) * event.nativeEvent.layout.width;
    }

    setInnerWidth(width);
  }

  return (
    <View style={styles.container}>
      {props.icon}
      <View style={styles.bar} onLayout={(event) => _handleLayout(event)}>
        <View
          style={[
            styles.barInner,
            {
              width: innerWidth,
            },
          ]}
        />
      </View>
      <View style={styles.value}>
        <Text style={styles.valueText}>{props.value}</Text>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flexDirection: 'row',
    margin: 4,
  },
  bar: {
    backgroundColor: '#EBECF0',
    borderRadius: 2,
    flex: 1,
    height: 4,
    marginLeft: 8,
  },
  barInner: {
    backgroundColor: '#FFCB05',
    borderRadius: 2,
    height: 4,
  },
  value: {
    width: 40,
  },
  valueText: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    marginLeft: 8,
  },
});
