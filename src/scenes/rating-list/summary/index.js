import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { ProgressBar } from './progress-bar';
import ThumbGreenIcon from './thumb-green.svg';
import ThumbRedIcon from './thumb-red.svg';
import ThumbYellowIcon from './thumb-yellow.svg';

export const Summary = (props) => {
  const total = props.likes + props.dislikes;
  return (
    <View style={styles.container}>
      <View style={styles.left}>
        <View style={styles.percentage}>
          <ThumbYellowIcon />
          <Text style={styles.percentageValue}>{props.rating}%</Text>
        </View>
        <Text style={styles.feedback}>{props.feedbacks} Feedback</Text>
      </View>
      <View style={styles.right}>
        <ProgressBar
          icon={<ThumbGreenIcon />}
          max={total}
          value={props.likes}
        />
        <ProgressBar
          icon={<ThumbRedIcon />}
          max={total}
          value={props.dislikes}
        />
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  left: {
    backgroundColor: '#FFF',
    padding: 16,
  },
  right: {
    backgroundColor: '#FFF',
    flex: 1,
    justifyContent: 'center',
    marginLeft: 1,
    padding: 16,
  },
  percentage: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  percentageValue: {
    color: '#FFCB05',
    fontFamily: 'Quicksand-Bold',
    fontSize: 24,
    marginLeft: 12,
  },
  feedback: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
  },
});
