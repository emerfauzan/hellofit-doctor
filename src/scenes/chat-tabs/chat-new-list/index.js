import React, { useEffect, useState } from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { useObserver } from 'mobx-react-lite';
import { InfoDialog } from '../../../components';
import { useForceUpdate } from '../../../hooks';
import { chatNewStore, chatProgressStore } from '../../../stores';
import { ChatNewItem } from './chat-new-item';
import ReactNativeAN from 'react-native-alarm-notification';

export const ChatNewList = (props) => {
  const [rejectedChatId, setRejectedChatId] = useState(0);
  const [acceptedChatId, setAcceptedChatId] = useState(0);
  const [limitChatId, setLimitChatId] = useState(0);
  const [listLength, setListLength] = useState(0);
  useForceUpdate();
  useEffect(() => {
    chatNewStore.find({
      query: {
        status: 'requested',
      },
    });
  }, []);

  async function _handleAccept() {
    setAcceptedChatId(0);
    const patchedChat = await chatNewStore.patch(acceptedChatId, {
      status: 'accepted',
    });
    Actions.push('chat-detail', {
      chat: patchedChat,
    });
  }

  async function _handleReject() {
    await chatNewStore.patch(rejectedChatId, {
      status: 'rejected',
    });
    setRejectedChatId(0);
  }

  async function _handleLimit() {
    setLimitChatId(0);
    setAcceptedChatId(limitChatId);
  }

  function _checkLimit(chatId) {
    if (chatProgressStore.total >= 5) {
      setLimitChatId(chatId);
    } else {
      setAcceptedChatId(chatId);
    }
  }

  return useObserver(() => (
    <>
      <ScrollView contentContainerStyle={styles.container}>
        { chatNewStore.data.length !== listLength && props.onButtonPress(0) && setListLength(chatNewStore.data.length) }
        {chatNewStore.data.map((chat) => (
          <ChatNewItem
            key={chat.id}
            chat={chat}
            onAccept={() => _checkLimit(chat.id)}
            onReject={() => setRejectedChatId(chat.id)}
          />
        ))}
      </ScrollView>
      <InfoDialog
        visible={rejectedChatId !== 0}
        title="Tolak Permintaan Chat"
        description="Menolak permintaan chat akan memengaruhi rating Anda. Apakah Anda yakin ingin menolak permintaan chat?"
        onCancel={() => {
          setRejectedChatId(0)
          props.onButtonPress(0)
        }}
        onConfirm={() => {
          _handleReject()
          props.onButtonPress(0)
          ReactNativeAN.stopAlarmSound();
        }}
      />
      <InfoDialog
        visible={acceptedChatId !== 0}
        title="Disclaimer"
        description="Pihak Hellofit tidak bertanggung jawab atas segala kerugian ataupun kerusakan pada pihak tenaga medis maupun pengguna aplikasi yang terjadi akibat hasil konsultasi. Pihak tenaga medis mengetahui, menyadari, dan menyetujui bahwa konsultasi dengan aplikasi e-healthcare bukanlah pengganti pertemuan secara langsung dan pemeriksaan fisik."
        onCancel={() => {
          setAcceptedChatId(0)
          props.onButtonPress(0)
        }}
        onConfirm={() => {
          _handleAccept()
          props.onButtonPress(1)
          ReactNativeAN.stopAlarmSound();
        }}
      />
      <InfoDialog
        visible={limitChatId !== 0}
        title="Masih Ada Chat Berjalan"
        description="Anda masih memiliki 5 chat yang sedang berjalan. Apakah Anda yakin untuk menerima permintaan chat ini?"
        onCancel={() => setLimitChatId(0)}
        onConfirm={() => {
          _handleLimit()
          ReactNativeAN.stopAlarmSound();
        }}
      />
    </>
  ));
};
const styles = StyleSheet.create({
  container: {
    padding: 8,
  },
});
