import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import moment from 'moment';
import { Timer } from '../../../components';
import { waitingSeconds } from '../../../utils';

export const ChatNewItem = (props) => {
  const age = moment().diff(moment(props.chat.patient.birthdate), 'years');
  const { height, weight } = props.chat.patient;
  return (
    <View style={styles.container}>
      <View style={styles.data}>
        <View style={styles.row}>
          <Text style={styles.name}>{props.chat.patient.name}</Text>
          <Timer
            initial={waitingSeconds(props.chat.createdAt)}
            max={300}
            onEnd={props.onReject}
          />
        </View>
        <Text style={styles.description}>
          {age} Tahun ･ {height} cm ･ {weight} kg
        </Text>
      </View>
      <View style={styles.buttons}>
        <TouchableOpacity style={styles.leftButton} onPress={props.onReject}>
          <Text style={styles.label}>Tolak</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.rightButton} onPress={props.onAccept}>
          <Text style={styles.label}>Terima</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 8,
  },
  data: {
    backgroundColor: '#FFF',
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    padding: 12,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  name: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
    fontSize: 16,
  },
  description: {
    color: '#9CA2B4',
    fontFamily: 'Quicksand-Regular',
  },
  buttons: {
    flexDirection: 'row',
    height: 32,
  },
  leftButton: {
    alignItems: 'center',
    backgroundColor: '#DE5261',
    borderBottomLeftRadius: 4,
    flex: 1,
    justifyContent: 'center',
  },
  rightButton: {
    alignItems: 'center',
    backgroundColor: '#3BD15E',
    borderBottomRightRadius: 4,
    flex: 1,
    justifyContent: 'center',
  },
  label: {
    color: '#FFF',
    fontFamily: 'Quicksand-SemiBold',
    fontSize: 12,
    lineHeight: 14,
  },
});
