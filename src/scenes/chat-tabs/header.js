import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { HeaderLayout } from '../../components';
import HistoryIcon from './history.svg';

export const Header = (props) => (
  <HeaderLayout>
    <View style={styles.container}>
      <Text style={styles.title}>Chat</Text>
      <TouchableOpacity style={styles.textButton} onPress={props.onRight}>
        <Text style={styles.textButtonLabel}>Riwayat</Text>
        <HistoryIcon />
      </TouchableOpacity>
    </View>
  </HeaderLayout>
);
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#FFCB05',
    flexDirection: 'row',
    height: 56,
    justifyContent: 'space-between',
    paddingLeft: 16,
  },
  title: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
    fontSize: 16,
    lineHeight: 19,
  },
  textButton: {
    flexDirection: 'row',
    padding: 16,
  },
  textButtonLabel: {
    color: '#373F50',
    fontFamily: 'Quicksand-SemiBold',
    lineHeight: 17,
    marginRight: 4,
  },
});
