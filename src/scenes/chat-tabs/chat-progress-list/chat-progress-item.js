import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import moment from 'moment';
import { Timer } from '../../../components';
import { chatSeconds } from '../../../utils';

export const ChatProgressItem = (props) => {
  const age = moment().diff(moment(props.chat.patient.birthdate), 'years');
  const { height, weight } = props.chat.patient;
  return (
    <TouchableOpacity onPress={props.onPress}>
      <View style={styles.top}>
        <Text style={styles.title}>Masih Berlangsung</Text>
      </View>
      <View style={styles.bottom}>
        <View>
          <View style={styles.row}>
            <Text style={styles.name}>{props.chat.patient.name}</Text>
            {props.mark && (
              <View style={styles.mark}>
                <Text style={styles.markLabel}>!</Text>
              </View>
            )}
          </View>
          <Text style={styles.description}>
            {age} Tahun ･ {height} cm ･ {weight} kg
          </Text>
        </View>
        <View>
          {props.chat.acceptedAt && (
            <Timer
              initial={chatSeconds(props.chat.acceptedAt)}
              max={1800}
              onEnd={() => {}}
            />
          )}
        </View>
      </View>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  top: {
    alignItems: 'center',
    backgroundColor: '#FFEEAD',
    height: 24,
    justifyContent: 'center',
  },
  bottom: {
    backgroundColor: '#FFFFFF',
    borderRadius: 4,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 12,
    paddingHorizontal: 16,
    paddingVertical: 12,
  },
  title: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
    lineHeight: 14,
  },
  row: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  name: {
    color: '#373F50',
    fontFamily: 'Quicksand-SemiBold',
    fontSize: 16,
  },
  mark: {
    alignItems: 'center',
    backgroundColor: '#F34759',
    borderRadius: 8,
    height: 16,
    justifyContent: 'center',
    marginLeft: 8,
    width: 16,
  },
  markLabel: {
    color: '#FFF',
  },
  description: {
    color: '#9CA2B4',
    fontFamily: 'Quicksand-Regular',
  },
});
