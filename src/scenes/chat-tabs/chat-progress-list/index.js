import React, { useEffect } from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { useObserver } from 'mobx-react-lite';
import { useForceUpdate } from '../../../hooks';
import { chatMessageStore, chatProgressStore } from '../../../stores';
import { ChatProgressItem } from './chat-progress-item';
import {toJS} from 'mobx'

export const ChatProgressList = (props) => {
  useForceUpdate();
  useEffect(() => {
    chatProgressStore.find({
      query: {
        status: 'accepted',
      },
    });
  }, []);
  return useObserver(() => (
    <ScrollView contentContainerStyle={styles.container}>
      {chatProgressStore.data.map((chat) => {
        return (
        <ChatProgressItem
          key={chat.id}
          mark={chatMessageStore.markIds.has(chat.id)}
          chat={chat}
          onPress={() => {
            props.onRender(1)
            chatMessageStore.markIds.delete(chat.id);
            chatMessageStore.deleteBadge(chat.id)
            Actions.push('chat-detail', {
              chat,
            });
          }}
        />
      )})}
    </ScrollView>
  ));
};
const styles = StyleSheet.create({
  container: {
    padding: 16,
  },
});
