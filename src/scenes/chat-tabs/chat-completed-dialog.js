import React, { useEffect, useState } from 'react';
import { Actions } from 'react-native-router-flux';
import { InfoDialog } from '../../components';
import { feathersClient } from '../../utils';
import { chatMessageStore } from '../../stores';

export const ChatCompletedDialog = (props) => {
  const [chat, setChat] = useState();
  const [showDialog, setShowDialog] = useState(false);
  const [description, setDescription] = useState('');
  const [noteExist, setNoteExist] = useState(false)
  useEffect(() => {
    feathersClient.service('chats').on('patched', (chat) => {
      if (chat.isNoteFilled == false) {
        setNoteExist(false)
      } else {
        setNoteExist(true)
      }
      if (
        chat.comment == null &&
        chat.status === 'completed' &&
        Actions.currentScene !== 'chat-detail' &&
        Actions.currentScene !== 'doctor-note'
      ) {
        setChat(chat);
        setDescription(`Pasien ${chat.patient.name} telah mengakhiri sesi`);
        setShowDialog(true);
      }
    });
  }, []);

  function _handleConfirm() {
    setShowDialog(false);
    if (noteExist) {
      Actions.pop()
    } else {
      Actions.push('doctor-note', {
        isNew: true,
        isEnd: true,
        chat,
      });
    }

    // Actions.push('rating', {
    //   chat,
    // });
    chatMessageStore.markIds.delete(chat.id);
    chatMessageStore.deleteBadge(chat.id)
  }

  return (
    <InfoDialog
      visible={showDialog}
      title="Sesi Telah Berakhir"
      description={description}
      confirmLabel="Oke"
      onConfirm={() => _handleConfirm()}
    />
  );
};
