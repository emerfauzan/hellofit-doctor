import React, { useEffect, useState } from 'react';
import { AppState, Dimensions, StyleSheet, Text, DeviceEventEmitter } from 'react-native';
import { TabBar, TabView, SceneMap } from 'react-native-tab-view';
import { Actions } from 'react-native-router-flux';
import messaging from '@react-native-firebase/messaging';
import PushNotification from 'react-native-push-notification';
import { Page } from '../../components';
import { chatNewStore, chatProgressStore, profileStore, chatMessageStore } from '../../stores';
import { feathersClient } from '../../utils';
import { Header } from './header';
import { ChatCompletedDialog } from './chat-completed-dialog';
import { ChatNewList } from './chat-new-list';
import { ChatProgressList } from './chat-progress-list';

export const ChatTabs = (props) => {
  const [navigationState, setNavigationState] = useState({
    index: 0,
    routes: [
      {
        key: 'new',
        title: 'Baru',
        badge: chatNewStore.data.length,
      },
      {
        key: 'progress',
        title: 'Sedang Berjalan',
        // badge: chatMessageStore.markIds._data.size,
        badge: chatMessageStore.badgeStatus(),
      },
    ],
  });
  useEffect(() => {
    const unsubsribe = messaging().onMessage(async remoteMessage => {
      if(remoteMessage.data.type === "chat"){
        badgeSet(1)
      }else{
        badgeSet(0)
      }
    })
    return unsubsribe;
  }, [])
  async function badgeSet(index){
    setTimeout(async() => {
      const baru = await chatNewStore.find({
        query:{
          status: 'requested',
        }
      })
      const progress = chatMessageStore.markIds._data.size
      setNavigationState({
        index: index,
        routes: [
          {
            key: 'new',
            title: 'Baru',
            badge: baru.data.length,
          },
          {
            key: 'progress',
            title: 'Sedang Berjalan',
            // badge: progress,
            badge: chatMessageStore.badgeStatus(),
          },
        ],
      })
    }, 500)
  }
  useEffect(() => {
    feathersClient.service('chats').on('patched', (chat) => {
      if (
        chat.comment == null &&
        chat.status === 'completed'
      ) {
        badgeSet(0)
      }
    });
  }, []);
  
  useEffect(() => {
    if (profileStore.doctor?.online) {
      PushNotification.getDeliveredNotifications((notifs) => {
        const ids = notifs.map((notif) => notif.identifier);

        if (!ids.includes('1')) {
          DeviceEventEmitter.addListener('notificationActionReceived', function(action){
            const info = JSON.parse(action.dataJSON);
            if (info.action == 'Accept') {
              // Do work pertaining to Accept action here
            } else if (info.action == 'Reject') {
              // Do work pertaining to Reject action here
            }
            // Add all the required actions handlers
          });
        }
      });
    } else {
      PushNotification.clearLocalNotification(1);
    }
  }, []);
  useEffect(() => {
    async function run() {
      const { user } = await feathersClient.get('authentication');
      const fcmToken = await messaging().getToken();
      await feathersClient.service('users').patch(user.id, {
        fcmToken,
      });

      if (!messaging().isDeviceRegisteredForRemoteMessages) {
        await messaging().registerDeviceForRemoteMessages();
      }
      badgeSet(0)
    }

    run();
  }, []);
  useEffect(() => {
    AppState.addEventListener('change', (nextState) => {
      if (nextState === 'active') {
        chatNewStore.find({
          query: {
            status: 'requested',
          },
        });
        chatProgressStore.find({
          query: {
            status: 'accepted',
          },
        });
      }
    });
  }, []);
  
  return (
    <Page>
      <Header onRight={() => Actions.push('chat-history-list')} />
      <TabView
        navigationState={navigationState}
        renderTabBar={(props) => (
          <TabBar
            {...props}
            indicatorContainerStyle={styles.indicatorContainerStyle}
            indicatorStyle={styles.indicatorStyle}
            renderBadge={(scene) =>(
              scene.route.badge === 0
              ? null :
              <Text style={styles.badge}>{scene.route.badge}</Text>
            )}
            renderLabel={(scene) => (
              <Text
                style={[
                  styles.label,
                  {
                    color: scene.focused ? '#373F50' : '#9CA2B4',
                  },
                ]}
              >
                {scene.route.title}
              </Text>
            )}
          />
        )}
        renderScene={({ route }) => {
          switch (route.key) {
            case 'new':
              return <ChatNewList 
                onButtonPress={(index)=> badgeSet(index)}
              />;
            case 'progress':
              return <ChatProgressList onRender={()=> badgeSet(1)} />;
            default:
              return null;
          }
        }}
        // renderScene={SceneMap({
        //   new: ChatNewList,
        //   progress: ChatProgressList,
        // })}
        onIndexChange={(index) => {
          setNavigationState({ ...navigationState, index })
        }}
        initialLayout={{
          width: Dimensions.get('window').width,
        }}
      />
      <ChatCompletedDialog />
    </Page>
  );
};
const styles = StyleSheet.create({
  indicatorContainerStyle: {
    backgroundColor: '#FFF',
  },
  indicatorStyle: {
    backgroundColor: '#FFCB05',
    marginLeft: 30,
    width: Dimensions.get('window').width / 2 - 60,
  },
  label: {
    fontFamily: 'Quicksand-SemiBold',
  },
  badge: {
    backgroundColor: '#3BD15E',
    margin: 4,
    color: '#FFFFFF',
    textAlign: 'center',
    textAlignVertical: 'center',
    borderRadius: 15/2,
    height: 15,
    width: 15,
    fontSize: 10,
    lineHeight: 13,
    fontFamily: 'Quicksand-Medium',
  },
});
