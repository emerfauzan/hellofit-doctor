import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { DayInput } from './day-input';

export const DurationInputField = (props) => {
  const { label, ...others } = props;
  return (
    <View style={styles.container}>
      <Text style={styles.label}>{props.label}</Text>
      <View style={styles.space} />
      <DayInput {...others} />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    marginTop: 16,
    paddingLeft: 16,
  },
  label: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
  },
  space: {
    height: 8,
  },
});
