import React from 'react';
import { StyleSheet, Text, TextInput, View } from 'react-native';

export const DayInput = (props) => {
  const { unit, ...others } = props;
  return (
    <View style={styles.container}>
      <TextInput
        keyboardType="number-pad"
        placeholderTextColor="#9CA2B4"
        selectionColor="#FFCB05"
        style={styles.textInput}
        {...others}
      />
      <View style={styles.unit}>
        <Text style={styles.unitLabel}>{unit ? unit : 'hari'}</Text>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  textInput: {
    borderColor: '#EFF0F3',
    borderBottomLeftRadius: 4,
    borderTopLeftRadius: 4,
    borderWidth: 1,
    fontFamily: 'Quicksand-Medium',
    fontSize: 14,
    height: 40,
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  unit: {
    backgroundColor: '#EFF0F3',
    borderBottomRightRadius: 4,
    borderTopRightRadius: 4,
    justifyContent: 'center',
    paddingHorizontal: 16,
  },
  unitLabel: {
    fontFamily: 'Quicksand-Medium',
    fontSize: 16,
    lineHeight: 19,
  },
});
