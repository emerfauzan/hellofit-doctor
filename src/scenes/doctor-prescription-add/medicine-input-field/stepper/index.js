import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import MinusIcon from './minus.svg';
import PlusIcon from './plus.svg';

export const Stepper = (props) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.button}
        onPress={() => props.onChangeValue(props.value - 1)}
      >
        <MinusIcon />
      </TouchableOpacity>
      <Text style={styles.number}>{props.value}</Text>
      <TouchableOpacity
        style={styles.button}
        onPress={() => props.onChangeValue(props.value + 1)}
      >
        <PlusIcon />
      </TouchableOpacity>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    borderColor: '#FFCB05',
    borderRadius: 16,
    borderWidth: 1,
    flexDirection: 'row',
    height: 32,
    justifyContent: 'space-between',
    paddingHorizontal: 8,
    width: 98,
  },
  button: {
    alignItems: 'center',
    height: 16,
    justifyContent: 'center',
    width: 16,
  },
  number: {
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
    lineHeight: 14,
  },
});
