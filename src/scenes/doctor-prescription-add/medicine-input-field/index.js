import React, { useState } from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Stepper } from './stepper';
import medicineImage from './medicine.png';
import ZoomIcon from './zoom.svg';

export const MedicineInputField = (props) => {
  const [quantity, setQuantity] = useState(1);
  return (
    <View style={styles.container}>
      <View style={styles.top}>
        <Text style={styles.fieldName}>Jenis Obat</Text>
        {props.medicine != null && (
          <TouchableOpacity style={styles.button} onPress={props.onChange}>
            <Text style={styles.buttonLabel}>Ganti Obat</Text>
          </TouchableOpacity>
        )}
      </View>
      <View style={styles.bottom}>
        {props.medicine ? (
          <>
            <View style={styles.bottomLeft}>
              <Image source={medicineImage} style={styles.image} />
            </View>
            <View style={styles.bottomMiddle}>
              <Text style={styles.name}>Paratusin 10 Tablet</Text>
              <Text style={styles.price}>Rp 11.400</Text>
            </View>
            <View style={styles.bottomRight}>
              <Stepper
                value={quantity}
                onChangeValue={(value) => setQuantity(value)}
              />
            </View>
          </>
        ) : (
          <TouchableOpacity
            style={styles.searchButton}
            onPress={props.onChange}
          >
            <ZoomIcon />
            <Text style={styles.searchButtonLabel}>Cari Obat</Text>
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    padding: 16,
  },
  top: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  bottom: {
    flexDirection: 'row',
    paddingTop: 8,
  },
  bottomLeft: {},
  bottomMiddle: {
    flex: 1,
    paddingLeft: 12,
  },
  bottomRight: {},
  fieldName: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
  },
  button: {
    paddingHorizontal: 4,
    paddingLeft: 4,
  },
  buttonLabel: {
    color: '#FFCB05',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
  },
  image: {
    height: 40,
    width: 40,
  },
  name: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
  },
  price: {
    color: '#FFCB05',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
  },
  searchButton: {
    alignItems: 'center',
    backgroundColor: '#EFF0F399',
    borderColor: '#C9CDD6',
    borderRadius: 4,
    borderStyle: 'dashed',
    borderWidth: 1,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    height: 40,
  },
  searchButtonLabel: {
    color: '#C9CDD6',
    fontFamily: 'Quicksand-Medium',
    lineHeight: 16,
    marginLeft: 4,
  },
});
