import React from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Button, Header, Page, TextInputField } from '../../components';
import { DosageInputField } from './dosage-input-field';
import { DurationInputField } from './duration-input-field';
import { MedicineInputField } from './medicine-input-field';

export const DoctorPrescriptionAdd = (props) => (
  <Page style={styles.container}>
    <Header title="Obat" onBack={() => Actions.pop()} />
    <ScrollView>
      <MedicineInputField onChange={() => Actions.push('medicine-list')} />
      <DosageInputField label="Aturan Pakai" value="3 x 1" />
      <DurationInputField label="Lama Pemakaian" value="7" />
      <TextInputField
        label="Catatan"
        multiline
        numberOfLines={5}
        style={{
          textAlignVertical: 'top',
        }}
      />
    </ScrollView>
    <View style={styles.footer}>
      <Button label="Tambahkan Obat" onPress={() => Actions.pop()} />
    </View>
  </Page>
);
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
  },
  footer: {
    backgroundColor: '#FFF',
    borderColor: '#EBECF0',
    borderTopWidth: 1,
  },
});
