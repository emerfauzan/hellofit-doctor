import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { SelectInput } from '../../components';
import { DayInput } from './day-input';

export const DosageInputField = (props) => {
  const { label, unit, ...others } = props;
  return (
    <View style={styles.container}>
      <Text style={styles.label}>{label}</Text>
      <View style={styles.row}>
        <DayInput {...others} />
        <View style={styles.space} />
        <SelectInput
          items={[
            {
              id: 1,
              name: 'Sebelum Makan',
            },
            {
              id: 2,
              name: 'Sesudah Makan',
            },
          ]}
          value={{
            id: 1,
            name: 'Sebelum Makan',
          }}
          onChangeValue={(item) => {}}
        />
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    marginHorizontal: 16,
  },
  label: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
  },
  row: {
    flexDirection: 'row',
    marginTop: 8,
  },
  space: {
    width: 8,
  },
});
