import React, { useState, useEffect } from 'react';
import { Dimensions, Image, ScrollView, StyleSheet, View, CheckBox, Text, ToastAndroid } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Toast from 'react-native-simple-toast';
import {
  Button,
  Footer,
  Page,
  TextButton,
  TextInputField,
} from '../../components';
import { feathersClient, Storage } from '../../utils';
import { result } from 'lodash';

const screenWidth = Dimensions.get('window').width;
export const Login = (props) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const [rememberMe, setRemember] = useState(false);
  const [finishLoad, setFinishLoad] = useState(false);
  const [showPassword, setShowPassword] = useState(true);

  function showPasswordFunc() {
    setShowPassword(!showPassword)
  }

  useEffect(() => {
    Storage.getLoginData().then((loginData) => {
      if (loginData !== null && !finishLoad) {
        setEmail(loginData.email);
        setPassword(loginData.password)
        if (!finishLoad) {
          setRemember(true)
        }
        setFinishLoad(true)
      }
    })
  })

  async function _login() {
    setLoading(true);
    _checkRemember();

    try {
      await feathersClient.authenticate({
        strategy: 'local',
        email,
        password
      }).then((result) => {
        if(result.user.type === 'doctor'){
          Actions.reset('main');
        } else {
          ToastAndroid.show("Kombinasi email dan kata sandi masih salah", ToastAndroid.SHORT);
        }
      });
      setLoading(false);
    } catch (error) {
      setLoading(false);
      setTimeout(() => {
        Toast.show(error.message);
      }, 100);
    }
  }

  async function _checkRemember() {
    if (rememberMe) {
      await Storage.setLoginData({
        email,
        password
      })
    } else {
      Storage.resetLoginData()
    }
  }

  return (
    <Page loading={loading} style={styles.container}>
      <ScrollView keyboardShouldPersistTaps="handled">
        <Image
          source={require('./logo.png')}
          style={{
            height: 300,
            width: screenWidth,
          }}
        />
        <View style={styles.form}>
          <TextInputField
            autoCapitalize="none"
            autoCompleteType="off"
            autoCorrect={false}
            keyboardType="email-address"
            label="Email"
            placeholder="Masukkan email Anda"
            value={email}
            onChangeText={(text) => setEmail(text)}
          />
          <TextInputField
            autoCapitalize="none"
            label="Kata Sandi"
            placeholder="Masukkan kata sandi Anda"
            secureTextEntry={showPassword}
            icon={true}
            setShowPassword={()=> showPasswordFunc()}
            value={password}
            onChangeText={(text) => setPassword(text)}
          />
        </View>
      </ScrollView>
      <Footer style={styles.footer}>
        <TextButton
          label2="Lupa Kata Sandi"
          onPress={() => Actions.push('forgot-password')}
        />
        <View style={styles.beforeLoginContainer}>
          <View style={styles.checkboxContainer}>
            <CheckBox
              value={rememberMe}
              onValueChange={setRemember}
              style={styles.checkbox}
            />
            <Text style={styles.label}>Remember me</Text>
          </View>
        </View>
        <Button style={styles.loginButton} label="Masuk" onPress={() => _login()} />
        <TextButton
          label1="Belum punya akun? "
          label2="Daftar"
          onPress={() => Actions.push('register')}
        />
      </Footer>
    </Page>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
  },
  form: {
    padding: 16,
  },
  footer: {
    paddingBottom: 32,
  },
  beforeLoginContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 13
  },
  loginButton: {
    marginTop: 0
  },
  checkboxContainer: {
    flexDirection: "row",
    marginBottom: 0,
  },
  checkbox: {
    alignSelf: "center",
  },
  label: {
    marginVertical: 8,
    color: '#373F50',
    fontFamily: 'Quicksand-SemiBold',
  },
});
