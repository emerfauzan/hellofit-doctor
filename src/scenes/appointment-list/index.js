import React, { useEffect } from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { useObserver } from 'mobx-react-lite';
import { Page } from '../../components';
import { appointmentStore } from '../../stores';
import { Header } from './header';

export const AppointmentList = (props) => {
  useEffect(() => {
    appointmentStore.find();
  }, []);
  return useObserver(() => (
    <Page>
      <Header onRight={() => Actions.push('appointment-history-list')} />
      <ScrollView contentContainerStyle={styles.container}>
        {/* {appointmentStore.data.map(appointment => (
         <AppointmentItem key={appointment.id} appointment={appointment} />
        ))} */}
      </ScrollView>
    </Page>
  ));
};
const styles = StyleSheet.create({
  container: {
    margin: 16,
  },
});
