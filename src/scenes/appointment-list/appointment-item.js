import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import moment from 'moment';
import { formatPatientDescription } from '../../utils';

export const AppointmentItem = (props) => {
  const date = moment(props.appointment.bookedAt).format('dddd, DD MMMM');
  const time = moment(props.appointment.bookedAt).format('HH:mm');
  const appointmentDate = `${date} Pukul ${time}`;
  return (
    <View style={styles.container}>
      <View style={styles.top}>
        <Text style={styles.time}>{appointmentDate}</Text>
      </View>
      <View style={styles.bottom}>
        <Text style={styles.name}>{props.appointment.patient.name}</Text>
        <Text style={styles.description}>
          {formatPatientDescription(props.appointment.patient)}
        </Text>
        <Text style={styles.hospital}>
          {props.appointment.hospitalSchedule.hospital.name}
        </Text>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    marginBottom: 16,
  },
  top: {
    alignItems: 'center',
    backgroundColor: '#FFEEAD',
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    height: 24,
    justifyContent: 'center',
  },
  time: {
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
    lineHeight: 14,
  },
  bottom: {
    backgroundColor: '#FFF',
    paddingHorizontal: 16,
    paddingVertical: 12,
  },
  name: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
  },
  description: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
  },
  hospital: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
  },
});
