import React, { useEffect, useState } from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { useObserver } from 'mobx-react-lite';
import Toast from 'react-native-simple-toast';
import {
  Button,
  Header,
  ImageInputField,
  Page,
  TextInputField,
  UploadField,
  InfoDialog,
} from '../../components';
import { educationStore, profileEditStore, profileStore, hospitalStore, hospitalScheduleStore } from '../../stores';
import { EducationInputField } from './education-input-field';
import { HospitalScheduleInputField } from './hospital-schedules-input-field';
import { DropDownField } from '../../components/drop-down-field';

export const ProfileEdit = (props) => {
  const [loading, setLoading] = useState(false);
  const [showAlert, setShowAlert] = useState(false);
  const [showGender, setShowGender] = useState(false);

  async function setData() {
    const result = await feathersClient.service('hospital-schedules').find({
      query: {
        doctorId: profileStore.doctor.id,
      },
    });
    hospitalScheduleStore.setData(result.data)
  }

  useEffect(() => {
    async function fetch() {
      await educationStore.find();
      await hospitalScheduleStore.find();
      profileEditStore.reset();
    }

    fetch();
  }, []);

  async function _submit() {
    if(!profileEditStore.birthdateValid){
      return Toast.show('Format Tanggal Lahir salah')
    }
    if(profileEditStore.form.idNo.length < 16){
      return setShowAlert(true)
    }
    setLoading(true);
    try {
      await profileEditStore.submit();
      await educationStore.submit();
      await hospitalScheduleStore.submit();
      hospitalScheduleStore.resetData();
      setLoading(false);
      Actions.pop();
    } catch (error) {
      console.log('onotrak error', error)
      setData()
      setLoading(false);
      setTimeout(() => {
        Toast.show(error.message);
      }, 100);
    }
  }

  return useObserver(() => (
    <Page style={styles.container} loading={loading}>
      <Header title="Ubah Profil" onBack={() => Actions.pop()} />
      <ScrollView contentContainerStyle={styles.content}>
        <ImageInputField
          name={profileEditStore.form.name}
          specialist={profileStore.doctor.specialty.name}
          value={profileEditStore.form.photo}
          onChangeValue={(value) => (profileEditStore.form.photo = value)}
        />
        <TextInputField
          label="Nama Lengkap"
          autoCompleteType="off"
          autoCorrect={false}
          value={profileEditStore.form.name}
          onChangeText={(text) => (profileEditStore.form.name = text)}
        />
        <DropDownField
          label="Jenis Kelamin"
          placeholder="Pilih jenis kelamin Anda"
          value={profileEditStore.form.gender}
          showGender={showGender}
          setShowGender={()=> setShowGender(true)}
          // disabled={true}
          onSelectMale={()=> {
            profileEditStore.form.gender = 'laki'
            setShowGender(false)
          }}
          onSelectFemale={()=> {
            profileEditStore.form.gender = 'perempuan'
            setShowGender(false)
          }}
        />
        <TextInputField
          keyboardType="number-pad"
          label="Tanggal Lahir"
          options={{
            mask: '99-99-9999',
          }}
          placeholder="dd-mm-yyyy"
          type="custom"
          value={profileEditStore.form.birthdate}
          onChangeText={(text) => {
            profileEditStore.form.birthdate = text;
          }}
        />
        <TextInputField
          disable={true}
          label="Email"
          value={profileEditStore.form.email}
        />
        <TextInputField
          label="Nomor Handphone"
          value={profileEditStore.form.phone}
        />
        <TextInputField
          disable={true}
          label="Kata Sandi"
          secureTextEntry={true}
          value={'12345678'}
        />
        <EducationInputField />
        <HospitalScheduleInputField />
        <TextInputField
          label="No. STR"
          keyboardType="number-pad"
          value={profileEditStore.form.regNo}
          onChangeText={(text) => (profileEditStore.form.regNo = text)}
        />
        <UploadField
          label="Upload STR"
          value={profileEditStore.form.regCard}
          onChangeValue={(value) => (profileEditStore.form.regCard = value)}
        />
        <TextInputField
          label="No. KTP"
          keyboardType="number-pad"
          value={profileEditStore.form.idNo}
          onChangeText={(text) => (profileEditStore.form.idNo = text)}
        />
        <UploadField
          label="Upload Kartu Identitas"
          value={profileEditStore.form.idCard}
          onChangeValue={(value) => (profileEditStore.form.idCard = value)}
        />
      </ScrollView>
      <InfoDialog
        visible={showAlert}
        title="Validasi No KTP"
        description="NIK (no KTP) harus terdiri dari 16 digit angka"
        confirmLabel="OK"
        onConfirm={() => {
          setShowAlert(false)
        }}
      />
      <Button label="Simpan" onPress={() => _submit()} />
    </Page>
  ));
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
  },
  content: {
    padding: 16,
  },
  buttonRow: {
    flexDirection: 'row',
  },
  buttonCol: {
    flex: 1,
  },
});
