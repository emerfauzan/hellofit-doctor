import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { useObserver } from 'mobx-react-lite';
import { educationStore } from '../../../stores';
import { EducationAddDialog } from './education-add-dialog';
import { EducationEditDialog } from './education-edit-dialog';
import EditIcon from './edit.svg';

export const EducationInputField = (props) => {
  const [showAddDialog, setShowAddDialog] = useState(false);
  const [showEditDialog, setShowEditDialog] = useState(false);
  const [education, setEducation] = useState();
  useEffect(() => {
    async function fetch() {
      await educationStore.find();
    }

    fetch();
  }, []);
  return useObserver(() => (
    <View style={styles.container}>
      <Text style={styles.label}>Pendidikan</Text>
      {educationStore.data.map((education) => (
        <View key={education.id} style={styles.educationItem}>
          <View style={styles.data}>
            <Text style={styles.dataLabel}>
              {education.degree.name}, {education.school.name},{' '}
              {education.graduate}
            </Text>
          </View>
          <TouchableOpacity
            style={styles.editButton}
            onPress={() => {
              setEducation(education);
              setShowEditDialog(true);
            }}
          >
            <EditIcon />
          </TouchableOpacity>
        </View>
      ))}
      <TouchableOpacity
        style={styles.textButton}
        onPress={() => setShowAddDialog(true)}
      >
        <Text style={styles.textButtonLabel}>+ Tambah Pendidikan</Text>
      </TouchableOpacity>
      <EducationAddDialog
        visible={showAddDialog}
        onAddEducation={(education) => {
          educationStore.add(education);
          setShowAddDialog(false);
        }}
        onRequestClose={() => setShowAddDialog(false)}
      />
      {education && (
        <EducationEditDialog
          education={education}
          educations={educationStore.data}
          visible={showEditDialog}
          onDeleteEducation={(education) => {
            educationStore.delete(education.id);
            setShowEditDialog(false);
          }}
          onEditEducation={(education) => {
            educationStore.edit(education);
            setShowEditDialog(false);
          }}
          onRequestClose={() => setShowEditDialog(false)}
        />
      )}
    </View>
  ));
};
const styles = StyleSheet.create({
  container: {
    marginBottom: 16,
  },
  label: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
    marginBottom: 8,
  },
  educationItem: {
    alignItems: 'center',
    borderColor: '#EFF0F3',
    borderRadius: 4,
    borderWidth: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 4,
    paddingLeft: 16,
  },
  data: {
    flex: 1,
  },
  dataLabel: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
  },
  editButton: {
    padding: 8,
    margin: 8,
  },
  textButton: {
    alignSelf: 'flex-end',
    padding: 8,
  },
  textButtonLabel: {
    color: '#FFCB05',
    fontFamily: 'Quicksand-Medium',
    lineHeight: 17,
  },
});
