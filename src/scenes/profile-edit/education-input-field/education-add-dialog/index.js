import React, { useEffect, useState } from 'react';
import { Modal, StyleSheet, View } from 'react-native';
import { useObserver } from 'mobx-react-lite';
import moment from 'moment';
import _ from 'lodash';
import { Button, SelectInputField } from '../../../../components';
import { degreeStore, schoolStore } from '../../../../stores';
import { isPaginated } from '../../../../utils';

const graduates = _.range(1980, moment().year()).map((year) => ({
  id: year,
  name: year.toString(),
}));

export const EducationAddDialog = (props) => {
  const [degree, setDegree] = useState();
  const [school, setSchool] = useState();
  const [graduate, setGraduate] = useState(graduates[0]);
  const [schoolKeyword, setSchoolKeyword] = useState('');
  const [schoolData, setSchoolData] = useState([]);

  function searchSchool(val){
    setSchoolKeyword(val);
    let data = schoolStore.data.filter((x)=>{
      if(val){
        return x.name.toLowerCase().search(val.toLowerCase()) !== -1
      }
    })
    if(val.length > 2){
      setSchoolData(data)
    }else{
      setSchoolData([])
    }
  }

  useEffect(() => {
    async function fetch() {
      const degreeResult = await degreeStore.find();

      if (isPaginated(degreeResult)) {
        setDegree(degreeResult.data[0]);
      }

      const schoolResult = await schoolStore.find();

      if (isPaginated(schoolResult)) {
        setSchool(schoolResult.data[0]);
      }
    }

    fetch();
  }, []);

  return useObserver(() => (
    <Modal transparent {...props}>
      <View style={styles.container}>
        {degree && school && graduate && (
          <View style={styles.dialog}>
            <SelectInputField
              label="Tingkat Pendidikan"
              items={degreeStore.data}
              value={degree}
              onChangeValue={(item) => setDegree(item)}
            />
            <SelectInputField
              label="Universitas"
              items={schoolData}
              value={school}
              onChangeValue={(item) => setSchool(item)}
              searchable
              searchPlaceholder="Cari Universitas"
              searchText={schoolKeyword}
              onChangeSearchText={(keyword) => searchSchool(keyword)}
            />
            <SelectInputField
              label="Tahun Lulus"
              items={graduates}
              value={graduate}
              onChangeValue={(value) => setGraduate(value)}
            />
            <Button
              label="Tambahkan"
              onPress={() => {
                props.onAddEducation({
                  degreeId: degree.id,
                  schoolId: school.id,
                  graduate: graduate.id,
                  _include: ['degree', 'school'],
                  degree,
                  school,
                });
              }}
            />
          </View>
        )}
      </View>
    </Modal>
  ));
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    flex: 1,
    justifyContent: 'center',
    padding: 16,
  },
  dialog: {
    backgroundColor: '#FFF',
    borderRadius: 4,
    padding: 16,
  },
});
