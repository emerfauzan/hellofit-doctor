import React, { useEffect, useState } from 'react';
import { Modal, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { useObserver } from 'mobx-react-lite';
import moment from 'moment';
import _ from 'lodash';
import { Button, SelectInputField } from '../../../../components';
import { degreeStore, schoolStore } from '../../../../stores';
import TrashIcon from './trash.svg';

const graduates = _.range(1980, moment().year()).map((year) => ({
  id: year,
  name: year.toString(),
}));

export const EducationEditDialog = (props) => {
  const [schoolKeyword, setSchoolKeyword] = useState('');
  const [degree, setDegree] = useState(props.education.degree);
  const [school, setSchool] = useState(props.education.school);
  const [graduate, setGraduate] = useState({
    id: props.education.graduate,
    name: props.education.graduate.toString(),
  });
  const [schoolFilter, setSchoolFilter] = useState('');
  useEffect(() => {
    setDegree(props.education.degree);
    setSchool(props.education.school);
    setGraduate({
      id: props.education.graduate,
      name: props.education.graduate.toString(),
    });
  }, [props.education]);
  useEffect(() => {
    async function fetch() {
      await degreeStore.find();
      await schoolStore.find({
        query: {
          $sort: {
            id: 1,
          },
        },
      });
    }

    fetch();
  }, []);

  const search = _.debounce(async (keyword) => {
    if (keyword.length > 0) {
      await schoolStore.find({
        query: {
          name: {
            $ilike: `%${keyword}%`,
          },
        },
      });
    } else {
      await schoolStore.find();
    }
  }, 1000);

  const loadMore = _.debounce(async () => {
    await schoolStore.loadMore();
  }, 1000);

  return useObserver(() => (
    <Modal transparent {...props}>
      <View style={styles.container}>
        {degree && school && graduate && (
          <View style={styles.dialog}>
            <TouchableOpacity
              style={styles.textButton}
              onPress={() => props.onDeleteEducation(props.education)}
            >
              <Text style={styles.textButtonLabel}>Hapus data</Text>
              <TrashIcon />
            </TouchableOpacity>
            <SelectInputField
              label="Tingkat Pendidikan"
              items={degreeStore.data}
              value={degree}
              onChangeValue={(item) => setDegree(item)}
            />
            <SelectInputField
              label="Universitas"
              items={schoolKeyword.length > 2 ? schoolStore.data : []}
              searchable
              searchText={schoolKeyword}
              searchPlaceholder="Cari Universitas"
              value={school}
              onChangeSearchText={(keyword) => {
                setSchoolKeyword(keyword);
                search(keyword);
              }}
              onChangeValue={(item) => {
                setSchoolKeyword('')
                setSchool(item)
              }}
              onLoadMore={() => loadMore()}
            />
            <SelectInputField
              label="Tahun Lulus"
              items={graduates}
              value={graduate}
              onChangeValue={(value) => setGraduate(value)}
            />
            <Button
              label="Ubah"
              onPress={() => {
                props.onEditEducation({
                  ...props.education,
                  degreeId: degree.id,
                  schoolId: school.id,
                  graduate: graduate.id,
                  degree,
                  school,
                });
              }}
            />
          </View>
        )}
      </View>
    </Modal>
  ));
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    flex: 1,
    justifyContent: 'center',
    padding: 16,
  },
  dialog: {
    backgroundColor: '#FFF',
    borderRadius: 4,
    padding: 16,
  },
  textButton: {
    alignItems: 'center',
    alignSelf: 'flex-end',
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 4,
  },
  textButtonLabel: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
    lineHeight: 14,
    marginRight: 4,
  },
});
