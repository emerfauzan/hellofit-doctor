import React, { useEffect, useState } from 'react';
import {
  Modal,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { useObserver } from 'mobx-react-lite';
import { useImmer } from 'use-immer';
import { Button, SelectInputField } from '../../../../components';
import { hospitalStore } from '../../../../stores';
import { days, feathersClient } from '../../../../utils';
import { isPaginated } from '../../../../utils';
import { HospitalScheduleItem } from './hospital-schedule-item';
import TrashIcon from './trash.svg';

let nextScheduleId = -1;
let nextTimeId = -1;
export const HospitalScheduleEditDialog = (props) => {
  const [hospital, setHospital] = useState(props.hospital);
  const [hospitalSchedules, updateHospitalSchedules] = useImmer([]);
  const [hospitalKeyword, setHospitalKeyword] = useState('');
  const [hospitalData, setHospitalData] = useState([]);

  function searchHospital(val){
    setHospitalKeyword(val);
    let data = hospitalStore.data.filter((x)=>{
      if(val){
        return x.name.toLowerCase().search(val.toLowerCase()) !== -1
      }
    })
    if(val.length > 2){
      setHospitalData(data)
    }else{
      setHospitalData([])
    }
  }

  useEffect(() => {
    // TODO: loadMore
    hospitalStore.find({
      query: {
        $sort: {
          id: 1,
        },
      },
    });
  }, []);

  useEffect(() => {
    setHospital(props.hospital);

    async function fetch() {
      const result = await feathersClient.service('hospital-schedules').find({
        query: {
          hospitalId: props.hospital.id,
        },
      });

      if (isPaginated(result) && result.data.length > 0) {
        const { data } = result;
        const { doctorId, hospitalId, doctor, hospital } = data[0];
        const usedDays = result.data.map((item) => item.day);
        updateHospitalSchedules((draft) => {
          for (const day of days) {
            if (!usedDays.includes(day)) {
              data.push({
                id: nextScheduleId,
                doctorId,
                hospitalId,
                day,
                enabled: false,
                times: [],
                doctor,
                hospital,
              });
              nextScheduleId--;
            }
          }

          return _sort(JSON.parse(JSON.stringify(props.hospitalSchedules)));
        });
      }
    }

    fetch();
  }, [props.hospital, updateHospitalSchedules]);

  function _sort(schedules) {
    const days = {
      monday: 1,
      tuesday: 2,
      wednesday: 3,
      thursday: 4,
      friday: 5,
      saturday: 6,
      sunday: 7,
    };
    return schedules.sort((a, b) => {
      return days[a.day] - days[b.day];
    });
  }

  async function search (keyword) {
    if (keyword.length > 0) {
      await hospitalStore.find({
        query: {
          name: {
            $ilike: `%${keyword}%`,
          },
        },
      });
    } else {
      await hospitalStore.find();
    }
  }

  async function loadMore () {
    await hospitalStore.loadMore();
  }

  return useObserver(() => (
    <Modal transparent {...props}>
      <View style={styles.container}>
        <View style={styles.dialog}>
          <ScrollView>
            {/* <TouchableOpacity
              style={styles.textButton}
              onPress={() => {
                props.onDeleteHospital(props.hospital.id)
              }}
            >
              <Text style={styles.textButtonLabel}>Hapus data</Text>
              <TrashIcon />
            </TouchableOpacity> */}
            <View style={styles.hospital}>
              {hospitalStore.data.length > 0 && props.hospitalSchedules && props.hospitalSchedules[0].newData ? (
              <SelectInputField
                label="Rumah Sakit"
                items={hospitalData}
                value={hospital}
                onChangeValue={(value) => setHospital(value)}
                searchable
                searchPlaceholder="Cari Rumah Sakit"
                searchText={hospitalKeyword}
                onChangeSearchText={(keyword) => searchHospital(keyword)}
                onLoadMore={() => loadMore()}
              />
              ):(
              <SelectInputField
                label="Rumah Sakit"
                items={
                  hospitalKeyword.length > 2 ? hospitalStore.data.filter(
                    (hospital) => !props.hospitalIds.includes(hospital.id)
                  ) : []
                }
                searchable
                searchPlaceholder="Cari Rumah Sakit"
                searchText={hospitalKeyword}
                value={hospital || hospitalStore.data[0]}
                onChangeSearchText={(keyword) => {
                  setHospitalKeyword(keyword);
                  search(keyword);
                }}
                onChangeValue={(item) => setHospital(item)}
                onLoadMore={() => loadMore()}
              />
            )}
            </View>
            <Text style={styles.field}>Waktu Praktik</Text>
            {hospitalSchedules.map((hospitalSchedule, index) => (
              <HospitalScheduleItem
                key={hospitalSchedule.id}
                hospitalSchedule={hospitalSchedule}
                onChangeHospitalSchedule={(editedHospitalSchedule) => {
                  updateHospitalSchedules((draft) => {
                    draft[index] = editedHospitalSchedule;
                  });
                }}
                onNewTime={() => {
                  updateHospitalSchedules((draft) => {
                    draft[index].times.push({
                      id: nextTimeId,
                      scheduleId: hospitalSchedule.id,
                      startHour: 9,
                      startMinute: 0,
                      finishHour: 24,
                      finishMinute: 0,
                    });
                  });
                  nextTimeId--;
                }}
              />
            ))}
            <Button
              label={props.label ? props.label : "Simpan"}
              onPress={() => props.onConfirm(hospitalSchedules, hospital)}
            />
            <TouchableOpacity
              style={styles.cancelButton}
              onPress={props.onRequestClose}
            >
              <Text style={styles.cancelButtonLabel}>Batal</Text>
            </TouchableOpacity>
          </ScrollView>
        </View>
      </View>
    </Modal>
  ));
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    flex: 1,
    justifyContent: 'center',
    padding: 16,
  },
  dialog: {
    backgroundColor: '#FFF',
    borderRadius: 4,
    flex: 1,
  },
  hospital: {
    paddingHorizontal: 16,
    paddingTop: 16,
  },
  field: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
    marginLeft: 16,
  },
  cancelButton: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    height: 40,
    marginHorizontal: 16,
    marginBottom: 16,
  },
  cancelButtonLabel: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
  },
  textButton: {
    alignItems: 'center',
    alignSelf: 'flex-end',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 16,
    paddingRight: 16,
  },
  textButtonLabel: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
    lineHeight: 14,
    marginRight: 4,
  },
});
