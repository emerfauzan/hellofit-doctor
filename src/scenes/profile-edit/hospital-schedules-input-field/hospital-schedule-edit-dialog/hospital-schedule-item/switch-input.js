import React, { useState } from 'react';
import {
  Animated,
  StyleSheet,
  TouchableWithoutFeedback,
  View,
} from 'react-native';

export const SwitchInput = (props) => {
  const [left] = useState(new Animated.Value(props.value ? 18 : 2));
  let backgroundColor = '#CACDD5';
  let top = 2;

  if (props.value) {
    backgroundColor = '#FFCB05';
  }

  function _animate() {
    if (props.value) {
      Animated.timing(left, {
        toValue: 2,
        duration: 100,
      }).start();
    } else {
      Animated.timing(left, {
        toValue: 18,
        duration: 100,
      }).start();
    }

    props.onChangeValue(!props.value);
  }

  return (
    <TouchableWithoutFeedback onPress={() => _animate()}>
      <View style={styles.container}>
        <View
          style={[
            styles.inner,
            {
              backgroundColor,
            },
          ]}
        >
          <Animated.View
            style={[
              styles.circle,
              {
                left,
                top,
              },
            ]}
          />
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    height: 48,
    justifyContent: 'center',
    width: 64,
  },
  inner: {
    borderRadius: 8,
    height: 16,
    width: 32,
  },
  circle: {
    backgroundColor: '#FFF',
    borderRadius: 6,
    height: 12,
    width: 12,
  },
});
