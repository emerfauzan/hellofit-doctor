import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { produce } from 'immer';
import { DAYS } from '../../../../../utils';
import { SwitchInput } from './switch-input';
import { HospitalScheduleTimeItem } from './hospital-schedule-time-item';

export const HospitalScheduleItem = (props) => (
  <View style={styles.container}>
    <View style={styles.top}>
      <Text style={styles.day}>{DAYS[props.hospitalSchedule.day]}</Text>
      <SwitchInput
        value={props.hospitalSchedule.enabled}
        onChangeValue={(value) =>
          props.onChangeHospitalSchedule({
            ...props.hospitalSchedule,
            enabled: value,
          })
        }
      />
    </View>
    {props.hospitalSchedule.enabled && (
      <View>
        {props.hospitalSchedule.times.map((time, index) => (
          <HospitalScheduleTimeItem
            key={time.id}
            hospitalScheduleTime={time}
            onChangeTime={(time) => {
              const hospitalSchedule = produce(
                props.hospitalSchedule,
                (draft) => {
                  draft.times[index] = time;
                }
              );
              props.onChangeHospitalSchedule(hospitalSchedule);
            }}
          />
        ))}
        <TouchableOpacity
          style={styles.addTimeButton}
          onPress={() => props.onNewTime()}
        >
          <Text style={styles.addTimeButtonLabel}>+Tambah Waktu</Text>
        </TouchableOpacity>
      </View>
    )}
  </View>
);
const styles = StyleSheet.create({
  container: {
    borderBottomWidth: 1,
    borderColor: '#EBECF0',
    paddingLeft: 16,
  },
  top: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  day: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
  },
  addTimeButton: {
    alignSelf: 'flex-start',
    marginBottom: 8,
    padding: 8,
  },
  addTimeButtonLabel: {
    color: '#FFCB05',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
    lineHeight: 14,
  },
});
