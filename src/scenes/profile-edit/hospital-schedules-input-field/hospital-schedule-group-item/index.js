import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { DAYS, numeral } from '../../../../utils';
import EditIcon from './edit.svg';

function generateDescription(schedule) {
  let times = '';

  if (schedule.times) {
    for (const time of schedule.times) {
      const startHour = numeral(time.startHour).format('00');
      const startMinute = numeral(time.startMinute).format('00');
      const finishHour = numeral(time.finishHour).format('00');
      const finishMinute = numeral(time.finishMinute).format('00');
      times += ` | ${startHour}:${startMinute} - ${finishHour}:${finishMinute}`;
    }
  }

  return `${DAYS[schedule.day]}${times}`;
}

export const HospitalScheduleGroupItem = (props) => (
  <View style={styles.hospitalScheduleItem}>
    <View style={styles.data}>
      <Text style={styles.name}>
        {props.hospitalSchedules[0].hospital.name}
      </Text>
      {props.hospitalSchedules
        .filter((hospitalSchedule) => hospitalSchedule.enabled)
        .map((hospitalSchedule) => (
          <View key={hospitalSchedule.id}>
            <Text style={styles.time}>
              {generateDescription(hospitalSchedule)}
            </Text>
          </View>
        ))}
    </View>
    <TouchableOpacity style={styles.editButton} onPress={props.onPress}>
      <EditIcon />
    </TouchableOpacity>
  </View>
);
const styles = StyleSheet.create({
  hospitalScheduleItem: {
    alignItems: 'center',
    borderColor: '#EFF0F3',
    borderRadius: 4,
    borderWidth: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 4,
    paddingLeft: 16,
    paddingVertical: 16,
  },
  data: {
    flex: 1,
  },
  name: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    marginBottom: 4,
  },
  time: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
  },
  editButton: {
    padding: 8,
    margin: 8,
  },
});
