import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { useObserver } from 'mobx-react-lite';
import _ from 'lodash';
import { hospitalScheduleStore, } from '../../../stores';
import { HospitalScheduleGroupItem } from './hospital-schedule-group-item';
import { feathersClient, isPaginated } from '../../../utils';
import { TextButton } from './text-button';
import { AddDialog } from '../add-dialog';

export const HospitalScheduleInputField = (props) => {
  const [hospital, setHospital] = useState();
  const [showDialog, setShowDialog] = useState(false);
  const [editStatus, setEditStatus] = useState(false);
  const [hospitals, setHospitals] = useState();
  const [hospitalSchedules, setHospitalSchedules] = useState();

  useEffect(() => {
    async function run() {
      const result = await feathersClient.service('hospitals').find({
        query: {
          $sort: {
            id: 1,
          },
        },
      });

      if (isPaginated(result)) {
        setHospitals(result.data);
      }
    }

    run();
  }, []);

  return useObserver(() => {
    const groups = _.groupBy(
      hospitalScheduleStore.scheduleState,
      (hospitalSchedule) => hospitalSchedule.hospitalId
    );

    const groupSchedules = _.groupBy(
      hospitalScheduleStore.scheduleForm.schedules,
      hospitalScheduleStore.data,
      'hospitalId'
    );
    const hospitalIds = Object.keys(groupSchedules);

    return (
      <View style={styles.container}>
        <Text style={styles.label}>Lokasi Praktek</Text>
        {Object.values(groups).map((hospitalSchedules, index) => (
          <HospitalScheduleGroupItem
            key={index}
            hospitalSchedules={hospitalSchedules}
            onPress={() => {
              setHospital(hospitalSchedules[0].hospital);
              setHospitalSchedules(hospitalSchedules)
              setShowDialog(true)
              setEditStatus(true)
            }}
          />
        ))}
        {hospitals && hospitalIds.length < hospitals.length && (
          <TextButton
            label="+ Tambah Lokasi Praktek"
            style={{textAlign: 'right', marginTop: 3}}
            onPress={() => {
              setHospitalSchedules()
              setShowDialog(true)
            }}
          />
        )}
        {hospitals && (
          <AddDialog
            hospital={hospital}
            hospitalIds={hospitalIds.map((id) => Number(id))}
            visible={showDialog}
            edit={editStatus}
            hospitalSchedules={hospitalSchedules}
            onCancel={() => {
              setShowDialog(false)
              setEditStatus(false)
              setHospital()
            }}
            onConfirm={(data, hospitalData) => {
              const countTimes = data.schedules.reduce(
                (acc, cur) => acc + (cur.enabled ? cur.times.length : 0),
                0
              );
              if (countTimes > 0) {
                if(editStatus){
                  hospitalScheduleStore.replaceData(data.schedules, hospitalData, hospital.id)
                }else{
                  hospitalScheduleStore.addData(data.schedules, hospitalData)
                }
              }
              setShowDialog(false);
              setEditStatus(false)
              setHospital()
            }}
            onDeleteHospital={(hospitalId) => {
              hospitalScheduleStore.deleteData(hospitalId);
              setShowDialog(false);
              setHospital()
            }}
          />
        )}
      </View>
    );
  });
};
const styles = StyleSheet.create({
  container: {
    marginBottom: 16,
  },
  label: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
    marginBottom: 8,
  },
  textButton: {
    alignSelf: 'flex-end',
    padding: 8,
  },
  textButtonLabel: {
    color: '#FFCB05',
    fontFamily: 'Quicksand-Medium',
    lineHeight: 17,
  },
});
