import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';

export const TextButton = (props) => {
  const { label, ...others } = props;
  return (
    <TouchableOpacity style={styles.container} {...others}>
      <Text style={[styles.label, props.style]}>+ Tambah Lokasi Praktek</Text>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  container: {
    // alignItems: 'center',
    // marginHorizontal: 16,
    marginTop: 8,
    padding: 8,
  },
  label: {
    color: '#FFCB05',
    fontFamily: 'Quicksand-Medium',
    textAlign: 'center',
  },
});
