import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { TimeItem } from './time-item';
import { SwitchInput } from './switch-input';

export const ScheduleItem = (props) => {
  return (
    <View style={styles.container}>
      <View style={styles.top}>
        <Text style={styles.day}>{props.day}</Text>
        <SwitchInput value={props.enabled} onChangeValue={props.onToggle} />
      </View>
      <View style={styles.bottom}>
        {props.enabled && (
          <View>
            {props.times.map((time, index) => (
              <TimeItem
                key={index}
                time={time}
                onChangeTime={(time) => props.onChangeTime(time, index)}
              />
            ))}
            <TouchableOpacity
              style={styles.textButton}
              onPress={props.onAddTime}
            >
              <Text style={styles.textButtonLabel}>+ Tambah Waktu</Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    marginBottom: 1,
  },
  top: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 16,
    paddingTop: 8,
  },
  bottom: {
    borderBottomWidth: 1,
    borderColor: '#EBECF0',
    paddingLeft: 16,
    paddingBottom: 12,
  },
  day: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
  },
  offline: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
  },
  textButton: {
    alignSelf: 'flex-start',
    flexDirection: 'row',
    height: 32,
    paddingTop: 16,
  },
  textButtonLabel: {
    color: '#FFCB05',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
    lineHeight: 14,
  },
});
