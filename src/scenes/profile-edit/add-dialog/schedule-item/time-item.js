import React from 'react';
import { StyleSheet, View } from 'react-native';
import _ from 'lodash';
import { SelectInput } from '../../../../components';
import { numeral } from '../../../../utils';

const hours = _.range(0, 25).map((hour) => ({
  id: hour,
  name: `${numeral(hour).format('00')}:00`,
}));

export const TimeItem = (props) => {
  const { startHour, startMinute, finishHour, finishMinute } = props.time;
  const startTime = `${numeral(startHour).format('00')}:${numeral(
    startMinute
  ).format('00')}`;
  const finishTime = `${numeral(finishHour).format('00')}:${numeral(
    finishMinute
  ).format('00')}`;
  return (
    <View>
      <View style={styles.range}>
        <View style={styles.rangeItem}>
          <SelectInput
            items={hours.slice(
              0,
              hours.findIndex((item) => item.name === finishTime)
            )}
            value={hours.find((item) => item.name === startTime) || hours[0]}
            onChangeValue={(value) =>
              props.onChangeTime({
                ...props.time,
                startHour: Number(value.name.split(':')[0]),
                startMinute: Number(value.name.split(':')[1]),
              })
            }
          />
        </View>
        <View style={styles.separator} />
        <View style={styles.rangeItem}>
          <SelectInput
            items={hours.slice(
              hours.findIndex((item) => item.name === startTime) + 1
            )}
            value={hours.find((item) => item.name === finishTime) || hours[1]}
            onChangeValue={(value) =>
              props.onChangeTime({
                ...props.time,
                finishHour: Number(value.name.split(':')[0]),
                finishMinute: Number(value.name.split(':')[1]),
              })
            }
          />
        </View>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  range: {
    flexDirection: 'row',
    paddingTop: 8,
    paddingRight: 16,
  },
  rangeItem: {
    flex: 1,
  },
  separator: {
    alignSelf: 'center',
    borderColor: '#999DAD',
    borderTopWidth: 1,
    marginHorizontal: 6,
    width: 4,
  },
});
