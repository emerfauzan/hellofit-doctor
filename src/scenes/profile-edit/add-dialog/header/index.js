import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import CloseIcon from './close.svg';

export const Header = (props) => (
  <View style={styles.container}>
    <TouchableOpacity onPress={props.onClose}>
      <View style={styles.left}>
        <CloseIcon />
      </View>
    </TouchableOpacity>
    <View style={styles.middle}>
      <Text style={styles.title}>Lokasi Praktik</Text>
    </View>
  </View>
);
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#FFCB05',
    height: 56,
    flexDirection: 'row',
  },
  left: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 56,
    width: 56,
  },
  middle: {
    flex: 1,
    justifyContent: 'center',
  },
  title: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
    fontSize: 16,
    lineHeight: 19,
  },
});
