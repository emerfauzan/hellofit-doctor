import React, { useEffect, useState } from 'react';
import { Modal, ScrollView, StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { useObserver } from 'mobx-react-lite';
import { useImmer } from 'use-immer';
import _ from 'lodash';
import { Button, SelectInputField } from '../../../components';
import { hospitalStore, hospitalScheduleStore, profileStore } from '../../../stores';
import { Header } from './header';
import { ScheduleItem } from './schedule-item';
import TrashIcon from './trash.svg';
import { feathersClient } from '../../../utils';

const days = {
  monday: 'Senin',
  tuesday: 'Selasa',
  wednesday: 'Rabu',
  thursday: 'Kamis',
  friday: 'Jumat',
  saturday: 'Sabtu',
  sunday: 'Minggu',
};
const initialTime = {
  startHour: 8,
  startMinute: 0,
  finishHour: 17,
  finishMinute: 0,
};

export const AddDialog = (props) => {
  const [hospitalData, setHospitalData] = useState([]);
  const [searchData, setSearchData] = useState([]);
  const [hospital, setHospital] = useState();
  const [hospitalKeyword, setHospitalKeyword] = useState('');

  function searchHospital(val){
    setHospitalKeyword(val);
    let data = hospitalData.filter((x)=>{
      if(val){
        return x.name.toLowerCase().search(val.toLowerCase()) !== -1
      }
    })
    if(val.length > 2){
      setSearchData(data)
    }else{
      setSearchData([])
    }
  }

  let initialState;
  if(props.hospitalSchedules && props.edit){
    initialState = {
      schedules: _sort(JSON.parse(JSON.stringify(props.hospitalSchedules)))
    }
  }else{
    initialState = {
      schedules: Object.keys(days).map((day) => ({
        day,
        enabled: false,
        times: [initialTime],
      })),
    };
  }
  function _sort(schedules) {
    const days = {
      monday: 1,
      tuesday: 2,
      wednesday: 3,
      thursday: 4,
      friday: 5,
      saturday: 6,
      sunday: 7,
    };
    return schedules.sort((a, b) => {
      return days[a.day] - days[b.day];
    });
  }
  const [state, updateState] = useImmer(initialState);

  useEffect(() => {
    async function fetch() {
      const result = await feathersClient.service('hospitals')
      .find({ 
        query: { 
          $sort: {
            id: profileStore.doctor.id,
          },
          $limit: 50
        } 
      });
      const groups = _.groupBy(
        hospitalScheduleStore.scheduleState,
        (hospitalSchedule) => hospitalSchedule.hospitalId
      );
      let idData = Object.keys(groups).map((x,z)=>({id: parseInt(x)}))
      if (result.total > 0) {
        let dataResult = result.data
        for (let i = 0; i < idData.length; i++) {
          dataResult = dataResult.filter((x)=>{
            return x.id !== idData[i].id
          })
        }
        setHospitalData(dataResult);
      }
    }
    fetch();
  }, []);

  return useObserver(() => (
    <Modal
      onDismiss={props.onCancel}
      onRequestClose={props.onCancel}
      onShow={() => updateState((draft) => (draft = initialState))}
      {...props}
    >
      <Header onClose={props.onCancel} />
      <ScrollView>
        {
          props.edit &&
          <TouchableOpacity
            style={styles.textButton}
            onPress={() => props.onDeleteHospital(props.hospital.id)}
          >
            <Text style={styles.textButtonLabel}>Hapus data</Text>
            <TrashIcon />
          </TouchableOpacity>
        }
        <View style={styles.field}>
          <SelectInputField
            label="Rumah Sakit"
            items={searchData}
            value={hospital ? hospital : props.hospital ? props.hospital : hospitalData[0]}
            onChangeValue={(value) => {
              setHospital(value)
              setHospitalKeyword('')
              setSearchData([])
            }}
            searchable
            searchPlaceholder="Cari Rumah Sakit"
            searchText={hospitalKeyword}
            onChangeSearchText={(keyword) => searchHospital(keyword)}
            // onLoadMore={() => loadMore()}
          />
        </View>
        <Text style={styles.label}>Waktu Praktik</Text>
        {state.schedules && state.schedules.map((schedule, index) => (
          <ScheduleItem
            key={index}
            day={days[schedule.day]}
            enabled={schedule.enabled}
            times={schedule.times}
            onAddTime={() => {
              updateState((draft) => {
                draft.schedules[index].times.push(initialTime);
              });
            }}
            onChangeTime={(time, timeIndex) => {
              updateState((draft) => {
                draft.schedules[index].times[timeIndex] = time;
              });
            }}
            onToggle={() =>
              updateState((draft) => {
                const status = draft.schedules[index].enabled;
                draft.schedules[index].enabled = !status;
              })
            }
          />
        ))}
        <Button
          label={props.edit ? "Simpan" : "Tambah Lokasi"}
          onPress={() => {
            props.onConfirm(state, hospital ? hospital : props.hospital ? props.hospital : hospitalData[0]);
            searchHospital('')
            setHospital()
          }}
        />
        <TouchableOpacity
          style={styles.cancelButton}
          onPress={props.onCancel}
        >
          <Text style={styles.cancelButtonLabel}>Batal</Text>
        </TouchableOpacity>
      </ScrollView>
    </Modal>
  ));
};
const styles = StyleSheet.create({
  field: {
    paddingHorizontal: 16,
    paddingTop: 16,
  },
  label: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
    paddingLeft: 16,
  },
  cancelButton: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    height: 40,
    marginHorizontal: 16,
    marginBottom: 16,
  },
  textButton: {
    alignItems: 'center',
    alignSelf: 'flex-end',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 16,
    paddingRight: 16,
  },
  textButtonLabel: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
    lineHeight: 14,
    marginRight: 4,
  },
});
