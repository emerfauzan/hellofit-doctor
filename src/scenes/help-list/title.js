import React from 'react';
import { StyleSheet, Text } from 'react-native';

export const Title = (props) => {
  const { style, ...others } = props;
  return <Text style={styles.container} {...others} />;
};
const styles = StyleSheet.create({
  container: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
    fontSize: 16,
    marginVertical: 8,
  },
});
