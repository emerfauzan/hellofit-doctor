import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import ChevronIcon from './chevron.svg';

export const HelpItem = (props) => (
  <View style={styles.container}>
    <TouchableOpacity onPress={props.onPress}>
      <View style={styles.inner}>
        <Text style={styles.text}>{props.help.question}</Text>
        <ChevronIcon />
      </View>
    </TouchableOpacity>
  </View>
);
const styles = StyleSheet.create({
  container: {
    borderColor: '#EFF0F3',
    borderBottomWidth: 1,
  },
  inner: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 12,
  },
  text: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
  },
});
