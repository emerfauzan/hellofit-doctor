import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

export const Button = (props) => (
  <TouchableOpacity>
    <View style={styles.container}>
      {props.icon}
      <View style={styles.space} />
      <Text style={styles.label}>{props.label}</Text>
    </View>
  </TouchableOpacity>
);
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    borderColor: '#FFCB05',
    borderRadius: 4,
    borderWidth: 1,
    flexDirection: 'row',
    height: 40,
    justifyContent: 'center',
    width: 160,
  },
  space: {
    width: 12,
  },
  label: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    fontSize: 16,
    lineHeight: 19,
  },
});
