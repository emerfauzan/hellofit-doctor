import React, { useEffect } from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { useObserver } from 'mobx-react-lite';
import { Header, Page } from '../../components';
import { helpStore } from '../../stores';
import { Button } from './button';
import { HelpItem } from './help-item';
import { Search } from './search';
import { Title } from './title';
import EmailIcon from './email.svg';
import PhoneIcon from './phone.svg';

export const HelpList = (props) => {
  useEffect(() => {
    helpStore.find({
      query: {
        type: 'doctor',
      },
    });
  }, []);
  return useObserver(() => (
    <Page style={styles.page}>
      <Header title="Bantuan" onBack={() => Actions.pop()} />
      <ScrollView contentContainerStyle={styles.content}>
        <Search />
        <View style={styles.space} />
        <Title>Hubungi Kami</Title>
        <View style={styles.buttonRow}>
          <Button icon={<EmailIcon />} label="Email" />
          <View style={styles.space} />
          <Button icon={<PhoneIcon />} label="Telepon" />
        </View>
        <Title>Frequently Ask Question (FAQ)</Title>
        {helpStore.data.map((help) => (
          <HelpItem
            key={help.id}
            help={help}
            onPress={() =>
              Actions.push('help-detail', {
                help,
              })
            }
          />
        ))}
      </ScrollView>
    </Page>
  ));
};
const styles = StyleSheet.create({
  page: {
    backgroundColor: '#FFF',
  },
  content: {
    padding: 16,
  },
  buttonRow: {
    flexDirection: 'row',
    marginVertical: 8,
  },
  space: {
    height: 8,
    width: 8,
  },
});
