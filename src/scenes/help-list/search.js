import React from 'react';
import { StyleSheet, TextInput } from 'react-native';

export const Search = (props) => (
  <TextInput
    autoCapitalize="none"
    autoCorrect={false}
    placeholder="Cari pertanyaan..."
    placeholderTextColor="#9CA2B4"
    selectionColor="#FFCB05"
    style={styles.container}
    {...props}
  />
);
const styles = StyleSheet.create({
  container: {
    borderColor: '#E5E7E9',
    borderRadius: 4,
    borderWidth: 1,
    fontFamily: 'Quicksand-Regular',
    fontSize: 16,
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
});
