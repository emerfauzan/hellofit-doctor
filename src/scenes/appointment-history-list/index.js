import React, { useEffect } from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { useObserver } from 'mobx-react-lite';
import { Header, Page } from '../../components';
import { appointmentHistoryStore } from '../../stores';

export const AppointmentHistoryList = (props) => {
  useEffect(() => {
    appointmentHistoryStore.find();
  }, []);
  return useObserver(() => (
    <Page>
      <Header title="Riwayat Book Jadwal" onBack={() => Actions.pop()} />
      <ScrollView contentContainerStyle={styles.container}>
        {/* {appointmentHistoryStore.data.map((appointment) => (
         <AppointmentHistoryItem
           key={appointment.id}
           appointment={appointment}
         />
        ))} */}
      </ScrollView>
    </Page>
  ));
};
const styles = StyleSheet.create({
  container: {
    margin: 16,
  },
});
