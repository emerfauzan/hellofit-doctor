import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import moment from 'moment';
import { formatPatientDescription } from '../../utils';

export const AppointmentHistoryItem = (props) => {
  const date = moment(props.appointment.bookedAt).format('DD MMM');
  return (
    <View style={styles.container}>
      <View style={styles.top}>
        <Text style={styles.time}>Selesai</Text>
      </View>
      <View style={styles.bottom}>
        <View>
          <Text style={styles.name}>{props.appointment.patient.name}</Text>
          <Text style={styles.description}>
            {formatPatientDescription(props.appointment.patient)}
          </Text>
          <Text style={styles.hospital}>
            {props.appointment.hospitalSchedule.hospital.name}
          </Text>
        </View>
        <View style={styles.bottomRight}>
          <Text style={styles.date}>{date}</Text>
        </View>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    marginBottom: 16,
  },
  top: {
    alignItems: 'center',
    backgroundColor: '#E1E4EB',
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    height: 24,
    justifyContent: 'center',
  },
  time: {
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
    lineHeight: 14,
  },
  bottom: {
    backgroundColor: '#FFF',
    flexDirection: 'row',
    paddingHorizontal: 16,
    paddingVertical: 12,
  },
  name: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
  },
  description: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
  },
  hospital: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
  },
  bottomRight: {
    alignItems: 'flex-end',
    flex: 1,
  },
  date: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
  },
});
