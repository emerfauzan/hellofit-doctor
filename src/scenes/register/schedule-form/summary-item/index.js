import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { numeral } from '../../../../utils';
import TrashIcon from './trash.svg';

export const SummaryItem = (props) => {
  function generateDescription(schedule) {
    let times = '';

    for (const time of schedule.times) {
      const startHour = numeral(time.startHour).format('00');
      const startMinute = numeral(time.startMinute).format('00');
      const finishHour = numeral(time.finishHour).format('00');
      const finishMinute = numeral(time.finishMinute).format('00');
      times += ` | ${startHour}:${startMinute} - ${finishHour}:${finishMinute}`;
    }

    return `${schedule.day}${times}`;
  }

  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.label}>{props.hospitalName}</Text>
        {props.schedules
          .filter((schedule) => schedule.enabled)
          .map((schedule) => (
            <Text key={schedule.day} style={styles.description}>
              {generateDescription(schedule)}
            </Text>
          ))}
      </View>
      <TouchableOpacity style={styles.iconButton} onPress={props.onDelete}>
        <TrashIcon />
      </TouchableOpacity>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    borderColor: '#EFF0F3',
    borderRadius: 4,
    borderWidth: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 8,
    padding: 16,
    paddingRight: 8,
  },
  label: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
  },
  description: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
    marginTop: 4,
  },
  iconButton: {
    alignItems: 'center',
    height: 40,
    justifyContent: 'center',
    width: 40,
  },
});
