import React, { useEffect, useState } from 'react';
import { Modal, ScrollView, StyleSheet, Text, View } from 'react-native';
import { useObserver } from 'mobx-react-lite';
import { useImmer } from 'use-immer';
import _ from 'lodash';
import { Button, SelectInputField } from '../../../../components';
import { hospitalStore } from '../../../../stores';
import { Header } from './header';
import { ScheduleItem } from './schedule-item';

const days = {
  monday: 'Senin',
  tuesday: 'Selasa',
  wednesday: 'Rabu',
  thursday: 'Kamis',
  friday: 'Jumat',
  saturday: 'Sabtu',
  sunday: 'Minggu',
};
const initialTime = {
  startHour: 8,
  startMinute: 0,
  finishHour: 17,
  finishMinute: 0,
};
export const AddDialog = (props) => {
  const [hospitalKeyword, setHospitalKeyword] = useState('');
  const [hospitalItem, setHospitalItem] = useState();
  const initialState = {
    schedules: Object.keys(days).map((day) => ({
      day,
      enabled: false,
      times: [initialTime],
    })),
  };
  const [state, updateState] = useImmer(initialState);
  useEffect(() => {
    async function fetch() {
      const result = await hospitalStore.find({
        query: {
          $sort: {
            id: 1,
          },
        },
      });

      if (result.total > 0) {
        setHospitalItem(result.data[0]);
      }
    }

    fetch();
  }, []);

  const search = _.debounce(async (keyword) => {
    if (keyword.length > 0) {
      await hospitalStore.find({
        query: {
          name: {
            $ilike: `%${keyword}%`,
          },
        },
      });
    } else {
      await hospitalStore.find();
    }
  }, 1000);

  const loadMore = _.debounce(async () => {
    await hospitalStore.loadMore();
  }, 1000);

  return useObserver(() => (
    <Modal
      onDismiss={props.onCancel}
      onRequestClose={props.onCancel}
      onShow={() => updateState((draft) => (draft = initialState))}
      {...props}
    >
      <Header onClose={props.onCancel} />
      <ScrollView>
        <View style={styles.field}>
          {hospitalStore.data.length > 0 && (
            <SelectInputField
              label="Rumah Sakit"
              items={
                hospitalKeyword.length > 2 ? hospitalStore.data.filter(
                  (hospital) => !props.hospitalIds.includes(hospital.id)
                ) : []
              }
              searchable
              searchPlaceholder="Cari Rumah Sakit"
              searchText={hospitalKeyword}
              value={hospitalItem || hospitalStore.data[0]}
              onChangeSearchText={(keyword) => {
                setHospitalKeyword(keyword);
                search(keyword);
              }}
              onChangeValue={(item) => setHospitalItem(item)}
              onLoadMore={() => loadMore()}
            />
          )}
        </View>
        <Text style={styles.label}>Waktu Praktik</Text>
        {state.schedules.map((schedule, index) => (
          <ScheduleItem
            key={index}
            day={days[schedule.day]}
            enabled={schedule.enabled}
            times={schedule.times}
            onAddTime={() => {
              updateState((draft) => {
                draft.schedules[index].times.push(initialTime);
              });
            }}
            onChangeTime={(time, timeIndex) => {
              updateState((draft) => {
                draft.schedules[index].times[timeIndex] = time;
              });
            }}
            onToggle={() =>
              updateState((draft) => {
                const status = draft.schedules[index].enabled;
                draft.schedules[index].enabled = !status;
              })
            }
          />
        ))}
      </ScrollView>
      <Button
        disabled={!hospitalItem}
        label="Tambah Lokasi"
        onPress={() => {
          if (hospitalItem) {
            props.onConfirm(hospitalItem.id, state);
          }
        }}
      />
    </Modal>
  ));
};
const styles = StyleSheet.create({
  field: {
    paddingHorizontal: 16,
    paddingTop: 16,
  },
  label: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
    paddingLeft: 16,
  },
});
