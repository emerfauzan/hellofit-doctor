import React, { useEffect, useState } from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';
import { useObserver } from 'mobx-react-lite';
import _ from 'lodash';
import { Button } from '../../../components';
import { registerStore } from '../../../stores';
import { feathersClient, isPaginated } from '../../../utils';
import { Stepper } from '../stepper';
import { Title } from '../title';
import { AddDialog } from './add-dialog';
import { SummaryItem } from './summary-item';
import { TextButton } from './text-button';

export const ScheduleForm = (props) => {
  const [showDialog, setShowDialog] = useState(false);
  const [hospitals, setHospitals] = useState();
  useEffect(() => {
    async function run() {
      const result = await feathersClient.service('hospitals').find({
        query: {
          $sort: {
            id: 1,
          },
        },
      });

      if (isPaginated(result)) {
        setHospitals(result.data);
      }
    }

    run();
  }, []);
  return useObserver(() => {
    const groupSchedules = _.groupBy(
      registerStore.scheduleForm.schedules,
      'hospitalId'
    );

    const hospitalIds = Object.keys(groupSchedules);

    function renderGroups() {
      const result = [];

      for (const key of Object.keys(groupSchedules)) {
        const schedules = groupSchedules[key].filter(
          (schedule) => schedule.enabled
        );

        if (schedules.length > 0 && hospitals) {
          const hospital = hospitals.find(
            (hospital) => hospital.id === schedules[0].hospitalId
          );
          const hospitalName = hospital ? hospital.name : '';
          result.push(
            <SummaryItem
              key={hospitalName}
              hospitalName={hospitalName}
              schedules={schedules}
              onDelete={() => {
                const filterSchedules = registerStore.scheduleForm.schedules.filter(
                  (schedule) =>
                    schedule.hospitalId !== (hospital && hospital.id)
                );
                registerStore.scheduleForm.schedules.replace(filterSchedules);
              }}
            />
          );
        }
      }

      return result;
    }

    return (
      <View style={styles.container}>
        <ScrollView
          keyboardShouldPersistTaps="handled"
          contentContainerStyle={styles.content}
        >
          <Title />
          <Stepper step={3} />
          {renderGroups()}
          {hospitals && hospitalIds.length < hospitals.length && (
            <TextButton
              label="+ Tambah Lokasi Praktek"
              onPress={() => setShowDialog(true)}
            />
          )}
        </ScrollView>
        <Button
          label="Selanjutnya"
          disabled={Object.keys(groupSchedules).length === 0}
          onPress={() => (registerStore.step = 4)}
        />
        {hospitals && (
          <AddDialog
            hospitalIds={hospitalIds.map((id) => Number(id))}
            visible={showDialog}
            onCancel={() => setShowDialog(false)}
            onConfirm={(hospitalId, data) => {
              const countTimes = data.schedules.reduce(
                (acc, cur) => acc + (cur.enabled ? cur.times.length : 0),
                0
              );

              if (countTimes > 0) {
                data.schedules.forEach((schedule) => {
                  registerStore.scheduleForm.schedules.push({
                    hospitalId,
                    day: schedule.day,
                    enabled: schedule.enabled,
                    times: schedule.times,
                  });
                });
              }

              setShowDialog(false);
            }}
          />
        )}
      </View>
    );
  });
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    padding: 16,
  },
});
