import React, { useState } from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { useObserver } from 'mobx-react-lite';
import { Button } from '../../../components';
import { degreeStore, registerStore, schoolStore } from '../../../stores';
import { AddDialog } from './add-dialog';
import { Stepper } from '../stepper';
import { Title } from '../title';
import TrashIcon from './trash-icon.svg';

export const EducationForm = (props) => {
  const [showDialog, setShowDialog] = useState(false);
  return useObserver(() => {
    function degreeName(degreeId) {
      return degreeId ? degreeStore.records.get(degreeId).name : '';
    }

    function schoolName(schoolId) {
      return schoolId ? schoolStore.records.get(schoolId).name : '';
    }

    return (
      <>
        <ScrollView
          keyboardShouldPersistTaps="handled"
          contentContainerStyle={styles.container}
        >
          <Title />
          <Stepper step={2} />
          {registerStore.educationForm.educations.map((education, i) => (
            <View key={i} style={styles.educationItem}>
              <Text style={styles.label}>
                {degreeName(education.degreeId)},{' '}
                {schoolName(education.schoolId)}, {education.graduate}
              </Text>
              <TouchableOpacity
                style={styles.iconButton}
                onPress={() => {
                  registerStore.educationForm.educations.remove(education);
                }}
              >
                <TrashIcon />
              </TouchableOpacity>
            </View>
          ))}
          <TouchableOpacity
            style={styles.textButton}
            onPress={() => setShowDialog(true)}
          >
            <Text style={styles.textButtonLabel}>+ Tambah Pendidikan</Text>
          </TouchableOpacity>
        </ScrollView>
        <Button
          label="Selanjutnya"
          disabled={registerStore.educationForm.educations.length === 0}
          onPress={() => (registerStore.step = 3)}
        />
        <AddDialog
          visible={showDialog}
          onDismiss={() => setShowDialog(false)}
          onRequestClose={() => setShowDialog(false)}
          onAddEducation={() => setShowDialog(false)}
        />
      </>
    );
  });
};
const styles = StyleSheet.create({
  container: {
    padding: 16,
  },
  educationItem: {
    alignItems: 'center',
    borderColor: '#EFF0F3',
    borderRadius: 4,
    borderWidth: 1,
    flexDirection: 'row',
    height: 50,
    justifyContent: 'space-between',
    marginTop: 8,
    paddingLeft: 16,
  },
  label: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
  },
  iconButton: {
    alignItems: 'center',
    height: 40,
    justifyContent: 'center',
    width: 40,
  },
  textButton: {
    alignItems: 'center',
    marginHorizontal: 16,
    marginTop: 8,
    padding: 8,
  },
  textButtonLabel: {
    color: '#FFCB05',
    fontFamily: 'Quicksand-Medium',
  },
});
