import React, { useEffect, useState } from 'react';
import { Modal, StyleSheet, View } from 'react-native';
import { useObserver } from 'mobx-react-lite';
import _ from 'lodash';
import moment from 'moment';
import { Button, SelectInputField } from '../../../components';
import { degreeStore, registerStore, schoolStore } from '../../../stores';

const search = _.debounce(async (keyword) => {
  if (keyword.length > 0) {
    await schoolStore.find({
      query: {
        name: {
          $ilike: `%${keyword}%`,
        },
      },
    });
  } else {
    await schoolStore.find();
  }
}, 1000);

const loadMore = _.debounce(async () => {
  await schoolStore.loadMore();
}, 1000);

const graduateYears = _.range(1980, moment().year()).map((n) => ({
  id: n,
  name: n.toString(),
}));

export const AddDialog = (props) => {
  const [schoolKeyword, setSchoolKeyword] = useState('');
  const [degreeItem, setDegreeItem] = useState();
  const [schoolItem, setSchoolItem] = useState();
  const [graduateItem, setGraduateItem] = useState(graduateYears[0]);
  useEffect(() => {
    async function fetch() {
      await degreeStore.find();
      await schoolStore.find({
        query: {
          $sort: {
            id: 1,
          },
        },
      });
    }

    fetch();
  }, []);

  function _handleShow() {
    if (degreeStore.data.length > 0) {
      setDegreeItem(degreeStore.data[0]);
    }

    if (schoolStore.data.length > 0) {
      setSchoolItem(schoolStore.data[0]);
    }

    graduateYears[0];
  }

  function _addEducation() {
    const found = registerStore.educationForm.educations.find(
      (education) =>
        education.degreeId === (degreeItem && degreeItem.id) &&
        education.schoolId === (schoolItem && schoolItem.id)
    );

    if (!found) {
      registerStore.educationForm.educations.push({
        degreeId: degreeItem && degreeItem.id,
        schoolId: schoolItem && schoolItem.id,
        graduate: graduateItem && graduateItem.id,
      });
    }

    props.onAddEducation();
  }

  return useObserver(() => {
    if (degreeStore.data.length === 0 || schoolStore.data.length === 0) {
      return null;
    }

    return (
      <Modal transparent {...props} onShow={() => _handleShow()}>
        <View style={styles.container}>
          <View style={styles.inner}>
            {degreeStore.data.length > 0 && (
              <SelectInputField
                label="Tingkat Pendidikan"
                items={degreeStore.data}
                value={degreeItem || degreeStore.data[0]}
                onChangeValue={(item) => setDegreeItem(item)}
              />
            )}
            {schoolStore.data.length > 0 && (
              <SelectInputField
                label="Universitas"
                items={schoolKeyword.length > 2 ? schoolStore.data : []}
                searchable
                searchPlaceholder="Cari Universitas"
                searchText={schoolKeyword}
                value={schoolItem || schoolStore.data[0]}
                onChangeSearchText={(keyword) => {
                  setSchoolKeyword(keyword);
                  search(keyword);
                }}
                onChangeValue={(item) => setSchoolItem(item)}
                onLoadMore={() => loadMore()}
              />
            )}
            <SelectInputField
              label="Tahun Kelulusan"
              items={graduateYears}
              value={graduateItem || graduateYears[0]}
              onChangeValue={(item) => setGraduateItem(item)}
            />
            <Button label="Tambah Pendidikan" onPress={() => _addEducation()} />
          </View>
        </View>
      </Modal>
    );
  });
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(55, 63, 80, 0.6)',
    flex: 1,
    justifyContent: 'center',
  },
  inner: {
    backgroundColor: '#FFF',
    borderColor: '#707070',
    borderRadius: 4,
    borderWidth: 1,
    marginHorizontal: 16,
    paddingHorizontal: 16,
    paddingVertical: 16,
  },
});
