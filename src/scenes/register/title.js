import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { HeaderLayout, TextButton } from '../../components';

export const Title = (props) => (
  <HeaderLayout transparent>
    <View>
      <Text style={styles.title}>Buat Akun Baru</Text>
    </View>
    <View style={styles.subtitle}>
      <TextButton
        label1="Sudah punya akun? "
        label2="Masuk"
        onPress={() => Actions.pop()}
      />
    </View>
  </HeaderLayout>
);
const styles = StyleSheet.create({
  title: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
    fontSize: 24,
  },
  subtitle: {
    alignItems: 'flex-start',
  },
});
