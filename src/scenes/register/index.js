import React, { useEffect } from 'react';
import { StyleSheet } from 'react-native';
import { useObserver } from 'mobx-react-lite';
import { Page } from '../../components';
import { registerStore } from '../../stores';
import { AccountForm } from './account-form';
import { EducationForm } from './education-form';
import { ScheduleForm } from './schedule-form';
import { VerificationForm } from './verification-form';

export const Register = (props) => {
  useEffect(() => {
    registerStore.step = 1;
  }, []);
  return useObserver(() => (
    <Page style={styles.container} loading={registerStore.loading}>
      {registerStore.step === 1 && <AccountForm />}
      {registerStore.step === 2 && <EducationForm />}
      {registerStore.step === 3 && <ScheduleForm />}
      {registerStore.step === 4 && <VerificationForm />}
    </Page>
  ));
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
  },
});
