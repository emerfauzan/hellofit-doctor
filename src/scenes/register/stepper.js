import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export const Stepper = (props) => {
  function _labelStyle(step) {
    let color = '#C9CDD6';

    if (step <= props.step) {
      color = '#373F50';
    }

    return [
      styles.label,
      {
        color,
      },
    ];
  }

  return (
    <View style={styles.container}>
      <Text style={_labelStyle(1)}>Akun</Text>
      <View style={styles.line} />
      <Text style={_labelStyle(2)}>Pendidikan</Text>
      <View style={styles.line} />
      <Text style={_labelStyle(3)}>Lokasi Praktik</Text>
      <View style={styles.line} />
      <Text style={_labelStyle(4)}>Verifikasi</Text>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#F5F6F8',
    borderColor: '#EBECF0',
    borderRadius: 20,
    borderWidth: 1,
    flexDirection: 'row',
    height: 40,
    justifyContent: 'center',
    marginVertical: 16,
  },
  label: {
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
    lineHeight: 14,
  },
  line: {
    borderTopWidth: 1,
    borderColor: '#C9CDD6',
    marginHorizontal: 8,
    width: 12,
  },
});
