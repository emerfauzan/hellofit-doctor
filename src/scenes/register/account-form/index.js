import React, { useEffect, useState } from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { useObserver } from 'mobx-react-lite';
import {
  Button,
  Footer,
  SelectInputField,
  TextInputField,
} from '../../../components';
import { registerStore, specialtyStore } from '../../../stores';
import { Stepper } from '../stepper';
import { Title } from '../title';
import { DropDownField } from '../../../components/drop-down-field';
import Toast from 'react-native-simple-toast';

export const AccountForm = (props) => {
  const [showGender, setShowGender] = useState(false);
  const [showPassword, setShowPassword] = useState(true);

  function showPasswordFunc() {
    setShowPassword(!showPassword)
  }

  useEffect(() => {
    specialtyStore.find({
      query: {
        $limit: 50,
      },
    });
  }, []);
  return useObserver(() => (
    <>
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={styles.container}
      >
        <Title />
        <Stepper step={1} />
        <TextInputField
          autoCapitalize="words"
          autoCompleteType="off"
          autoCorrect={false}
          label="Nama Lengkap Beserta Gelar"
          placeholder="Contoh: drg. Alam"
          value={registerStore.accountForm.name}
          onChangeText={(text) => {
            registerStore.accountForm.name = text;
          }}
        />
        <DropDownField
          label="Jenis Kelamin"
          placeholder="Pilih jenis kelamin Anda"
          value={registerStore.accountForm.gender}
          showGender={showGender}
          setShowGender={()=> setShowGender(true)}
          // disabled={true}
          onSelectMale={()=> {
            registerStore.accountForm.gender = 'laki'
            setShowGender(false)
          }}
          onSelectFemale={()=> {
            registerStore.accountForm.gender = 'perempuan'
            setShowGender(false)
          }}
        />
        <TextInputField
          keyboardType="number-pad"
          label="Tanggal Lahir"
          options={{
            mask: '99-99-9999',
          }}
          placeholder="dd-mm-yyyy"
          type="custom"
          value={registerStore.accountForm.birthdate}
          onChangeText={(text) => {
            registerStore.accountForm.birthdate = text;
          }}
        />
        {specialtyStore.data.length > 0 && (
          <SelectInputField
            label="Spesialisasi"
            items={specialtyStore.data}
            value={
              specialtyStore.data.find(
                (item) => item.id === registerStore.accountForm.specialtyId
              ) || specialtyStore.data[0]
            }
            onChangeValue={(item) =>
              (registerStore.accountForm.specialtyId = item.id)
            }
          />
        )}
        <TextInputField
          autoCapitalize="none"
          autoCompleteType="off"
          autoCorrect={false}
          keyboardType="email-address"
          label="Email"
          value={registerStore.accountForm.email}
          onChangeText={(text) => {
            registerStore.accountForm.email = text;
          }}
        />
        <TextInputField
          keyboardType="number-pad"
          label="Nomor Handphone"
          value={registerStore.accountForm.phone}
          onChangeText={(text) => {
            registerStore.accountForm.phone = text;
          }}
        />
        <TextInputField
          autoCapitalize="none"
          label="Kata Sandi"
          secureTextEntry={showPassword}
          icon={true}
          setShowPassword={()=> showPasswordFunc()}
          value={registerStore.accountForm.password}
          onChangeText={(text) => {
            registerStore.accountForm.password = text;
          }}
        />
      </ScrollView>
      <Footer>
        <Button
          label="Selanjutnya"
          disabled={!registerStore.accountFormValid}
          onPress={() => {
            if(!registerStore.birthdateValid){
              return Toast.show('Format Tanggal Lahir salah')
            }
            if(!registerStore.emailValid){
              return Toast.show('Format E-mail salah')
            }
            (registerStore.step = 2)
          }}
        />
      </Footer>
    </>
  ));
};
const styles = StyleSheet.create({
  container: {
    padding: 16,
  },
});
