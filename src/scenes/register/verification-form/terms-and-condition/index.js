import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import BlankIcon from './blank.svg';
import FilledIcon from './filled.svg';

export const TermsAndCondition = (props) => (
  <TouchableOpacity onPress={() => props.onPress()}>
    <View style={styles.container}>
      {props.selected ? <FilledIcon /> : <BlankIcon />}
      <View style={styles.right}>
        <Text>
          <Text style={styles.label}>Saya telah membaca dan menyetujui </Text>
          <Text
            onPress={() => props.onLink()}
            style={[styles.label, styles.link]}
          >
            Syarat & Ketentuan{' '}
          </Text>
          <Text style={styles.label}>Hellofit</Text>
        </Text>
      </View>
    </View>
  </TouchableOpacity>
);
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginTop: 48,
  },
  right: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginLeft: 8,
  },
  label: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    lineHeight: 17,
  },
  link: {
    color: '#FFCB05',
  },
});
