import React, { useState } from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { useObserver } from 'mobx-react-lite';
import Toast from 'react-native-simple-toast';
import { Actions } from 'react-native-router-flux';
import {
  Button,
  InfoDialog,
  TextInputField,
  UploadField,
} from '../../../components';
import { registerStore } from '../../../stores';
import { Stepper } from '../stepper';
import { Title } from '../title';
import { TermsAndCondition } from './terms-and-condition';

export const VerificationForm = (props) => {
  const [showDialog, setShowDialog] = useState(false);
  const [showAlert, setShowAlert] = useState(false);

  async function _submit() {
    if(registerStore.verificationForm.idNo.length < 16){
      return setShowAlert(true)
    }
    registerStore.loading = true;

    try {
      await registerStore.submit();
      registerStore.loading = false;
      setShowDialog(true);
    } catch (error) {
      registerStore.loading = false;
      setTimeout(() => {
        Toast.show(error.message);
      }, 100);
    }
  }

  return useObserver(() => (
    <>
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={styles.container}
      >
        <Title />
        <Stepper step={4} />
        <TextInputField
          label="No. STR"
          keyboardType="number-pad"
          value={registerStore.verificationForm.regNo}
          onChangeText={(text) => (registerStore.verificationForm.regNo = text)}
        />
        <UploadField
          label="Upload STR (optional)"
          value={registerStore.verificationForm.regCard}
          onChangeValue={(value) =>
            (registerStore.verificationForm.regCard = value)
          }
        />
        <TextInputField
          label="No. KTP"
          keyboardType="number-pad"
          value={registerStore.verificationForm.idNo}
          onChangeText={(text) => (registerStore.verificationForm.idNo = text)}
        />
        <UploadField
          label="Upload Kartu Identitas"
          value={registerStore.verificationForm.idCard}
          onChangeValue={(value) =>
            (registerStore.verificationForm.idCard = value)
          }
        />
        <TermsAndCondition
          selected={registerStore.verificationForm.termsAgreed}
          onLink={() => {}}
          onPress={() => {
            if(registerStore.verificationForm.termsAgreed){
              registerStore.verificationForm.termsAgreed = false
            }else{
              registerStore.verificationForm.termsAgreed = true;
              Actions.push('terms-and-conditions');
            }
          }}
        />
        <InfoDialog
          visible={showDialog}
          title="Data Berhasil Dikirim"
          description="Pendaftaran Anda telah kami terima dan sedang dalam proses verifikasi selama kurang lebih 2 hari kerja."
          confirmLabel="Oke"
          onConfirm={() => Actions.reset('login')}
        />
      </ScrollView>
      <InfoDialog
        visible={showAlert}
        title="Validasi No KTP"
        description="NIK (no KTP) harus terdiri dari 16 digit angka"
        confirmLabel="OK"
        onConfirm={() => {
          setShowAlert(false)
        }}
      />
      <Button
        label="Daftar"
        disabled={!registerStore.verificationFormValid}
        onPress={() => _submit()}
      />
    </>
  ));
};
const styles = StyleSheet.create({
  container: {
    padding: 16,
  },
});
