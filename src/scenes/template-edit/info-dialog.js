import React from 'react';
import { Modal, StyleSheet, Text, View } from 'react-native';
import { Button } from '../../components';

export const InfoDialog = (props) => {
  const cancelLabel = props.cancelLabel || 'Batal';
  const confirmLabel = props.confirmLabel || 'Iya';
  return (
    <Modal
      transparent
      onDismiss={props.onCancel}
      onRequestClose={props.onCancel}
      {...props}
    >
      <View style={styles.outer}>
        <View style={styles.inner}>
          {/* <Text style={styles.description}>{props.description}</Text> */}
          <Text style={styles.title}>{props.description1}</Text>
          <Text style={styles.title}>{props.description2}</Text>
          <View style={styles.footer}>
            <View style={styles.footerItem}>
              <Button 
                color="#E6495A"
                label={confirmLabel} 
                fontColor="white"
                onPress={props.onConfirm}
              />
            </View>
            {props.onCancel && (
              <View style={styles.footerItem}>
                <Button
                  color="#EBECF0"
                  label={cancelLabel}
                  onPress={props.onCancel}
                />
              </View>
            )}
            {props.onCancel && <View style={styles.blank} />}
          </View>
        </View>
      </View>
    </Modal>
  );
};
const styles = StyleSheet.create({
  outer: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    flex: 1,
    justifyContent: 'center',
  },
  inner: {
    alignItems: 'center',
    backgroundColor: '#FFF',
    borderRadius: 4,
    margin: 16,
    paddingHorizontal: 22,
    paddingVertical: 20,
  },
  title: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
    fontSize: 18,
  },
  description: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    lineHeight: 24,
    marginBottom: 16,
    marginTop: 12,
    textAlign: 'center',
  },
  footer: {
    flexDirection: 'row',
    marginTop: 15
  },
  footerItem: {
    flex: 1,
    marginHorizontal: -10,
  },
  blank: {
    width: 8,
  },
});
