import React, { createRef, useState, useEffect } from 'react';
import { ScrollView, StyleSheet, TouchableOpacity, View, Text, Animated, TextInput } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { useObserver } from 'mobx-react-lite';
import { profileStore } from '../../stores';
import { Header, Page } from '../../components';
import AddIcon from './add-template.svg';
import DeleteIcon from './trash.svg';
import EditIcon from './edit.svg';
import SearchIcon from './search.svg';
import { InfoDialog } from './info-dialog';
import { AddTemplate } from './add-template';
import { feathersClient } from '../../utils';
import Toast from 'react-native-simple-toast';

export const TemplateEdit = (props) => {
  const [loading, setLoading] = useState(true);
  const [idDelete, setIdDelete] = useState(0);
  const [alertDelete, setAlertDelete] = useState(false);
  const [showAddTemplate, setShowAddTemplate] = useState(false);
  const [valueTitle, setValueTitle] = useState('');
  const [valueDesc, setValueDesc] = useState('');
  const [search, setSearch] = useState('');
  const [templateItem, setTemplateItem] = useState([])

  useEffect(() => {
    getData();
  }, [])
  
  async function getData() {
    const result = await feathersClient.service('reply-templates').find({
      query:{
        doctorId: profileStore.doctor.id,
      }
    })
    setTemplateItem(result.data)
    setLoading(false)
  }

  async function postData() {
    setLoading(true)
    await feathersClient.service('reply-templates').create({
      doctorId: profileStore.doctor.id,
      text: textValue,
    }).catch(error => {
      Toast.show(error.message);
      setLoading(false)
    })
    setValueTitle('')
    setValueDesc('')
    setShowAddTemplate(false)
    getData()
  }

  async function removeData() {
    setLoading(true)
    await feathersClient.service('reply-templates').remove({
      id: idDelete
    })
    setIdDelete(0)
    setAlertDelete(false)
    getData()
  }

  async function getMore() {
    const result = await feathersClient.service('reply-templates')
      .find({ 
        query: { 
          doctorId: profileStore.doctor.id,
          $skip: templateItem.length,
        } 
      });
      setTemplateItem(templateItem.concat(result.data))
      setLoading(false)
  }

  function isScrollToBottom ({ layoutMeasurement, contentOffset, contentSize }) {
    const paddingToBottom = 20;
    return (
       layoutMeasurement.height + contentOffset.y >=
       contentSize.height - paddingToBottom
    );
  };

  function searchTemplate(){
    let data = templateItem.filter((x)=>{
      if(search){
        return x.text.toLowerCase().search(search.toLowerCase()) !== -1
      }else{
        return x
      }
    })
    return data
  }
  
  return useObserver(() => (
    <Page loading={loading}>
      <Header
        title="Reply Template"
        onBack={() => Actions.pop()}
      />
      <InfoDialog
        visible={alertDelete}
        description1="Apakah kamu yakin ingin"
        description2="Menghapus template ini?"
        onCancel={() => setAlertDelete(false)}
        onConfirm={() => removeData()}
      />
      <AddTemplate
        visible={showAddTemplate}
        onRequestClose={() => {
          setValueTitle('');
          setValueDesc('');
          setShowAddTemplate(false);
        }}
        onChangeTitle={(text) => setValueTitle(text)}
        onChangeDesc={(text) => {
          if(text.length < 500){
            setValueDescs(text);
          }
        }}
        valueTitle={valueTitle}
        valueDesc={valueDesc}
        onSimpan={()=> postData()}
      />
      <View style={styles.viewTop}>
        <View style={styles.searchBox}>
          <TextInput
            style={[styles.textArea, {height: 40}]}
            underlineColorAndroid="transparent"
            autoCapitalize='none'
            placeholder="Cari Template"
            placeholderTextColor='#C9CDD6'
            value={search}
            onChangeText={(text)=> setSearch(text)}
            // onSubmitEditing={searchTemplate()}
          />
          <SearchIcon/>
        </View>
      </View>
      <ScrollView
        onScroll={Animated.event([
          { nativeEvent: { contentOffset: { y: new Animated.Value(0) } } }
        ])}
        onMomentumScrollEnd={async ({ nativeEvent }) => {
          if (isScrollToBottom(nativeEvent)) {
            getMore()
          }
        }}
      >
        <View style={styles.container}>
          {
            searchTemplate(templateItem).map((item) => (
              <View>
                <View style={styles.menuItem}>
                  <View style={styles.firstItem}>
                    <View style={styles.iconRow}>
                      <Text style={[styles.menuLabel, {maxWidth: '80%'}]}>{item.title ? item.title : 'Judul'}</Text>
                      <TouchableOpacity 
                        style={styles.button}
                        key={item.id} 
                        onPress={() => {
                          setIdDelete(item.id);
                          setValueTitle(item.title ? item.title : '');
                          setValueDesc(item.text);
                          setShowAddTemplate(true);
                        }}
                      >
                        <EditIcon/>
                      </TouchableOpacity>
                    </View>
                    <TouchableOpacity 
                      style={styles.button}
                      key={item.id} 
                      onPress={() => {
                        setIdDelete(item.id);
                        setAlertDelete(true);
                      }}
                    >
                      <DeleteIcon/>
                    </TouchableOpacity>
                  </View>
                  <Text style={[styles.menuLabel, {paddingTop: 8}]}>{item.text}</Text>
                </View>
              </View>
            ))
          }
        </View>
      </ScrollView>
      <TouchableOpacity
        style={styles.floating}
        onPress= {()=> setShowAddTemplate(true)}
      >
        <AddIcon/>
      </TouchableOpacity>
    </Page>
  ));
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    flex: 1,
    paddingHorizontal: 16,
    paddingVertical: 20,
  },
  menuItem: {
    borderColor: '#EBECF0',
    backgroundColor: '#F9F9FB',
    marginBottom: 12,
    borderRadius: 5,
    justifyContent: 'space-between',
    padding: 10,
  },
  menuLabel: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    fontSize: 14,
  },
  floating: {
    position: 'absolute',
    bottom: 24,
    right: 16,
    backgroundColor: '#FFCB05',
    width: 50,
    height: 50,
    borderRadius: 50/2,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 5,

    //shadow
    shadowColor: 'grey',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,
  },
  firstItem: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    borderBottomColor: '#C9CDD6',
    borderBottomWidth: 1,
    paddingBottom: 10,
  },
  iconRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  button: {
    paddingHorizontal: 10,
    justifyContent: 'center',
  },
  viewTop: {
    backgroundColor: '#FFFFFF',

    //shadow
    shadowColor: '#F9F9FB',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,
    elevation: 4,
  },
  searchBox: {
    height: 40,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    marginHorizontal: 16,
    marginVertical: 6,
    borderColor: '#EBECF0',
    borderRadius: 4,
    borderWidth: 1,
    alignItems: 'center'
  },
  textArea: {
    width: '90%',
    justifyContent: "flex-start",
    fontFamily: 'Quicksand-Medium',
    fontSize: 14,
    paddingHorizontal: 16,
    paddingVertical: 9,
  },
});
