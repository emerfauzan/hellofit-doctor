import React from 'react';
import {
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
} from 'react-native';
import BackIcon from './back.svg';
import { Button } from '../../components';

export const AddTemplate = (props) => (
  <Modal {...props} style={{backgroundColor: 'red'}} >
    <View style={styles.header}>
      <TouchableOpacity onPress={props.onRequestClose}>
        <View style={styles.left}>
          <BackIcon />
        </View>
      </TouchableOpacity>
      <View style={styles.middle}>
        <Text style={styles.title}>Buat Template</Text>
      </View>
    </View>
    <View style={styles.container}>
      <Text style={styles.label}>Judul</Text>
      <TextInput
        style={[styles.textArea, {height: 40}]}
        underlineColorAndroid="transparent"
        autoCapitalize='words'
        placeholder="Judul template"
        placeholderTextColor='#C9CDD6'
        value={props.valueTitle}
        onChangeText={props.onChangeTitle}
      />
      <Text style={styles.label}>Deskripsi</Text>
      <TextInput
        textAlignVertical='top'
        style={styles.textArea}
        underlineColorAndroid="transparent"
        placeholder="Masukkan deskripsi"
        placeholderTextColor='#C9CDD6'
        numberOfLines={10}
        multiline={true}
        value={props.valueDesc}
        onChangeText={props.onChangeDesc}
      />
      <Text style={styles.message}>*maksimal 500 karakter</Text>
      <Button
        color="#FFCB05"
        label='Simpan'
        disabled={props.valueTitle.length > 0 && props.valueDesc > 0 ? false : true}
        onPress={props.onSimpan}
      />
    </View>
  </Modal>
);
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    backgroundColor: '#FFCB05',
    flexDirection: 'row',
    height: 56,
  },
  title: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
    fontSize: 16,
    lineHeight: 19,
  },
  left: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 56,
    width: 56,
  },
  middle: {
    flex: 1,
    justifyContent: 'center',
  },
  textArea: {
    height: 128,
    justifyContent: "flex-start",
    fontFamily: 'Quicksand-Medium',
    fontSize: 14,
    marginHorizontal: 16,
    padding: 10,
    marginTop: 8,
    borderColor: '#373F50',
    borderRadius: 4,
    borderWidth: 1,
  },
  message:{
    color: '#999DAD',
    fontSize: 10,
    fontFamily: 'Quicksand-Regular',
    alignSelf: 'flex-end',
    marginRight: 20,
    marginBottom: 10,
  },
  label: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
    lineHeight: 15,
    marginTop: 4,
    paddingLeft: 16,
    paddingTop: 16,
  },
});
