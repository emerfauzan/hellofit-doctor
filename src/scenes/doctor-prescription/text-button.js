import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

export const TextButton = (props) => (
  <TouchableOpacity onPress={props.onPress}>
    <View style={styles.container}>
      <Text style={styles.label}>{props.label}</Text>
    </View>
  </TouchableOpacity>
);
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flexDirection: 'row',
    height: 40,
    justifyContent: 'center',
  },
  label: {
    color: '#FFCB05',
    fontFamily: 'Quicksand-Medium',
  },
});
