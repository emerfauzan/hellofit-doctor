import React, { useState } from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import medicineImage from './medicine.png';
import ArrowDown from './arrow-down.svg';
import ArrowUp from './arrow-up.svg';
import DeleteIcon from './delete.svg';
import EditIcon from './edit.svg';

export const PrescriptionItem = (props) => {
  const [showDetail, setShowDetail] = useState(false);
  return (
    <View style={styles.container}>
      <View style={styles.top}>
        <View style={styles.left}>
          <Image
            source={medicineImage}
            style={styles.image}
            resizeMode="contain"
          />
        </View>
        <View style={styles.middle}>
          <Text style={styles.name}>Paratusin 10 Tablet</Text>
          <Text>
            <Text style={styles.price}>Rp 11.400</Text>
            <Text style={styles.quantity}> x1</Text>
          </Text>
        </View>
        <View style={styles.right}>
          <TouchableOpacity>
            <View style={styles.iconButton}>
              <EditIcon />
            </View>
          </TouchableOpacity>
          <TouchableOpacity>
            <View style={styles.iconButton}>
              <DeleteIcon />
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => setShowDetail(!showDetail)}>
            <View style={styles.toggleButton}>
              {showDetail ? <ArrowUp /> : <ArrowDown />}
            </View>
          </TouchableOpacity>
        </View>
      </View>
      {showDetail && (
        <View style={styles.detail}>
          <View style={styles.field}>
            <Text style={styles.fieldName}>Aturan Pakai</Text>
            <Text style={styles.fieldValue}>3 x 1 hari | Sebelum Makan</Text>
          </View>
          <View style={styles.field}>
            <Text style={styles.fieldName}>Lama Pemakaian</Text>
            <Text style={styles.fieldValue}>7 hari</Text>
          </View>
          <View style={styles.field}>
            <Text style={styles.fieldName}>Catatan</Text>
            <Text style={styles.fieldValue}>-</Text>
          </View>
        </View>
      )}
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    marginBottom: 8,
  },
  top: {
    backgroundColor: '#FFF',
    flexDirection: 'row',
    height: 68,
  },
  left: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 68,
  },
  middle: {
    flex: 1,
    justifyContent: 'center',
  },
  right: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    paddingRight: 8,
  },
  image: {
    height: 40,
    width: 40,
  },
  name: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
  },
  price: {
    color: '#FFCB05',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
  },
  quantity: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
  },
  description: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
  },
  iconButton: {
    alignItems: 'center',
    backgroundColor: '#EBECF0',
    borderRadius: 12,
    height: 24,
    justifyContent: 'center',
    margin: 4,
    width: 24,
  },
  toggleButton: {
    alignItems: 'center',
    height: 24,
    justifyContent: 'center',
    margin: 4,
    width: 24,
  },
  detail: {
    backgroundColor: '#FFF',
    borderColor: '#EBECF0',
    borderTopWidth: 1,
    padding: 16,
  },
  field: {
    marginBottom: 8,
  },
  fieldName: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
  },
  fieldValue: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
  },
});
