import React from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Button, Header, Page } from '../../components';
import { PrescriptionItem } from './prescription-item';
import { TextButton } from './text-button';

export const DoctorPrescription = (props) => (
  <Page>
    <Header title="Rekomendasi Obat" onClose={() => Actions.pop()} />
    <ScrollView>
      <PrescriptionItem />
      <PrescriptionItem />
      <View
        style={{
          backgroundColor: '#FFF',
        }}
      >
        <TextButton
          label="Tambah Obat"
          onPress={() => Actions.push('doctor-prescription-add')}
        />
      </View>
    </ScrollView>
    <View style={styles.footer}>
      <Button label="Bagikan Rekomendasi" onPress={() => Actions.pop()} />
    </View>
  </Page>
);
const styles = StyleSheet.create({
  footer: {
    backgroundColor: '#FFF',
    borderColor: '#EBECF0',
    borderTopWidth: 1,
  },
});
