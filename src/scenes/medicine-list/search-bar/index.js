import React, { useState } from 'react';
import { StyleSheet, TextInput, View } from 'react-native';
import ZoomIcon from './zoom.svg';

export const SearchBar = (props) => {
  const [keywords, setKeywords] = useState('');
  return (
    <View style={styles.container}>
      <TextInput
        autoCapitalize="none"
        autoCompleteType="off"
        autoCorrect={false}
        placeholder="Cari Obat"
        placeholderTextColor="#9CA2B4"
        selectionColor="#FFCB05"
        style={styles.input}
        value={keywords}
        onChangeText={(text) => setKeywords(text)}
      />
      <View style={styles.icon}>
        <ZoomIcon />
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    elevation: 1,
    justifyContent: 'center',
    padding: 8,
    shadowColor: '#000',
    shadowOffset: {
      height: 1,
      width: 0,
    },
    shadowOpacity: 0.18,
    shadowRadius: 1.0,
  },
  input: {
    borderColor: '#E5E7E9',
    borderRadius: 4,
    borderWidth: 1,
    fontFamily: 'Quicksand-Regular',
    fontSize: 16,
    paddingLeft: 16,
    paddingRight: 32,
    paddingVertical: 8,
  },
  icon: {
    position: 'absolute',
    right: 16,
  },
});
