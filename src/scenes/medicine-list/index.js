import React from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Header, Page } from '../../components';
import { MedicineItem } from './medicine-item';
import { SearchBar } from './search-bar';

export const MedicineList = (props) => (
  <Page>
    <Header title="Cari Obat" onClose={() => Actions.pop()} />
    <SearchBar />
    <ScrollView contentContainerStyle={styles.container}>
      <MedicineItem />
      <MedicineItem />
      <MedicineItem />
      <MedicineItem />
      <MedicineItem />
      <MedicineItem />
    </ScrollView>
  </Page>
);
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    padding: 16,
  },
});
