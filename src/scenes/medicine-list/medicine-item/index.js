import React from 'react';
import { Dimensions, Image, StyleSheet, Text, View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Button } from './button';
import medicineImage from './medicine.png';

const width = (Dimensions.get('window').width - 40) / 2;
export const MedicineItem = (props) => (
  <View style={styles.container}>
    <Image source={medicineImage} resizeMode="contain" style={styles.image} />
    <Text style={styles.name}>Paratusin 10 Tablet</Text>
    <Text style={styles.price}>Rp 11.400</Text>
    <View style={styles.space} />
    <Button label="Pilih" onPress={() => Actions.pop()} />
  </View>
);
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    borderRadius: 4,
    justifyContent: 'flex-start',
    marginBottom: 12,
    padding: 8,
    width,
  },
  image: {
    width: width - 16,
  },
  name: {
    color: '#373F50',
    fontFamily: 'Quicksand-SemiBold',
  },
  space: {
    height: 8,
  },
  price: {
    color: '#FFCB05',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
  },
});
