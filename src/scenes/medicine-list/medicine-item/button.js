import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';

export const Button = (props) => {
  const { label, ...others } = props;
  return (
    <TouchableOpacity style={styles.container} {...others}>
      <Text style={styles.label}>{props.label}</Text>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#FFCB05',
    borderRadius: 16,
    height: 32,
    justifyContent: 'center',
  },
  label: {
    color: '#373F50',
    fontFamily: 'Quicksand-SemiBold',
    fontSize: 12,
    lineHeight: 14,
  },
});
