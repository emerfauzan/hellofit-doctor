import { useEffect } from 'react';
import { StatusBar, Platform } from 'react-native';
import { Actions } from 'react-native-router-flux';
import RNBootSplash from 'react-native-bootsplash';
import DeviceInfo from 'react-native-device-info';
import semverDiff from 'semver-diff';
import { profileStore } from '../../stores';
import { feathersClient } from '../../utils';

export const Splash = (props) => {
  useEffect(() => {
    const _run = async () => {
      const appVersion = DeviceInfo.getVersion();
      const version = await feathersClient.service('versions').get(1);
      if (Platform.OS === 'android' && semverDiff(appVersion, version.doctor)) {
        Actions.reset('version', {
          current: appVersion,
          next: version.doctor,
        });
        RNBootSplash.hide({
          duration: 250,
        });
      } else {
        if (props.url == null) {
          try {
            await feathersClient.reAuthenticate();
            if(props.notifData !== ""){
              const chat =  await feathersClient.service('chats').find({
                query: {
                  id: props.notifData.id,
                }
              })
              console.log("DATAA ", chat.data[0])
              Actions.reset('main');

              Actions.push('chat-detail', {
                chat: chat.data[0],
              });
            } else {
              Actions.reset('main');

            }
          } catch (error) {
            Actions.reset('login');
          } finally {
            RNBootSplash.hide({
              duration: 250,
            });
            StatusBar.setHidden(false);
          }
        } else {
          const route = props.url.replace(/.*?:\/\//g, '');
          const routeArr = route.split('/');
          Actions.reset('reset-password', { id: routeArr[3] });
        }
      }
    };

    _run();
  }, []);
  return null;
};
