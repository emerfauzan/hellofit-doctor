import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import moment from 'moment';

export const ChatHistoryItem = (props) => {
  const age = moment().diff(moment(props.chat.patient.birthdate), 'years');
  const { height, weight } = props.chat.patient;
  let chatDate;

  if (props.chat.closedAt) {
    chatDate = moment(props.chat.closedAt).format('DD MMM YYYY');
  }

  return (
    <TouchableOpacity onPress={props.onPress}>
      <View style={styles.top}>
        <Text style={styles.title}>Selesai</Text>
      </View>
      <View style={styles.bottom}>
        <View>
          <Text style={styles.name}>{props.chat.patient.name}</Text>
          <Text style={styles.description}>
            {age} Tahun ･ {height} cm ･ {weight} kg
          </Text>
        </View>
        <View>
          <Text style={styles.time}>{chatDate}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  top: {
    alignItems: 'center',
    backgroundColor: '#E1E4EB',
    height: 24,
    justifyContent: 'center',
  },
  bottom: {
    backgroundColor: '#FFFFFF',
    borderRadius: 4,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 12,
    paddingHorizontal: 16,
    paddingVertical: 12,
  },
  title: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
    lineHeight: 14,
  },
  name: {
    color: '#373F50',
    fontFamily: 'Quicksand-SemiBold',
    fontSize: 16,
  },
  description: {
    color: '#9CA2B4',
    fontFamily: 'Quicksand-Regular',
  },
  time: {
    color: '#9CA2B4',
    fontFamily: 'Quicksand-Regular',
    fontSize: 12,
  },
});
