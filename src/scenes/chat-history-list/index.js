import React, { useEffect } from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { useObserver } from 'mobx-react-lite';
import { Header, Page } from '../../components';
import { chatHistoryStore } from '../../stores';
import { ChatHistoryItem } from './chat-history-item';

export const ChatHistoryList = (props) => {
  useEffect(() => {
    chatHistoryStore.find({
      query: {
        $sort: {
          closedAt: -1,
        },
        status: 'completed',
      },
    });
  }, []);
  return useObserver(() => (
    <Page>
      <Header title="Riwayat Chat" onBack={() => Actions.pop()} />
      <ScrollView contentContainerStyle={styles.container}>
        {chatHistoryStore.data.map((chat) => (
          <ChatHistoryItem
            key={chat.id}
            chat={chat}
            onPress={() =>
              Actions.push('chat-history-detail', {
                chat,
              })
            }
          />
        ))}
      </ScrollView>
    </Page>
  ));
};
const styles = StyleSheet.create({
  container: {
    padding: 16,
  },
});
