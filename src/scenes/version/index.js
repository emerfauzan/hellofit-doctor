import React from 'react';
import { Linking, StyleSheet, Text, View } from 'react-native';
import { Button } from '../../components';

export const Version = (props) => (
  <View style={styles.container}>
    <View style={styles.section}>
      <Text style={styles.title}>Versi anda {props.current}</Text>
      <Text style={styles.description}>
        Saat ini telah tersedia versi terbaru {props.next}, silahkan upgrade
        terlebih dahulu di Google Play Store
      </Text>
    </View>
    <Button
      label="Upgrade"
      onPress={() =>
        Linking.openURL(
          'https://play.google.com/store/apps/details?id=id.hellofit.doctor'
        )
      }
    />
  </View>
);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    padding: 16,
  },
  section: {
    alignItems: 'center',
  },
  title: {
    fontFamily: 'Quicksand-Bold',
    fontSize: 16,
    marginBottom: 16,
  },
  description: {
    fontFamily: 'Quicksand-Medium',
    textAlign: 'center',
    marginBottom: 16,
  },
});
