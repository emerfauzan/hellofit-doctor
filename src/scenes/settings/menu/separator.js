import React from 'react';
import { StyleSheet, View } from 'react-native';

export const Separator = (props) => (
  <View style={styles.container}>
    <View style={styles.inner} />
  </View>
);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 52,
  },
  inner: {
    borderColor: '#EFF0F3',
    borderBottomWidth: 1,
  },
});
