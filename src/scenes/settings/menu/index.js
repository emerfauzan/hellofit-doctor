import React, { Children } from 'react';
import { StyleSheet, View } from 'react-native';
import { Separator } from './separator';

export * from './menu-item';
export const Menu = (props) => (
  <View style={styles.container}>
    {Children.map(props.children, (child, i) => (
      <>
        {child}
        {i < Children.count(props.children) - 1 && <Separator />}
      </>
    ))}
  </View>
);
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    borderRadius: 4,
  },
});
