import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

export const MenuItem = (props) => (
  <TouchableOpacity onPress={props.onPress}>
    <View style={styles.container}>
      <View style={styles.left}>{props.icon}</View>
      <View style={styles.right}>
        <Text style={styles.label}>{props.label}</Text>
      </View>
    </View>
  </TouchableOpacity>
);
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  left: {
    alignItems: 'center',
    height: 50,
    justifyContent: 'center',
    width: 50,
  },
  right: {
    flex: 1,
    justifyContent: 'center',
  },
  label: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    lineHeight: 17,
  },
});
