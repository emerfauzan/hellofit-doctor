import React from 'react';
import { Actions } from 'react-native-router-flux';
import { Header, Page } from '../../components';
import { Menu, MenuItem } from './menu';
import CalendarIcon from './calendar.svg';
import KeyIcon from './key.svg';
import PriceIcon from './price.svg';
import ChatIcon from './chat.svg';

export const Settings = (props) => (
  <Page>
    <Header title="Pengaturan" onBack={() => Actions.pop()} />
    <Menu>
      {/* <MenuItem
       icon={<UserIcon />}
       label="Ubah Profil"
       onPress={() => Actions.push('profile-edit')}
      /> */}
      <MenuItem
        icon={<KeyIcon />}
        label="Ubah Kata Sandi"
        onPress={() => Actions.push('change-password')}
      />
      <MenuItem
        icon={<CalendarIcon />}
        label="Atur Jadwal Online"
        onPress={() => Actions.push('schedule-edit')}
      />
      <MenuItem
        icon={<PriceIcon />}
        label="Atur Biaya Konsultasi"
        onPress={() => Actions.push('price-edit')}
      />
      <MenuItem
        icon={<ChatIcon />}
        label="Atur Reply Template"
        onPress={() => Actions.push('template-edit')}
      />
    </Menu>
  </Page>
);
