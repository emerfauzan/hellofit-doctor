import React, { useState } from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Toast from 'react-native-simple-toast';
import { Header, Page, TextInputField } from '../../components';
import { profileStore } from '../../stores';

export const ChangePassword = (props) => {
  const [loading, setLoading] = useState(false);
  const [password, setPassword] = useState('');
  const [showPassword, setShowPassword] = useState(true);

  function showPasswordFunc() {
    setShowPassword(!showPassword)
  }

  async function _submit() {
    if (password.length < 6) {
      Toast.show('Kata sandi kurang dari 6 karakter');
    } else {
      setLoading(true);

      try {
        await profileStore.updatePassword(password);
        setLoading(false);
        Actions.pop();
      } catch (error) {
        setLoading(false);
        setTimeout(() => {
          Toast.show(error.message);
        }, 100);
      }
    }
  }

  return (
    <Page loading={loading}>
      <Header
        title="Ubah Kata Sandi"
        buttonRight="Simpan"
        onBack={() => Actions.pop()}
        onRight={() => _submit()}
      />
      <ScrollView contentContainerStyle={styles.container}>
        <TextInputField
          label="Kata Sandi Baru (minimal 6 karakter)"
          autoCapitalize="none"
          placeholder="******"
          secureTextEntry={showPassword}
          icon={true}
          setShowPassword={()=> showPasswordFunc()}
          value={password}
          onChangeText={(text) => setPassword(text)}
        />
      </ScrollView>
    </Page>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    padding: 16,
  },
});
