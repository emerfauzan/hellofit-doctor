import React, { useState, useEffect } from 'react';
import { StatusBar, Linking, AppState, AsyncStorage, SafeAreaView } from 'react-native';
import { Actions, Router, Scene, Stack } from 'react-native-router-flux';
import messaging from '@react-native-firebase/messaging';
import { TabIcon } from './components';
import PushNotification from 'react-native-push-notification';
import ReactNativeAN from 'react-native-alarm-notification';
import {
  AppointmentHistoryList,
  AppointmentList,
  ArticlePilihan,
  ChangePassword,
  ChatDetail,
  ChatHistoryDetail,
  ChatHistoryList,
  ChatTabs,
  DoctorNote,
  DoctorPrescription,
  DoctorPrescriptionAdd,
  ForgotPassword,
  HelpDetail,
  HelpList,
  Login,
  MedicineList,
  PriceEdit,
  TemplateEdit,
  Profile,
  ProfileEdit,
  RatingList,
  Register,
  ResetPassword,
  ScheduleEdit,
  Settings,
  Splash,
  TermsAndConditions,
  Version,
  Home,
} from './scenes';
import { feathersClient } from './utils'
import { scheduleEditStore, chatMessageStore } from './stores';

const alarmNotifData = {
  title: 'Alert',
  message: 'Hellofit Doctor',
  vibrate: false,
  // vibrate: true,
  play_sound: true,
  schedule_type: 'once',
  channel: 'wakeup',
  // data: {content: 'my notification id is 22'},
  loop_sound: true,
  sound_names: 'notif_hellofit.mp3'
  // has_button: true,
};

const fireDate = ReactNativeAN.parseDate(new Date(Date.now()))

const useMount = func => useEffect(() => func(), []);

const useInitialURL = () => {
  const [url, setUrl] = useState(null);
  const [processing, setProcessing] = useState(true);

  useMount(() => {
    const getUrlAsync = async () => {
      // Get the deep link used to open the app
      AppState.addEventListener('change', handleAppStateChange);
      const initialUrl = await Linking.getInitialURL();
      
      // The setTimeout is just for testing purpose
      setTimeout(() => {
        setUrl(initialUrl);
        setProcessing(false);
      }, 1000);
    };

    getUrlAsync();
    
  });

  return { url, processing };
};

const handleAppStateChange = async (event) => {
  const initial = await Linking.getInitialURL();
  
  if (initial !== null && !initialised) {
    setInitialised(true);
    // app was opened by a Universal Link
    // custom setup dependant on URL...
  }
}


export const App = (props) => {
  const { url: initialUrl, processing } = useInitialURL();
  const [notifType, setNotifType] = useState("");
  const [notifData, setNotifData] = useState("");
  const [isInitiate, setInitiate] = useState(false);
  const [isFromNotif, setFromNotif] = useState(false);

  function _backAndroidHandler() {
    if (
      ['version', 'login', '_chat-tabs', 'reset-password'].indexOf(Actions.currentScene) != -1
    ) {
      return false;
    } else {
      Actions.pop();
      return true;
    }
  }

  useEffect(() => {
    // Assume a message-notification contains a "type" property in the data payload of the screen to open
    requestPermissionUser()
    ReactNativeAN.stopAlarmSound();
    messaging().onNotificationOpenedApp(remoteMessage => {
        'Notification caused app to open from background state:',
        remoteMessage,
      setNotifType(remoteMessage.data?.type)
      if(remoteMessage.data.type === "online-schedule"){
        ReactNativeAN.stopAlarmSound();
        setOnlineSchedule({onlineSchedule: false})
      }
      // setOnlineSchedule({onlineSchedule: false})
    });

    // Check whether an initial notification is available
    messaging()
      .getInitialNotification()
      .then(remoteMessage => {
        if (remoteMessage) {
          setFromNotif(true)
            'Notification caused app to open from quit state:',
            remoteMessage,
          setNotifType(remoteMessage.data?.type)

          if(remoteMessage.data.type === "chat" || remoteMessage.data.type === "chat-accept"){
            let dataFormat = JSON.parse(remoteMessage.data?.data)
            setNotifData(dataFormat)
            getChatData(dataFormat)
          } else {
            setInitiate(true)
          }
          if(remoteMessage.data.type === "online-schedule"){
            ReactNativeAN.stopAlarmSound();
            setOnlineSchedule({onlineSchedule: false})
          }
        }
        // setOnlineSchedule({onlineSchedule: false})

      });
  }, []);
  
  async function setOnlineSchedule(data){
    const ONLINE_SCHEDULE = "ONLINE_SCHEDULE"
    const onlineSchedule = await AsyncStorage.getItem(ONLINE_SCHEDULE);
    if(!onlineSchedule){
      await AsyncStorage.setItem(ONLINE_SCHEDULE, JSON.stringify(data));
      console.log("SET ACT")
    }else{
      ReactNativeAN.stopAlarmSound();
      await AsyncStorage.removeItem(ONLINE_SCHEDULE)
      console.log("SET DEACT")

    }
  }

  async function getChatData(data){
    await feathersClient.service('chats').find({
      query: {
        id: data.id,
      }
    }).then((result) => {
      setNotifData(result.data[0])
    })

    setInitiate(true)
  }

  useEffect(() => {
    const unsubsribe = messaging().onMessage(async remoteMessage => {
      console.log('onotrak notif 1', remoteMessage)
      await PushNotification.localNotification({
        id: remoteMessage.notification.messageId,
        message: remoteMessage.notification.body,
        smallIcon: remoteMessage.notification.android.smallIcon,
        color: remoteMessage.notification.android.color,
        soundName: 'notif_hellofit.mp3',
      });

      if(remoteMessage.data.type === "chat"){
        // console.log('onotrak remoteMessage', data);
        let data = JSON.parse(remoteMessage.data.data)
        console.log('onotrak data', data);
        chatMessageStore.setBadge(data.id)
      }

      if(remoteMessage.data.type === "online-schedule"){
        setOnlineSchedule({onlineSchedule: true})
      }
      
      //Sound notif
      if(remoteMessage.data.type === "chat-request" || remoteMessage.data.type === "online-schedule"){
        const details = {...alarmNotifData, fire_date: fireDate};
        try {
          const alarm = await ReactNativeAN.scheduleAlarm(details);
          if (alarm) {
            console.log('alarm', alarm);
          }
        } catch (e) {
          console.log('error', e);
        }
      }
      //end of sound notif
    })
    return unsubsribe;
  }, [])

  const requestPermissionUser = async () => {
    
    const authorizationStatus = await messaging().requestPermission();
    
    if (authorizationStatus) {
      console.log('Permission status:', authorizationStatus);
    }
  }
      
  //Sound notif
  useEffect(() => {
    messaging().setBackgroundMessageHandler(async remoteMessage => {
      console.log('onotrak notif 2', remoteMessage)
      if(remoteMessage.data.type === "chat"){
        let data = JSON.parse(remoteMessage.data.data)
        console.log('onotrak remoteMessage', data);
        chatMessageStore.setBadge(data.id)
      }

      if(remoteMessage.data.type === "chat-request" || remoteMessage.data.type === "online-schedule"){
        const details = {...alarmNotifData, fire_date: fireDate};
        try {
          const alarm = await ReactNativeAN.scheduleAlarm(details);
          if (alarm) {
            console.log('alarm', alarm);
          }
        } catch (e) {
          console.log('error', e);
        }
      }
      
      if(remoteMessage.data.type === "online-schedule"){
        setOnlineSchedule({onlineSchedule: true})
      }
    })
  }, [])
  //end of sound notif

  // if(isFromNotif && !isInitiate){
  //   return null
  // }

  return (
    <SafeAreaView style={{flex: 1}}>
      <StatusBar
        barStyle="dark-content"
        backgroundColor="rgba(0,0,0,0.3)"
        translucent
      />
      <Router backAndroidHandler={() => _backAndroidHandler()}>
        <Stack key="root" hideNavBar>
          <Scene key="splash" component={Splash} url={initialUrl} notifData={notifData} />
          <Scene key="version" component={Version} />
          <Scene key="reset-password" component={ResetPassword} />
          <Scene key="login" component={Login} />
          <Scene key="forgot-password" component={ForgotPassword} />
          <Scene key="register" component={Register} />
          <Scene key="terms-and-conditions" component={TermsAndConditions} />
          <Scene
            tabs
            key="main"
            headerMode="none"
            showLabel={false}
            tabBarStyle={{
              height: 56,
            }}
          >
            <Scene key="home" icon={TabIcon} title="Beranda" component={Home} notifData={notifData} />
            <Scene
              key="chat-tabs"
              icon={TabIcon}
              title="Chat"
              component={ChatTabs}
              initial={notifType === "chat-request"}
            />
            {/* <Scene
              key="appointment-list"
              icon={TabIcon}
              title="Book Jadwal"
              component={AppointmentList}
            /> */}
            <Scene
              key="profile"
              icon={TabIcon}
              title="Profil"
              component={Profile}
            />
          </Scene>
          <Scene key="chat-detail" component={ChatDetail} />
          <Scene key="article-pilihan" component={ArticlePilihan} />
          <Scene key="doctor-note" component={DoctorNote} />
          <Scene key="doctor-prescription" component={DoctorPrescription} />
          <Scene
            key="doctor-prescription-add"
            component={DoctorPrescriptionAdd}
          />
          <Scene key="medicine-list" component={MedicineList} />
          <Scene key="chat-history-list" component={ChatHistoryList} />
          <Scene key="chat-history-detail" component={ChatHistoryDetail} />
          <Scene
            key="appointment-history-list"
            component={AppointmentHistoryList}
          />
          <Scene key="rating-list" component={RatingList} />
          <Scene key="settings" component={Settings} />
          <Scene key="help-list" component={HelpList} />
          <Scene key="profile-edit" component={ProfileEdit} />
          <Scene key="change-password" component={ChangePassword} />
          <Scene key="schedule-edit" component={ScheduleEdit} />
          <Scene key="price-edit" component={PriceEdit} />
          <Scene key="template-edit" component={TemplateEdit} />
          <Scene key="help-detail" component={HelpDetail} />
        </Stack>
      </Router>
    </SafeAreaView>
  );
};

console.disableYellowBox = true;