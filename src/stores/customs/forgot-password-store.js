import { computed, observable } from 'mobx';
import moment from 'moment';
import { feathersClient } from '../../utils';
import { validateEmail } from '../../utils/helper';

class ForgotPasswordStore {
    @observable
    loading = false;
    accountForgotForm = observable.object({
        email: '',
    });
    resetPasswordForm = observable.object({
        id: '',
        password: '',
        cpassword: ''
    })

    @computed
    get emailValid() {
      const {
        email,
      } = this.accountForgotForm;
      return (
        validateEmail(email)
      );
    }

    @computed
    get accountForgotFormValid() {
        const {
            email,
        } = this.accountForgotForm;
        return (
            email.length > 0
        );
    }

    get resetPasswordFormValid() {
        const {
            password,
            cpassword
        } = this.resetPasswordForm

        return (
            password.length > 5 &&
            password === cpassword
        )
    }


    async submit() {
        const result = await feathersClient.service('forgot-password').create({
            email: this.accountForgotForm.email,
            type: 'doctor'
        });
    }

    async update() {
        const result = await feathersClient.service('forgot-password').update(this.resetPasswordForm.id, {
            password: this.resetPasswordForm.password
        })
    }
}

export const forgotPasswordStore = new ForgotPasswordStore();
