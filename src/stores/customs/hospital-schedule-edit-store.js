import { observable } from 'mobx';

const days = [
  'monday',
  'tuesday',
  'wednesday',
  'thursday',
  'friday',
  'saturday',
  'sunday',
];
const defaultTime = {
  startHour: 8,
  startMinute: 0,
  finishHour: 17,
  finishMinute: 0,
};

class HospitalScheduleEditStore {
  @observable
  hospital = {
    id: 1,
    photo: null,
    name: 'RS Persahabatan',
    address: '',
    city: '',
  };
  @observable
  hospitalSchedules = days.map((day) => ({
    hospitalId: this.hospital.id,
    day,
    enabled: true,
    times: [defaultTime],
  }));

  addSchedule(hospitalId) {}

  addTime(index) {
    this.hospitalSchedules[index].times?.push(defaultTime);
  }
}

export const hospitalScheduleEditStore = new HospitalScheduleEditStore();
