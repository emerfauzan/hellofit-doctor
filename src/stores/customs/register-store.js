import { computed, observable } from 'mobx';
import moment from 'moment';
import { blankToNull, feathersClient } from '../../utils';
import { validateEmail } from '../../utils/helper';

class RegisterStore {
  @observable
  loading = false;
  @observable
  step = 1;
  accountForm = observable.object({
    name: '',
    gender: '',
    birthdate: '',
    specialtyId: 1,
    email: '',
    phone: '',
    password: '',
  });
  educationForm = observable.object({
    educations: observable.array([]),
  });
  scheduleForm = observable.object({
    schedules: observable.array([]),
  });
  verificationForm = observable.object({
    regNo: '',
    regCard: '',
    idNo: '',
    idCard: '',
    termsAgreed: false,
  });

  @computed
  get accountFormValid() {
    const { name, gender, birthdate, email, phone, password } = this.accountForm;
    return (
      name.length > 0 &&
      gender.length > 0 &&
      birthdate.length > 0 &&
      email.length > 0 &&
      phone.length > 0 &&
      password.length > 6
    );
  }

  @computed
  get birthdateValid() {
    const { birthdate } = this.accountForm;
    let now = moment(new Date()).format('YYYYMMDD')
    let date = moment(birthdate, 'DD-MM-YYYY').format('YYYYMMDD')
    let format = moment(birthdate, 'DD-MM-YYYY').format('DD-MM-YYYY') === birthdate
    let newDate = date <= now ? true:false
    return moment(birthdate, 'DD-MM-YYYY').isValid() && format && newDate
  }

  @computed
  get emailValid() {
    const {
      email,
    } = this.accountForm;
    return (
      validateEmail(email)
    );
  }

  @computed
  get verificationFormValid() {
    const { regNo, idNo, idCard, termsAgreed } = this.verificationForm;
    return (
      regNo.length > 0 &&
      idNo.length > 0 &&
      idCard.length > 0 &&
      termsAgreed === true
    );
  }

  async submit() {
    const result = await feathersClient.service('register-doctor').create({
      doctor: {
        specialtyId: this.accountForm.specialtyId,
        name: this.accountForm.name,
        online: true,
        birthdate: moment(this.accountForm.birthdate, 'DD-MM-YYYY').toDate(),
        regNo: this.verificationForm.regNo,
        idNo: this.verificationForm.idNo,
        regCard: blankToNull(this.verificationForm.regCard),
        idCard: blankToNull(this.verificationForm.idCard),
      },
      educations: this.educationForm.educations,
      hospitalSchedules: this.scheduleForm.schedules,
      user: {
        email: this.accountForm.email,
        gender: this.accountForm.gender,
        phone: this.accountForm.phone,
        password: this.accountForm.password,
      },
    });
  }
}

export const registerStore = new RegisterStore();
