import { observable, computed } from 'mobx';
import moment from 'moment';
import { feathersClient } from '../../utils';
import { profileStore } from '../customs';

class ProfileEditStore {
  @observable
  loading = false;
  @observable
  form = {
    photo: null,
    name: '',
    gender: '',
    birthdate: '',
    email: '',
    phone: '',
    regNo: '',
    regCard: '',
    idNo: '',
    idCard: '',
  };

  @computed
  get birthdateValid() {
    const { birthdate } = this.form;
    let now = moment(new Date()).format('YYYYMMDD')
    let date = moment(birthdate, 'DD-MM-YYYY').format('YYYYMMDD')
    let format = moment(birthdate, 'DD-MM-YYYY').format('DD-MM-YYYY') === birthdate
    let newDate = date <= now ? true:false
    return moment(birthdate, 'DD-MM-YYYY').isValid() && format && newDate
  }

  reset() {
    if (profileStore.doctor) {
      this.form.photo = profileStore.doctor.photo;
      this.form.name = profileStore.doctor.name;
      this.form.birthdate = moment(profileStore.doctor.birthdate).format(
        'DD-MM-YYYY'
        );
      this.form.gender = (profileStore.doctor.user && profileStore.doctor.user.gender) || '';
      this.form.email = (profileStore.doctor.user && profileStore.doctor.user.email) || '';
      this.form.phone = (profileStore.doctor.user && profileStore.doctor.user.phone) || '';
      this.form.regNo = profileStore.doctor.regNo;
      this.form.regCard = profileStore.doctor.regCard || '';
      this.form.idNo = profileStore.doctor.idNo;
      this.form.idCard = profileStore.doctor.idCard || '';
    }
  }

  async submit() {
    if (profileStore.doctor) {
      this.loading = true;
      await feathersClient.service('doctors').patch(profileStore.doctor.id, {
        ...profileStore.doctor,
        photo: this.form.photo,
        name: this.form.name,
        birthdate: moment(this.form.birthdate, 'DD-MM-YYYY').format('YYYY-MM-DD'),
        regNo: this.form.regNo,
        idNo: this.form.idNo,
        regCard: this.form.regCard,
        idCard: this.form.idCard,
        user: { ...profileStore.doctor.user, phone: this.form.phone, gender: this.form.gender, },
      });
      await profileStore.fetch();
      this.loading = false;
    }
  }
}

export const profileEditStore = new ProfileEditStore();
