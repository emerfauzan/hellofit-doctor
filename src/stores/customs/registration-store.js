import { computed, observable } from 'mobx';
import moment from 'moment';
import { ajv, feathersClient, uploadFile, isPaginated } from '../../utils';

const validateAccount = ajv.compile({
  properties: {
    name: {
      type: 'string',
      minLength: 2,
    },
    email: {
      type: 'string',
      format: 'email',
    },
    phone: {
      type: 'string',
      minLength: 10,
    },
    password: {
      type: 'string',
      minLength: 6,
    },
  },
});

class RegistrationStore {
  @observable
  name = '';
  @observable
  specialtyId = 1;
  @observable
  birthdate = '';
  @observable
  email = '';
  @observable
  phone = '';
  @observable
  password = '';
  educations = observable.array([]);
  @observable
  regCard = '';
  @observable
  idCard = '';
  @observable
  termsAgreed = false;

  async checkEmailAndPhone() {
    const checkEmailResult = await feathersClient.service('users').find({
      query: {
        email: this.email,
      },
    });

    if (isPaginated(checkEmailResult) && checkEmailResult.total > 0) {
      throw new Error('Email sudah terdaftar');
    }

    const checkPhoneResult = await feathersClient.service('users').find({
      query: {
        phone: this.phone,
      },
    });

    if (isPaginated(checkPhoneResult) && checkPhoneResult.total > 0) {
      throw new Error('Nomor Handphone sudah terdaftar');
    }
  }

  @computed
  get isAccountValid() {
    return validateAccount({
      name: this.name,
      email: this.email,
      phone: this.phone,
      password: this.password,
    });
  }

  @computed
  get isVerificationValid() {
    return this.regCard !== '' && this.idCard !== '' && this.termsAgreed;
  }

  reset() {
    this.name = '';
    this.birthdate = '';
    this.specialtyId = 1;
    this.email = '';
    this.phone = '';
    this.password = '';
    this.educations.clear();
    this.regCard = '';
    this.idCard = '';
    this.termsAgreed = false;
  }

  async submit() {
    const doctorService = feathersClient.service('doctors');
    const newDoctor = await doctorService.create({
      specialtyId: this.specialtyId,
      name: this.name,
      online: true,
      birthdate: moment(this.birthdate, 'DD-MM-YYYY').toDate(),
      user: {
        email: this.email,
        phone: this.phone,
        password: this.password,
      },
    });
    await feathersClient.authenticate({
      strategy: 'local',
      email: this.email,
      password: this.password,
    });

    for (const item of this.educations) {
      await feathersClient.service('educations').create({
        degreeId: item.degree.id,
        schoolId: item.school.id,
        graduate: item.graduate,
      });
    }

    const regCard = await uploadFile(this.regCard, 'image/*', 'regCard');
    const idCard = await uploadFile(this.idCard, 'image/*', 'idCard');
    await feathersClient.service('doctors').patch(newDoctor.id, {
      regCard,
      idCard,
    });
  }
}

export const registrationStore = new RegistrationStore();
