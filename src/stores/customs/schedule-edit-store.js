import { computed, observable } from 'mobx';
import { days, feathersClient, isPaginated } from '../../utils';

class ScheduleEditStore {
  nextScheduleId = -1;
  nextTimeId = -1;
  records = observable.map();
  ids = observable.array();

  @computed
  get data() {
    const result = [];

    for (const id of this.ids) {
      const record = this.records.get(id);

      if (record) {
        result.push(record);
      }
    }

    return result;
  }

  @computed
  get hours() {
    let result = 0;

    for (const id of this.ids) {
      const record = this.records.get(id);

      if (record && record.enabled) {
        for (const time of record.times) {
          result += time.finishHour - time.startHour;
        }
      }
    }

    return result;
  }

  async submit() {
    // create new schedules
    for (const id of this.ids.filter((id) => id < 0)) {
      const record = this.records.get(id);

      if (record && record.enabled) {
        const schedule = await feathersClient.service('schedules').create({
          day: record.day,
          enabled: record.enabled,
        });

        for (const time of record.times) {
          await feathersClient.service('schedule-times').create({
            scheduleId: schedule.id,
            startHour: time.startHour,
            startMinute: time.startMinute,
            finishHour: time.finishHour,
            finishMinute: time.finishMinute,
          });
        }
      }
    } // patch existing schedules

    for (const id of this.ids.filter((id) => id > 0)) {
      const record = this.records.get(id);

      if (record) {
        await feathersClient.service('schedules').patch(id, {
          enabled: record.enabled,
        });

        for (const time of record.times) {
          if (time.id < 0) {
            // create new times
            await feathersClient.service('schedule-times').create({
              scheduleId: record.id,
              startHour: time.startHour,
              startMinute: time.startMinute,
              finishHour: time.finishHour,
              finishMinute: time.finishMinute,
            });
          } else {
            // patch existing times
            await feathersClient.service('schedule-times').patch(time.id, {
              startHour: time.startHour,
              startMinute: time.startMinute,
              finishHour: time.finishHour,
              finishMinute: time.finishMinute,
            });
          }
        }
      }
    }
  }

  async reset() {
    this.records.clear();
    this.ids.clear();
    const result = await feathersClient.service('schedules').find();

    if (isPaginated(result)) {
      for (const day of days) {
        const schedule = result.data.find((item) => item.day === day);

        if (schedule) {
          this.records.set(schedule.id, schedule);
          this.ids.push(schedule.id);
        } else {
          this.records.set(this.nextScheduleId, {
            id: this.nextScheduleId,
            day,
            enabled: false,
            times: [],
          });
          this.ids.push(this.nextScheduleId);
          this.nextScheduleId = this.nextScheduleId - 1;
        }
      }
    }
  }

  addTime(scheduleId) {
    const record = this.records.get(scheduleId);

    if (record) {
      this.records.set(record.id, {
        ...record,
        times: [
          ...record.times,
          {
            id: this.nextTimeId,
            scheduleId: record.id,
            startHour: 9,
            startMinute: 0,
            finishHour: 24,
            finishMinute: 0,
          },
        ],
      });
      this.nextTimeId = this.nextTimeId - 1;
    }
  }
}

export const scheduleEditStore = new ScheduleEditStore();
