import { observable } from 'mobx';
import { feathersClient, isPaginated } from '../../utils';

class ProfileStore {
  @observable
  doctor = null;

  async fetch() {
    const { user } = await feathersClient.get('authentication');
    const result = await feathersClient.service('doctors').find({
      query: {
        userId: user.id,
      },
    });

    console.log("Doc ", user)

    if (isPaginated(result)) {
      const doctor = result.data[0];
      this.doctor = doctor;
    }
  }

  async setStatus(value) {
    if (this.doctor) {
      this.doctor.online = value;
      await feathersClient.service('doctors').patch(this.doctor.id, {
        online: value,
      });
      const actionType = value ? 'online' : 'offline';
      await feathersClient.service('actions').create({
        type: actionType,
      });
    }
  }

  async updatePassword(password) {
    const { user } = await feathersClient.get('authentication');
    await feathersClient.service('users').patch(user.id, {
      password,
    });
  }

  async updatePrice(price) {
    if (this.doctor) {
      this.doctor.price = price;
      await feathersClient.service('doctors').patch(this.doctor.id, {
        price,
      });
    }
  }
}

export const profileStore = new ProfileStore();
