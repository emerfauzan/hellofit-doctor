import { feathersClient, FeathersStore } from '../../utils';

class ChatHistoryStore extends FeathersStore {
  constructor(serviceName) {
    super(serviceName);
    feathersClient.service(serviceName).on('patched', (chat) => {
      this.find({
        query: {
          status: 'completed',
        },
      });
    });
  }
}

export const chatHistoryStore = new ChatHistoryStore('chats');
