import { FeathersStore } from '../../utils';

class AppointmentHistoryStore extends FeathersStore {
  constructor() {
    super('appointments');
  }

  async find() {
    return super.find({
      query: {
        bookedAt: {
          $gt: new Date(),
        },
      },
    });
  }
}

export const appointmentHistoryStore = new AppointmentHistoryStore();
