import { observable } from 'mobx';
import { Actions } from 'react-native-router-flux';
import { feathersClient, FeathersStore } from '../../utils';

class ChatMessageStore extends FeathersStore {
  markIds = observable.set();
  id = 0;
  badge = [];

  constructor(serviceName) {
    super(serviceName);
    feathersClient.service(serviceName).on('created', (chatMessage) => {
      if (chatMessage.chatId === this.id) {
        this.find({
          query: {
            chatId: chatMessage.chatId,
            $sort: {
              createdAt: 1,
            },
            $limit: 50,
          },
        });
      }


      if (Actions.currentScene !== 'chat-detail') {
        this.markIds.add(chatMessage.chatId);
      }
    });
  }

  setBadge(id){
    let badgeData = this.badge
    if(this.badge.length){
      let idFound = this.badge.filter((x)=>{
        return x.id === id
      })
      if(idFound.length){
        badgeData = badgeData.map((x)=> {
          if(x.id === id){
            return {
              ...x,
              badge: x.badge+1
            }
          }
          return x
        })
      }else{
        badgeData = badgeData.concat({id: id, badge: 1})
      }
    }else{
      badgeData.push({id: id, badge: 1})
    }
    this.badge = badgeData
  }

  deleteBadge(id){
    let badgeData = this.badge.filter((x)=>{
      return x.id !== id
    })
    this.badge = badgeData
    // this.badge = []
  }

  badgeStatus(){
    let badgeData = 0
    for (let i = 0; i < this.badge.length; i++) {
      badgeData = badgeData+this.badge[i].badge;
    }
    return badgeData
  }

  setId(id) {
    this.id = id
  }

  getId() {
    return this.id
  }
}

export const chatMessageStore = new ChatMessageStore('chat-messages');
