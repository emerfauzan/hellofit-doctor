import { FeathersStore } from '../../utils';

class AppointmentStore extends FeathersStore {
  constructor() {
    super('appointments');
  }

  async find() {
    return super.find({
      query: {
        bookedAt: {
          $lt: new Date(),
        },
      },
    });
  }
}

export const appointmentStore = new AppointmentStore();
