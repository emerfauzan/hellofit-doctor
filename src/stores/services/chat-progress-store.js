import { feathersClient, FeathersStore } from '../../utils';

class ChatProgressStore extends FeathersStore {
  constructor(serviceName) {
    super(serviceName);
    feathersClient.service(serviceName).on('patched', (chat) => {
      this.find({
        query: {
          status: 'accepted',
        },
      });
    });
  }
}

export const chatProgressStore = new ChatProgressStore('chats');
