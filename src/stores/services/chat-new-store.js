import { feathersClient, FeathersStore } from '../../utils';

class ChatNewStore extends FeathersStore {
  constructor(serviceName) {
    super(serviceName);
    feathersClient.service(serviceName).on('created', (chat) => {
      this.find({
        query: {
          status: 'requested',
        },
      });
    });
    feathersClient.service(serviceName).on('patched', (chat) => {
      this.find({
        query: {
          status: 'requested',
        },
      });
    });
  }
}

export const chatNewStore = new ChatNewStore('chats');
