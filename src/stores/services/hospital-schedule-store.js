import _ from 'lodash';
import { days, FeathersStore, feathersClient } from '../../utils';
import { observable } from 'mobx';
import Config from 'react-native-config';
import { profileStore } from '../customs';

class HospitalScheduleStore extends FeathersStore {
  nextNewTimeId = -1;
  @observable
  loading = false;
  @observable
  step = 1;

  @observable
  scheduleState = [];
  
  scheduleForm = observable.object({
    schedules: observable.array([]),
  });

  constructor() {
    super('hospital-schedules');
  }

  setData(data){
    this.scheduleState = data
  }

  resetData(){
    this.scheduleState = []
  }
  
  addData(data, hospital){
    let schedule = []
    for (let i = 0; i < data.length; i++) {
      schedule.push({
        day: data[i].day,
        enabled: data[i].enabled,
        times: data[i].times,
        doctorId: profileStore.doctor.id,
        hospitalId: hospital.id,
        doctor: profileStore.doctor,
        hospital: hospital,
      })
    }
    this.scheduleState = this.scheduleState.concat(schedule)
  }

  replaceData(data, hospital, prefId){
    let schedule = []
    for (let i = 0; i < data.length; i++) {
      schedule.push({
        day: data[i].day,
        enabled: data[i].enabled,
        times: data[i].times,
        doctorId: profileStore.doctor.id,
        hospitalId: hospital.id,
        doctor: profileStore.doctor,
        hospital: hospital,
      })
    }
    let hospData = this.scheduleState.filter((x)=> {
      return x.hospitalId !== prefId
    })
    this.scheduleState = hospData.concat(schedule)
  }

  deleteData(id){
    let hospData = []
    hospData = this.scheduleState.filter((x)=> {
      return x.hospitalId !== id
    })
    this.scheduleState = hospData
  }

  async setup() {
    const result = await this.find();

    const groupSchedules = _.groupBy(result.data, (item) => item.hospitalId);

    for (const hospitalId of Object.keys(groupSchedules)) {
      const groupDays = groupSchedules[hospitalId].map((item) => item.day);

      for (const day of days) {
        if (!groupDays.includes(day)) {
          // insert new schedule
          this.add({
            hospitalId: Number(hospitalId),
            day,
            hospital: {
              id: Number(hospitalId),
              name: groupSchedules[hospitalId][0].hospital.name,
              photo: null,
              address: '',
              city: '',
            },
            times: [],
          });
        }
      }
    }
  }

  addTime(scheduleId) {
    const schedule = this.records.get(scheduleId);
    const newTime = {
      id: this.nextNewTimeId,
      startHour: 9,
      startMinute: 0,
      finishHour: 24,
      finishMinute: 0,
    };
    schedule.times.push(newTime);
    this.nextNewTimeId++;
  }

  editTime(scheduleId, index, time) {
    const schedule = this.records.get(scheduleId);
    schedule.times[index] = time;
  }

  async submit(){
    let data = this.scheduleState
    for (let i = 0; i < data.length; i++) {
      delete data[i].doctor;
      delete data[i].hospital;
    }
    const result = await feathersClient.service('hospital-schedules').create({
      schedule: data
    })
    console.log('onotrak hospital-schedules', result);
  }
}

export const hospitalScheduleStore = new HospitalScheduleStore();