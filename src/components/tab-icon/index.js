import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import ChatIcon from './chat.svg';
import ChatGrayIcon from './chat-gray.svg';
import ChatYellowIcon from './chat-yellow.svg';
import ScheduleIcon from './schedule.svg';
import ScheduleGrayIcon from './schedule-gray.svg';
import ScheduleYellowIcon from './schedule-yellow.svg';
import UserIcon from './user.svg';
import UserGrayIcon from './user-gray.svg';
import UserYellowIcon from './user-yellow.svg';
import HomeIcon from './home.svg';
import HomeGrayIcon from './home-gray.svg';
import HomeYellowIcon from './home-yellow.svg';

export const TabIcon = (props) => {
  let Icon = HomeIcon;
  let Shadow = HomeYellowIcon;

  if (props.focused) {
    if (props.title === 'Chat') {
      Icon = ChatIcon;
      Shadow = ChatYellowIcon;
    // } else if (props.title === 'Book Jadwal') {
    //   Icon = ScheduleIcon;
    //   Shadow = ScheduleYellowIcon;
    } else if (props.title === 'Profil') {
      Icon = UserIcon;
      Shadow = UserYellowIcon;
    }
  } else {
    if (props.title === 'Beranda') {
      Icon = HomeGrayIcon;
    } else if (props.title === 'Chat') {
      Icon = ChatGrayIcon;
    // } else if (props.title === 'Book Jadwal') {
    //   Icon = ScheduleGrayIcon;
    } else if (props.title === 'Profil') {
      Icon = UserGrayIcon;
    }
  }

  return (
    <View style={styles.container}>
      {props.focused ? (
        <View>
          <Shadow
            style={{
              position: 'absolute',
              left: 1,
              top: 1,
            }}
          />
          <Icon />
        </View>
      ) : (
        <Icon />
      )}
      {props.focused ? <Text style={styles.label}>{props.title}</Text> : null}
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  label: {
    color: '#373F50',
    fontFamily: 'Quicksand-SemiBold',
  },
});
