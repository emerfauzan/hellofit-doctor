import React from 'react';
import {
  Dimensions,
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

const articleWidth = (Dimensions.get('window').width - 40) / 2;
const articleHeight = 1.2 * articleWidth;
export const ArticleGrid = (props) => {
  return (
    <View style={styles.container}>
      <View style={styles.articles}>
        {props.articles.map((article) => (
          <TouchableOpacity
            key={article.id}
            onPress={() => props.onPressArticle(article)}
          >
            <ImageBackground
              source={{
                uri: article.photo,
              }}
              style={styles.article}
            >
              <View style={styles.articleFooter}>
                <Text style={styles.articleTitle}>
                  {article.title.substring(0, 40) + '...'}
                </Text>
              </View>
            </ImageBackground>
          </TouchableOpacity>
        ))}
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    padding: 16,
  },
  articles: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
  article: {
    height: articleHeight,
    justifyContent: 'flex-end',
    marginVertical: 4,
    width: articleWidth,
  },
  articleFooter: {
    backgroundColor: 'rgba(55, 63, 80, 0.8)',
    height: 50,
    padding: 4,
  },
  articleTitle: {
    color: '#FFCB05',
    fontFamily: 'Nunito-SemiBold',
  },
});
