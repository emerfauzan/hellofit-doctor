import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import moment from 'moment';

export const Timer = (props) => {
  const [seconds, setSeconds] = useState(props.initial);
  const [labelColor, setLabelColor] = useState('#373F50');
  useEffect(() => {
    let counter = props.initial;
    const interval = setInterval(() => {
      // set label
      if (counter < 60) {
        setLabelColor('#DE5261');
      } // stop counter or decrease counter

      if (counter <= 0) {
        clearInterval(interval);
        props.onEnd();
      } else {
        counter = counter - 1;
        setSeconds(counter);
      }
    }, 1000);
    return function cleanup() {
      clearInterval(interval);
    };
  }, [props, props.initial]);
  const label = moment
    .utc(moment.duration(seconds, 'seconds').asMilliseconds())
    .format('mm : ss');
  return (
    <View style={styles.container}>
      <View style={styles.circle}>
        <AnimatedCircularProgress
          size={12}
          width={6}
          fill={((props.max - seconds) / props.max) * 100}
          tintColor="#EBECF0"
          backgroundColor="#E6495A"
          rotation={0}
        />
      </View>
      <Text
        style={[
          styles.label,
          {
            color: labelColor,
          },
        ]}
      >
        {label}
      </Text>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flexDirection: 'row',
    width: 72,
  },
  circle: {
    alignItems: 'center',
    backgroundColor: '#EBECF0',
    borderRadius: 9,
    height: 16,
    justifyContent: 'center',
    width: 16,
  },
  label: {
    flex: 1,
    fontFamily: 'Quicksand-Regular',
    fontSize: 12,
    lineHeight: 14,
    marginLeft: 8,
  },
});
