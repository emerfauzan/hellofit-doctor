import React, { useState } from 'react';
import {
  ActivityIndicator,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import Config from 'react-native-config';
import CameraIcon from './camera.svg';

export const UploadField = (props) => {
  const [loading, setLoading] = useState(false);

  function _upload() {
    ImagePicker.showImagePicker({}, async (response) => {
      if (response.didCancel) {
        props.onChangeValue('');
      } else {
        setLoading(true);
        const formData = new FormData();
        formData.append('file', {
          uri: response.uri,
          type: response.type,
          name: response.fileName,
        });
        const resp = await fetch(`${Config.API_URL}/uploads`, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
          },
          body: formData,
        });
        const result = await resp.json();

        if (result) {
          props.onChangeValue(`${Config.API_URL}/uploads/${result.id}`);
        }

        setLoading(false);
      }
    });
  }

  function _renderImage() {
    if (loading) {
      return <ActivityIndicator />;
    } else if (props.value === '') {
      return <CameraIcon />;
    } else {
      return (
        <Image
          source={{
            uri: props.value,
          }}
          style={styles.image}
        />
      );
    }
  }

  return (
    <View style={styles.container}>
      <Text style={styles.label}>{props.label}</Text>
      <TouchableOpacity
        disabled={loading}
        style={styles.photoBox}
        onPress={() => _upload()}
      >
        {_renderImage()}
      </TouchableOpacity>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    marginVertical: 8,
  },
  label: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
    marginBottom: 8,
  },
  photoBox: {
    alignItems: 'center',
    backgroundColor: '#F5F6F8',
    borderColor: '#EBECF0',
    borderRadius: 4,
    borderWidth: 1,
    height: 120,
    justifyContent: 'center',
  },
  image: {
    height: 88,
    width: 88,
  },
});
