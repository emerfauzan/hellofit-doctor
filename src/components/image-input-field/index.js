import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import ImagePicker from 'react-native-image-picker';
import Config from 'react-native-config';
import { feathersClient } from '../../utils';
import avatar from './avatar.png';
import camera from './camera.png';

export const ImageInputField = (props) => {
  async function _select() {
    const { accessToken } = await feathersClient.get('authentication');
    ImagePicker.showImagePicker({}, (response) => {
      if (response.didCancel) {
      } else if (response.error) {
      } else {
        const formData = new FormData();
        formData.append('file', {
          uri: Platform.OS === "android" ? response.uri : response.uri.replace("file://", ""),
          type: response.type,
          name: response.fileName ? response.fileName : 'avatar.jpg',
        });
        fetch(`${Config.API_URL}/uploads`, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            Authorization: accessToken,
            'Content-Type': 'multipart/form-data',
          },
          body: formData,
        })
          .then((resp) => {
            console.log("UP ", resp)
            return resp.json()
          })
          .then((result) => {
            props.onChangeValue(`${Config.API_URL}/uploads/${result.id}`);
          });
      }
    });
  }

  return (
    <View style={styles.container}>
      {props.label && <Text style={styles.label}>{props.label}</Text>}
      <TouchableOpacity onPress={() => _select()}>
        <Image
          style={styles.image}
          source={
            props.value
              ? {
                uri: props.value,
              }
              : avatar
          }
        />
        <View style={styles.imageCard}>
        <Image
          style={styles.image2}
          source={ camera }
        />
        </View>
      </TouchableOpacity>
      <View style={styles.desc}>
        <Text style={styles.label2}>{props.name}</Text>
        <Text style={styles.label3}>{props.specialist}</Text>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    marginBottom: 16,
    paddingVertical: 14,
    paddingHorizontal: 16,
    borderRadius: 5,
    flexDirection: 'row',

    //shadow
    shadowColor: '#0000001A',
    shadowOffset: {
      width: 2,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,
    elevation: 2.5,
  },
  label: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
  },
  image: {
    height: 56,
    borderRadius: 56/2,
    width: 56,
  },
  desc: {
    marginVertical: 7,
    marginHorizontal: 12,
  },
  label2: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
    fontSize: 16,
    lineHeight: 20,
  },
  label3: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Regular',
    fontSize: 14,
    lineHeight: 18,
  },
  imageCard: {
    position: 'absolute',
    bottom: -1, right: -1,
    alignItems: 'center',
    justifyContent: 'center',
    color: 'white',
    height: 20,
    borderRadius: 20/2,
    width: 20,
    backgroundColor: '#FFCB05'
  },
  image2: {
    height: 14,
    width: 14,
    tintColor: 'white'
  },
});
