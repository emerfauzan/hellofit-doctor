import React, { forwardRef } from 'react';
import { StyleSheet, Text, TextInput, View, TouchableOpacity, Image } from 'react-native';
import { TextInputMask } from 'react-native-masked-text';

export const TextInputField = forwardRef((props, ref) => {
  const { label, options, type, style, ...others } = props;
  return (
    <View style={styles.container}>
      <Text style={styles.label}>{props.label}</Text>
      {props.disable && <Text style={styles.label2}>*tidak dapat diubah</Text>}
      {type && options ? (
        <TextInputMask
          ref={ref}
          selectionColor="#FFCB05"
          placeholderTextColor="#C9CDD6"
          style={[styles.input, style]}
          {...others}
          type={type}
          options={options}
        />
      ) : (
        <TextInput
          editable={!props.disable}
          selectionColor="#FFCB05"
          placeholderTextColor="#C9CDD6"
          style={[styles.input, style, props.disable && {backgroundColor: '#EBECF0'}]}
          {...others}
        />
      )}
      {props.message && (
        <Text style={styles.message}>{props.message}</Text>
      )}
      {
        props.icon && <TouchableOpacity style={styles.icon} onPress={props.setShowPassword}><Image source={require('./eye.png')} resizeMode='contain' style={styles.imageIcon}/></TouchableOpacity>
      }
    </View>
  );
});
const styles = StyleSheet.create({
  container: {
    marginBottom: 16,
  },
  label: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
  },
  label2: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
    position: 'absolute',
    right: 0
  },
  input: {
    borderColor: '#373F50',
    borderRadius: 4,
    borderWidth: 1,
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    fontSize: 14,
    height: 40,
    marginTop: 8,
    paddingHorizontal: 16,
  },
  message:{
    color: '#999DAD',
    fontSize: 10,
    fontFamily: 'Quicksand-Regular',
    alignSelf: 'flex-end',
  },
  icon: {
    position: 'absolute',
    right: 16,
    top: 32,
    height: 25,
    width: 25,
  },
  imageIcon: {
    height: 25,
    width: 25,
    tintColor: '#999DAD',
  },
});
