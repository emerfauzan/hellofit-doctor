import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import BackIcon from './back.svg';
import CloseIcon from './close.svg';
import { HeaderLayout } from './header-layout';

export const Header = (props) => (
  <HeaderLayout>
    <View style={styles.container}>
      {props.onBack && (
        <TouchableOpacity onPress={props.onBack}>
          <View style={styles.left}>
            <BackIcon />
          </View>
        </TouchableOpacity>
      )}
      {props.onClose && (
        <TouchableOpacity onPress={props.onClose}>
          <View style={styles.left}>
            <CloseIcon />
          </View>
        </TouchableOpacity>
      )}
      <View style={styles.middle}>
        <Text style={styles.title}>{props.title}</Text>
      </View>
      {props.buttonRight && (
        <TouchableOpacity onPress={props.onRight}>
          <View style={styles.rightButton}>
            <Text style={styles.rightButtonLabel}>{props.buttonRight}</Text>
          </View>
        </TouchableOpacity>
      )}
    </View>
  </HeaderLayout>
);
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFCB05',
    flexDirection: 'row',
    height: 56,
  },
  left: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 56,
    width: 56,
  },
  middle: {
    flex: 1,
    justifyContent: 'center',
  },
  title: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
    fontSize: 16,
    lineHeight: 19,
  },
  rightButton: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 56,
    marginRight: 16,
    width: 56,
  },
  rightButtonLabel: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    lineHeight: 17,
  },
});
