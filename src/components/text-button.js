import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

export const TextButton = (props) => (
  <TouchableOpacity onPress={props.onPress}>
    <View style={styles.container}>
      <Text style={styles.label}>
        {props.label1}
        <Text
          style={[
            styles.label,
            {
              color: '#FFCB05',
            },
          ]}
        >
          {props.label2}
        </Text>
      </Text>
    </View>
  </TouchableOpacity>
);
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flexDirection: 'row',
    height: 40,
    justifyContent: 'center',
  },
  label: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
  },
});
