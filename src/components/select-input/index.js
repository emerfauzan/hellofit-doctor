import React, { useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Picker } from './picker';
import ArrowIcon from './arrow.svg';

export function SelectInput(props) {
  const [showPicker, setShowPicker] = useState(false);
  return (
    <View>
      <View style={styles.container}>
        <TouchableOpacity
          style={styles.touchable}
          onPress={() => setShowPicker(true)}
        >
          <Text style={styles.value}>{props.value ? props.value.name : ''}</Text>
          <ArrowIcon />
        </TouchableOpacity>
      </View>
      <Picker
        items={props.items}
        searchable={props.searchable}
        searchPlaceholder={props.searchPlaceholder}
        searchText={props.searchText}
        visible={showPicker}
        onChangeSearchText={props.onChangeSearchText}
        onChangeValue={(item) => {
          props.onChangeValue(item);
          setShowPicker(false);
        }}
        onDismiss={() => setShowPicker(false)}
        onLoadMore={props.onLoadMore}
        onRequestClose={() => setShowPicker(false)}
      />
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    borderColor: '#EFF0F3',
    borderRadius: 4,
    borderWidth: 1,
    height: 40,
    paddingHorizontal: 16,
  },
  touchable: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  value: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    lineHeight: 17,
  },
});
