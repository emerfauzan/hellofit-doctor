import React from 'react';
import {
  FlatList,
  Modal,
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
} from 'react-native';
import { SearchBar } from '../../components';

export function Picker(props) {
  return (
    <Modal transparent {...props}>
      <View style={styles.container}>
        <View style={styles.inner}>
          {props.searchable && (
            <SearchBar
              placeholder={props.searchPlaceholder}
              value={props.searchText}
              onChangeText={props.onChangeSearchText}
            />
          )}
          <FlatList
            data={props.items}
            renderItem={({ item }) => (
              <TouchableOpacity
                onPress={() => props.onChangeValue(item)}
                style={styles.item}
              >
                <Text style={styles.itemLabel}>{item.name}</Text>
              </TouchableOpacity>
            )}
            keyExtractor={(item) => item.id.toString()}
            onEndReached={props.onLoadMore}
          />
        </View>
      </View>
    </Modal>
  );
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(55, 63, 80, 0.6)',
    flex: 1,
    justifyContent: 'center',
  },
  inner: {
    backgroundColor: '#FFF',
    borderColor: '#707070',
    borderRadius: 4,
    borderWidth: 1,
    marginHorizontal: 16,
    marginVertical: 64,
    paddingHorizontal: 4,
    paddingVertical: 8,
  },
  content: {
    marginVertical: 4,
  },
  item: {
    height: 40,
    justifyContent: 'center',
    paddingHorizontal: 16,
  },
  itemLabel: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    lineHeight: 17,
  },
});
