import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export const TextField = (props) => {
  let flexDirection = 'column';
  let justifyContent = 'flex-start';
  let fontSize = 12;
  let marginTop = 4;

  if (props.horizontal) {
    flexDirection = 'row';
    justifyContent = 'space-between';
    fontSize = 14;
    marginTop = 0;
  }

  return (
    <View
      style={[
        styles.container,
        {
          flexDirection,
          justifyContent,
        },
      ]}
    >
      <Text
        style={[
          styles.name,
          {
            fontSize,
          },
        ]}
      >
        {props.name}
      </Text>
      <Text
        style={[
          styles.value,
          {
            marginTop,
          },
        ]}
      >
        {props.value}
      </Text>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    paddingBottom: 12,
  },
  name: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
  },
  value: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
  },
});
