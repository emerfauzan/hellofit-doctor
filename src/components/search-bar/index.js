import React from 'react';
import { StyleSheet, TextInput, View } from 'react-native';
import ZoomIcon from './zoom.svg';

export const SearchBar = (props) => (
  <View style={styles.container}>
    <TextInput
      autoCapitalize="none"
      autoCompleteType="off"
      autoCorrect={false}
      placeholder="Cari Obat"
      placeholderTextColor="#9CA2B4"
      selectionColor="#FFCB05"
      style={styles.input}
      {...props}
    />
    <View style={styles.icon}>
      <ZoomIcon />
    </View>
  </View>
);
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    justifyContent: 'center',
  },
  input: {
    borderColor: '#E5E7E9',
    borderRadius: 4,
    borderWidth: 1,
    fontFamily: 'Quicksand-Regular',
    fontSize: 16,
    paddingLeft: 16,
    paddingRight: 48,
    paddingVertical: 8,
  },
  icon: {
    position: 'absolute',
    right: 16,
  },
});
