import React from 'react';
import { Modal, StyleSheet, Text, View } from 'react-native';
import { Button } from './button';

export const InfoDialog = (props) => {
  const cancelLabel = props.cancelLabel || 'Tidak';
  const confirmLabel = props.confirmLabel || 'Iya';
  return (
    <Modal
      transparent
      onDismiss={props.onCancel}
      onRequestClose={props.onCancel}
      {...props}
    >
      <View style={styles.outer}>
        <View style={styles.inner}>
          <Text style={styles.title}>{props.title}</Text>
          <Text style={styles.description}>{props.description}</Text>
          <View style={styles.footer}>
            {props.onCancel && (
              <View style={styles.footerItem}>
                <Button
                  color="#EBECF0"
                  label={cancelLabel}
                  onPress={props.onCancel}
                />
              </View>
            )}
            {props.onCancel && <View style={styles.blank} />}
            <View style={styles.footerItem}>
              <Button label={confirmLabel} onPress={props.onConfirm} />
            </View>
          </View>
        </View>
      </View>
    </Modal>
  );
};
const styles = StyleSheet.create({
  outer: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    flex: 1,
    justifyContent: 'center',
  },
  inner: {
    alignItems: 'center',
    backgroundColor: '#FFF',
    borderRadius: 4,
    margin: 16,
    paddingHorizontal: 22,
    paddingVertical: 20,
  },
  title: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
    fontSize: 18,
  },
  description: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    lineHeight: 24,
    marginBottom: 16,
    marginTop: 12,
    textAlign: 'center',
  },
  footer: {
    flexDirection: 'row',
  },
  footerItem: {
    flex: 1,
  },
  blank: {
    width: 8,
  },
});
