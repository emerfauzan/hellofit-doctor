import React from 'react';
import { StyleSheet, View } from 'react-native';

export const Form = (props) => (
  <View style={styles.container}>{props.children}</View>
);
const styles = StyleSheet.create({
  container: {
    padding: 16,
  },
});
