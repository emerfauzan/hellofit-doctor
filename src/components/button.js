import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

export const Button = (props) => {
  const style = [styles.container];

  if (props.color) {
    style.push({
      backgroundColor: props.color,
    });
  }

  if (props.disabled) {
    style.push({
      backgroundColor: '#F5F6F8',
    });
  }

  return (
    <TouchableOpacity disabled={props.disabled} onPress={props.onPress}>
      <View style={style}>
        <Text style={[styles.label, props.fontColor && {color: props.fontColor}]}>{props.label}</Text>
      </View>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#FFCB05',
    borderRadius: 24,
    flexDirection: 'row',
    height: 40,
    justifyContent: 'center',
    marginHorizontal: 16,
    marginVertical: 12,
  },
  label: {
    color: '#373F50',
    fontFamily: 'Quicksand-SemiBold',
    lineHeight: 17,
  },
});
