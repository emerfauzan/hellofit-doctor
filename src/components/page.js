import React from 'react';
import { StyleSheet, View } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';

export const Page = (props) => {
  const { loading, children, style, ...others } = props;
  return (
    <View style={[styles.container, style]} {...others}>
      {children}
      <Spinner visible={loading} />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F2F3F4',
    flex: 1,
  },
});
