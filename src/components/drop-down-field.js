import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

export const DropDownField = (props) => {
  const { label, label2, mask, ...others } = props;
  return (
    <View style={styles.container}>
      <Text style={styles.label}>{props.label}</Text>
      {props.disabled && <Text style={styles.label2}>*tidak dapat diubah</Text>}
      <TouchableOpacity disabled={props.disabled} onPress={props.setShowGender} style={styles.input}>
        <Text style={[styles.label3, props.value && {color: '#373F50'}]}>{props.value ? props.value == 'laki' ? 'Laki-Laki':'Perempuan' : 'Pilih Jenis Kelamin'}</Text>
      </TouchableOpacity>
      {
        props.showGender &&
        <View style={styles.selectGender}>
          <TouchableOpacity 
            style={[styles.btnGender, {borderBottomWidth: 1, borderBottomColor: 'grey'}]} 
            onPress={props.onSelectMale}
            >
            <Text style={[styles.label, {color: '#373F50'}]}>Laki-laki</Text>
          </TouchableOpacity>
          <TouchableOpacity 
            style={styles.btnGender} 
            onPress={props.onSelectFemale}
            >
            <Text style={[styles.label, {color: '#373F50'}]}>Perempuan</Text>
          </TouchableOpacity>
        </View>
      }
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    paddingBottom: 16,
  },
  label: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
  },
  label2: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
    position: 'absolute',
    right: 0
  },
  label3: {
    color: '#C9CDD6',
    fontFamily: 'Quicksand-Medium',
    fontSize: 14,
    lineHeight: 15,
  },
  input: {
    borderColor: '#373F50',
    borderRadius: 4,
    borderWidth: 1,
    height: 40,
    marginTop: 8,
    paddingHorizontal: 16,
    justifyContent: 'center',
  },
  selectGender: {
    marginTop: -25,
    marginLeft: 10,
    backgroundColor: 'white',
    width: '50%',
    height: 80,
    zIndex: 3,
    shadowColor: 'grey',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,
    elevation: 4,
  },
  btnGender: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignContent: 'center',
  },
  textGenderBtn: {
    color: 'black',
    textAlign: 'left'
  },
});
