import React from 'react';
import { SafeAreaView, StatusBar, StyleSheet, View } from 'react-native';

export const HeaderLayout = (props) => {
  const { transparent, children, style, ...others } = props;
  const customStyle = [styles.container, style];

  if (transparent) {
    customStyle.push({
      backgroundColor: undefined,
    });
  }

  return (
    <SafeAreaView style={customStyle} {...others}>
      <View
        style={{
          height: StatusBar.currentHeight,
        }}
      />
      {children}
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFCB05',
  },
});
