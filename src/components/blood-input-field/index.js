import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Option } from './option';

export const BloodInputField = (props) => (
  <View style={styles.container}>
    <Text style={styles.label}>{props.label}</Text>
    <View style={styles.row}>
      <Option
        selected={props.value === 1}
        onPress={() => props.onChangeValue(1)}
      >
        A
      </Option>
      <Option
        selected={props.value === 2}
        onPress={() => props.onChangeValue(2)}
      >
        B
      </Option>
      <Option
        selected={props.value === 3}
        onPress={() => props.onChangeValue(3)}
      >
        AB
      </Option>
      <Option
        selected={props.value === 4}
        onPress={() => props.onChangeValue(4)}
      >
        O
      </Option>
    </View>
  </View>
);
const styles = StyleSheet.create({
  container: {
    marginHorizontal: 16,
    marginVertical: 12,
  },
  label: {
    color: '#373F50',
    fontFamily: 'Quicksand-SemiBold',
    fontSize: 12,
  },
  row: {
    flexDirection: 'row',
    marginTop: 8,
  },
});
