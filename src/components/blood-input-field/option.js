import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

export const Option = (props) => {
  const style = [styles.inner];

  if (props.selected) {
    style.push({
      backgroundColor: '#FFCB05',
    });
  }

  return (
    <TouchableOpacity style={styles.container} onPress={props.onPress}>
      <View style={style}>
        <Text style={styles.label}>{props.children}</Text>
      </View>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    maxWidth: 76,
    marginRight: 8,
  },
  inner: {
    alignItems: 'center',
    borderColor: '#EFF0F3',
    borderRadius: 4,
    borderWidth: 1,
    height: 40,
    justifyContent: 'center',
  },
  label: {
    color: '#373F50',
    fontFamily: 'Quicksand-SemiBold',
    lineHeight: 17,
  },
});
