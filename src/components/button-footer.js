import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Button } from './button';

export const ButtonFooter = (props) => {
  return (
    <View style={styles.container}>
      <Button {...props} />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    marginHorizontal: 16,
    marginVertical: 12,
  },
});
