import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { SelectInput } from './select-input';

export function SelectInputField(props) {
  return (
    <View style={styles.container}>
      <Text style={styles.label}>{props.label}</Text>
      <SelectInput
        items={props.items}
        searchable={props.searchable}
        searchPlaceholder={props.searchPlaceholder}
        searchText={props.searchText || ''}
        value={props.value}
        onChangeSearchText={props.onChangeSearchText}
        onChangeValue={props.onChangeValue}
        onLoadMore={props.onLoadMore}
      />
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    marginBottom: 16,
  },
  label: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
    marginBottom: 8,
  },
});
