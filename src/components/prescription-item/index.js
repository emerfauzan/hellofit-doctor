import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import defaultPhoto from '../../images/default-photo.png';

export const PrescriptionItem = (props) => (
  <View style={styles.container}>
    <View style={styles.top}>
      <View style={styles.left}>
        <Image
          source={defaultPhoto}
          style={styles.image}
          resizeMode="contain"
        />
      </View>
      <View style={styles.middle}>
        <Text style={styles.name}>Paratusin 10 Tablet</Text>
        <Text>
          <Text style={styles.price}>Rp 11.400</Text>
          <Text style={styles.quantity}> x1</Text>
        </Text>
        <Text style={styles.description}>1 x 1 hari | Sebelum Tidur</Text>
      </View>
    </View>
  </View>
);
const styles = StyleSheet.create({
  container: {
    marginBottom: 1,
  },
  top: {
    backgroundColor: '#FFF',
    flexDirection: 'row',
    height: 68,
  },
  left: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 68,
  },
  middle: {
    flex: 1,
    justifyContent: 'center',
  },
  image: {
    height: 40,
    width: 40,
  },
  name: {
    color: '#373F50',
    fontFamily: 'Quicksand-Bold',
  },
  price: {
    color: '#FFCB05',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
  },
  quantity: {
    color: '#373F50',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
  },
  description: {
    color: '#999DAD',
    fontFamily: 'Quicksand-Medium',
    fontSize: 12,
  },
});
