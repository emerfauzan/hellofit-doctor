import React from 'react';
import { KeyboardAvoidingView, Platform, View } from 'react-native';

export const Footer = (props) => {
  return Platform.OS === 'ios' ? (
    <KeyboardAvoidingView behavior="padding">
      <View {...props} />
    </KeyboardAvoidingView>
  ) : (
    <View {...props} />
  );
};
