import { useEffect, useState } from 'react';
import { AppState } from 'react-native';

export function useForceUpdate() {
  const [, setState] = useState();
  return useEffect(() => {
    function listener(nextState) {
      if (nextState === 'active') {
        setState({});
      }
    }

    AppState.addEventListener('change', listener);
    return function cleanup() {
      AppState.removeEventListener('change', listener);
    };
  }, []);
}
