import momentjs from 'moment';
import 'moment/locale/id';

momentjs.locale('id');
export const moment = momentjs;
