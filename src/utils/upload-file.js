import Config from 'react-native-config';
import { feathersClient } from './feathers-client';

export async function uploadFile(uri, type, name) {
  const { accessToken } = await feathersClient.get('authentication');
  const formData = new FormData();
  formData.append('file', {
    uri,
    type,
    name,
  });
  const resp = await fetch(`${Config.API_URL}/uploads`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      Authorization: accessToken,
      'Content-Type': 'multipart/form-data',
    },
    body: formData,
  });
  const result = await resp.json();
  return `${Config.API_URL}/uploads/${result.id}`;
}
