import feathers from '@feathersjs/feathers';
import auth from '@feathersjs/authentication-client';
import socketio from '@feathersjs/socketio-client';
import io from 'socket.io-client';
import { AsyncStorage } from 'react-native';
import Config from 'react-native-config';

const socket = io(Config.API_URL);
export const feathersClient = feathers();
feathersClient.configure(socketio(socket));
feathersClient.configure(
  auth({
    storage: AsyncStorage,
  })
);
