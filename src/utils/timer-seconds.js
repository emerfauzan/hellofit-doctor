import moment from 'moment';

export function chatSeconds(acceptedAt) {
  if (acceptedAt == null) {
    return 0;
  }

  const closedAt = moment(acceptedAt).add(30, 'minutes');
  const now = moment();
  let seconds = Math.round(moment.duration(closedAt.diff(now)).asSeconds());

  if (seconds < 0) {
    return 0;
  }

  return seconds;
}
export function waitingSeconds(createdAt) {
  const closedAt = moment(createdAt).add(5, 'minutes');
  const now = moment();
  let seconds = Math.round(moment.duration(closedAt.diff(now)).asSeconds());

  if (seconds < 0) {
    return 0;
  }

  return seconds;
}
