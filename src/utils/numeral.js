import numeraljs from 'numeral';

numeraljs.register('locale', 'id', {
  delimiters: {
    thousands: '.',
    decimal: ',',
  },
  abbreviations: {
    thousand: 'rb',
    million: 'jt',
    billion: 'M',
    trillion: 'T',
  },
  ordinal: function (number) {
    return '.';
  },
  currency: {
    symbol: 'Rp ',
  },
});
numeraljs.locale('id');
export const numeral = numeraljs;
