import moment from 'moment';

export function formatPatientDescription(patient) {
  const age = moment().diff(moment(patient.birthdate), 'years');
  const { height, weight } = patient;
  return `${age} Tahun ･ ${height} cm ･ ${weight} kg`;
}
