export function isPaginated(result) {
  return result.total !== undefined;
}
