export function blankToNull(value) {
  if (value.length === 0) {
    return null;
  }

  return value;
}
