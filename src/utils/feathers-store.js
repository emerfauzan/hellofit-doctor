import { computed, observable } from 'mobx';
import _ from 'lodash';
import { feathersClient, isPaginated } from '../utils';

export class FeathersStore {
  query = {};
  nextNewId = -1;
  @observable
  fetching = false;
  @observable
  total = 0;
  @observable
  limit = 0;
  @observable
  skip = 0;
  ids = observable.array();
  records = observable.map();

  @computed
  get data() {
    if (this.ids.length > 0) {
      return this.ids.map((id) => this.records.get(id));
    }

    return [];
  }

  @observable
  current = null;
  addedIds = observable.set();
  editedIds = observable.set();
  deletedIds = observable.set();

  constructor(serviceName) {
    this.serviceName = serviceName;
  }

  reset() {
    this.total = 0;
    this.limit = 0;
    this.skip = 0;
    this.ids.clear();
    this.records.clear();
    this.current = null;
    this.addedIds.clear();
    this.editedIds.clear();
    this.deletedIds.clear();
  }

  add(item) {
    this.records.set(this.nextNewId, { ...item, id: this.nextNewId });
    this.ids.push(this.nextNewId);
    this.addedIds.add(this.nextNewId);
    this.nextNewId = this.nextNewId - 1;
  }

  edit(item) {
    this.records.set(item.id, item);

    if (item.id) {
      this.editedIds.add(item.id);
    }
  }

  delete(id) {
    this.deletedIds.add(id);
    this.addedIds.delete(id);
    this.ids.remove(id);
    this.records.delete(id);
  }

  async submit() {
    for (const id of this.deletedIds.values()) {
      await feathersClient.service(this.serviceName).remove(id);
      this.deletedIds.delete(id);
    }

    for (const id of this.addedIds.values()) {
      const data = this.records.get(id);
      await feathersClient.service(this.serviceName).create(_.omit(data, 'id'));
      this.addedIds.delete(id);
      this.editedIds.delete(id);
    }

    for (const id of this.editedIds.values()) {
      const data = this.records.get(id);
      await feathersClient.service(this.serviceName).patch(id, data);
      this.editedIds.delete(id);
    }
  }

  async loadMore() {
    if (this.ids.length < this.total) {
      const result = await feathersClient.service(this.serviceName).find({
        query: { ...this.query, $skip: this.skip + this.limit },
      });

      if (isPaginated(result)) {
        this.skip = result.skip;

        for (const item of result.data) {
          this.records.set(item.id, item);
        }

        this.ids.replace(
          this.ids.slice().concat(result.data.map((item) => item.id))
        );
      }
    }
  }

  async find(params) {
    if (params?.query) {
      this.query = params.query;
    }

    this.fetching = true;
    const result = await feathersClient.service(this.serviceName).find(params);

    if (isPaginated(result)) {
      this.total = result.total;
      this.limit = result.limit;
      this.skip = result.skip;

      for (const item of result.data) {
        this.records.set(item.id, item);
      }

      this.ids.replace(result.data.map((item) => item.id));
    }

    this.fetching = false;
    return result;
  }

  async get(id, params) {
    const result = await feathersClient
      .service(this.serviceName)
      .get(id, params);
    this.current = result;
    return result;
  }

  async create(data, params) {
    this.fetching = true;
    const result = await feathersClient
      .service(this.serviceName)
      .create(data, params);
    this.fetching = false;
    return result;
  }

  async patch(id, data, params) {
    this.fetching = true;
    const result = await feathersClient
      .service(this.serviceName)
      .patch(id, data, params);
    this.fetching = false;
    return result;
  }

  async remove(id, params) {
    this.fetching = true;
    const result = await feathersClient
      .service(this.serviceName)
      .remove(id, params);
    this.fetching = false;
    return result;
  }
}
